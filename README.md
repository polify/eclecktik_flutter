# Eclektik

> A cross-platform music streaming client with offline mode.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Overview

### Explore

![explore page screenshot](./screenshots/android/explore.png)

### Albums

![albums page screenshot](./screenshots/android/albums.png)
![albums page screenshot alt](./screenshots/android/albums_1.png)

### Artists

![artists page screenshot](./screenshots/android/artists.png)

### Songs

![songs page screenshot](./screenshots/android/songs.png)

### Album Page

![album page screenshot](./screenshots/android/album_page.png)

### Artist Page

![artist page screenshot](./screenshots/android/artist_page.png)

### Player With Screen Unlocked

![player unlocked screenshot](./screenshots/android/player.png)

### Player With Screen locked

![player locked screenshot](./screenshots/android/player_locked.png)
