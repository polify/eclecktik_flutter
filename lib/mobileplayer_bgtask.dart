import 'dart:async';

// import 'package:audio_session/audio_session.dart';
import 'package:audio_service/audio_service.dart';
import 'package:just_audio/just_audio.dart';

class AudioPlayerTask extends BackgroundAudioTask {
  final _player = AudioPlayer(); // e.g. just_audio
  List<MediaItem> mediaqueue = [];
  late ConcatenatingAudioSource sourcequeue;
  int? get index => _player.currentIndex;
  MediaItem? get current => index == null ? null : mediaqueue[index!];

  // Implement callbacks here. e.g. onStart, onStop, onPlay, onPause
  @override
  onPlay() => _player.play();
  @override
  onPause() => _player.pause();
  @override
  onStop() async {
    // Stop and dispose of the player.
    await _player.dispose();
    // Shut down the background task.
    await super.onStop();
  }

  @override
  Future<void> onPlayMediaItem(MediaItem mediaItem) async {
    print("onPlayMediaItem : $mediaItem");
    if (mediaqueue.isEmpty) {
      mediaqueue = [mediaItem];
      sourcequeue = ConcatenatingAudioSource(children: [
        AudioSource.uri(Uri.parse(mediaItem.id), tag: mediaItem.title),
      ]);
      await _player.setAudioSource(sourcequeue);
    } else {
      mediaqueue.add(mediaItem);
      await sourcequeue
          .add(AudioSource.uri(Uri.parse(mediaItem.id), tag: mediaItem.title));
    }
    await AudioService.updateQueue(mediaqueue);
    await AudioService.play();
    return super.onPlayMediaItem(mediaItem);
  }

  @override
  Future<void> onAddQueueItem(MediaItem mediaItem) async {
    print("onAddQueueItem");
    if (mediaqueue.isEmpty) {
      mediaqueue = [mediaItem];
      sourcequeue = ConcatenatingAudioSource(children: [
        AudioSource.uri(Uri.parse(mediaItem.id), tag: mediaItem.title),
      ]);
      await _player.setAudioSource(sourcequeue);
    } else {
      mediaqueue.add(mediaItem);
      await sourcequeue
          .add(AudioSource.uri(Uri.parse(mediaItem.id), tag: mediaItem.title));
    }
    return super.onAddQueueItem(mediaItem);
  }

  @override
  Future<void> onUpdateQueue(List<MediaItem> queue) async {
    print("onAddQueueItem");
    if (queue.isEmpty && sourcequeue.length > 0) {
      sourcequeue.clear();
    } else {
      mediaqueue = queue;
      List<AudioSource> children = [];
      for (var i = 0; i < queue.length; i++) {
        var mediaItem = queue[i];
        children.add(
            AudioSource.uri(Uri.parse(mediaItem.id), tag: mediaItem.title));
      }
      sourcequeue = ConcatenatingAudioSource(children: children);
      await _player.setAudioSource(sourcequeue);
    }
    // await _player.setAudioSource(sourcequeue);
  }

  @override
  Future<void> onSkipToNext() async {
    print("Skipto next ??? ");
    if (mediaqueue.isNotEmpty) {
      await _player.seekToNext();
      // _player.add
    }

    return super.onSkipToNext();
  }

  @override
  Future<void> onSkipToPrevious() async {
    print("Skipto prev ??? ");
    if (mediaqueue.isNotEmpty) {
      await _player.seekToPrevious();
      // _player.add
    }
    return super.onSkipToPrevious();
  }

  @override
  onSeekTo(Duration position) => _player.seek(position);
  @override
  onSetSpeed(double speed) => _player.setSpeed(speed);
  @override
  onStart(Map<String, dynamic>? params) async {
    if (params != null) {
      print("[INFO] onStart : ${params.toString()}");
    }
    _player.sequenceStateStream.listen((event) {
      print("[BG_INFO] Got state event queue: ${event?.sequence.length}");
    });
    _player.playbackEventStream.listen((event) {
      print("[BG_INFO] playbackEventStream : $event");
      _broadcastState();
    });
    _player.currentIndexStream.listen((index) {
      print("[BG_INFO] currentIndexStream : $index");
      if (index != null) AudioServiceBackground.setMediaItem(mediaqueue[index]);
    });
    // Listen to state changes on the player...
    _player.playerStateStream.listen((playerState) {
      // ... and forward them to all audio_service clients.
      print("PlayerState : $playerState");
      if (playerState.processingState == ProcessingState.completed) {
        // _player.setLoopMode(LoopMode.all);
        print("Completed");
      }
      AudioServiceBackground.setState(
        playing: playerState.playing,
        // systemActions: [MediaAction.playFromMediaId],
        // Every state from the audio player gets mapped onto an audio_service state.
        processingState: {
          ProcessingState.idle: AudioProcessingState.none,
          ProcessingState.loading: AudioProcessingState.connecting,
          ProcessingState.buffering: AudioProcessingState.buffering,
          ProcessingState.ready: AudioProcessingState.ready,
          ProcessingState.completed: AudioProcessingState.completed,
        }[playerState.processingState],
        // Tell clients what buttons/controls should be enabled in the
        // current state.
        controls: [
          MediaControl.skipToPrevious,
          playerState.playing ? MediaControl.pause : MediaControl.play,
          MediaControl.stop,
          MediaControl.skipToNext,
        ],
      );
    });
    // Play when ready.
    await _player.play();
    // Start loading something (will play when ready).
    // if (currTrack != null) {
    //   // Tell the UI and media notification what we're playing.
    //   AudioServiceBackground.setMediaItem(mediaItem);
    //   var currentTrack = await storage.getObject(currTrack.key);
    //   await _player.setAudioSource(currentTrack);
    // }
  }

  AudioProcessingState _getProcessingState() {
    switch (_player.processingState) {
      case ProcessingState.idle:
        return AudioProcessingState.stopped;
      case ProcessingState.loading:
        return AudioProcessingState.connecting;
      case ProcessingState.buffering:
        return AudioProcessingState.buffering;
      case ProcessingState.ready:
        return AudioProcessingState.ready;
      case ProcessingState.completed:
        return AudioProcessingState.completed;
      default:
        throw Exception('Invalid state: ${_player.processingState}');
    }
  }

  /// Broadcasts the current state to all clients.
  Future<void> _broadcastState() async {
    await AudioServiceBackground.setState(
      controls: [
        MediaControl.skipToPrevious,
        if (_player.playing) MediaControl.pause else MediaControl.play,
        MediaControl.stop,
        MediaControl.skipToNext,
      ],
      systemActions: [
        MediaAction.seekTo,
        MediaAction.seekForward,
        MediaAction.seekBackward,
      ],
      androidCompactActions: [0, 1, 3],
      processingState: _getProcessingState(),
      playing: _player.playing,
      position: _player.position,
      bufferedPosition: _player.bufferedPosition,
      speed: _player.speed,
    );
  }
}

/// An object that performs interruptable sleep.
class Sleeper {
  Completer? _blockingCompleter;

  /// Sleep for a duration. If sleep is interrupted, a
  /// [SleeperInterruptedException] will be thrown.
  Future<void> sleep([Duration? duration]) async {
    _blockingCompleter = Completer();
    if (duration != null) {
      await Future.any([Future.delayed(duration), _blockingCompleter!.future]);
    } else {
      await _blockingCompleter!.future;
    }
    final interrupted = _blockingCompleter!.isCompleted;
    _blockingCompleter = null;
    if (interrupted) {
      throw SleeperInterruptedException();
    }
  }

  /// Interrupt any sleep that's underway.
  void interrupt() {
    if (_blockingCompleter?.isCompleted == false) {
      _blockingCompleter!.complete();
    }
  }
}

class SleeperInterruptedException {}

class Seeker {
  final AudioPlayer player;
  final Duration positionInterval;
  final Duration stepInterval;
  final MediaItem mediaItem;
  bool _running = false;

  Seeker(
    this.player,
    this.positionInterval,
    this.stepInterval,
    this.mediaItem,
  );

  start() async {
    _running = true;
    while (_running) {
      Duration newPosition = player.position + positionInterval;
      if (newPosition < Duration.zero) newPosition = Duration.zero;
      if (newPosition > mediaItem.duration!) newPosition = mediaItem.duration!;
      player.seek(newPosition);
      await Future.delayed(stepInterval);
    }
  }

  stop() {
    _running = false;
  }
}
