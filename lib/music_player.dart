import 'dart:io' show Platform;
import 'package:audio_service/audio_service.dart';
import 'package:dart_vlc/dart_vlc.dart';
// import 'package:eclektik/mobileplayer_bgtask.dart';
import 'package:ferry/ferry.dart';
import 'package:ferry_hive_store/ferry_hive_store.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart';
import 'package:eclektik/graphql/song_query.req.gql.dart';
import 'package:eclektik/graphql/randomplaylist_query.req.gql.dart';
import 'package:just_audio/just_audio.dart';

class MusicPlayer {
  final HiveStore store;
  String? currentSongID;
  GSongCard? current;
  int _playlistIndex = 0;
  final AudioPlayer? _mobilePlayer =
      Platform.isAndroid || Platform.isIOS ? AudioPlayer() : null;
  int? get mobileindex {
    int? el;
    if (_mobilePlayer != null) {
      var quu = AudioService.currentMediaItem;
      if (quu != null && playlist != null) {
        for (var i = 0; i < playlist!.length; i++) {
          if (quu.title == playlist![i].title) {
            el = i;
            current = playlist!.elementAt(i);
            break;
          }
        }
      }
    }
    return el;
  }

  List<GSongCard>? playlist;
  List<Media>? _desktopPlaylist;
  Function? setState;
  Player? desktopPlayer =
      Platform.isLinux || Platform.isWindows ? Player(id: 69420) : null;
  final client = GetIt.I<Client>();

  MusicPlayer({required this.store});
  Future clearPlaylist() async {
    AudioService.queue?.clear();
    // await AudioService.updateQueue([]);
    _mobilePlayer!.sequence?.clear();
  }

  Future<bool> randomPlaylist(int size) async {
    final randomReq = GRandomPlaylistReq((b) => b
      ..vars.limit = size
      ..fetchPolicy = FetchPolicy.NoCache);
    if (_mobilePlayer != null) {
      _mobilePlayer!.setAudioSource(ConcatenatingAudioSource(children: []));
      await clearPlaylist();
    }
    client.request(randomReq).listen((resp) async {
      if (resp.loading || resp.data == null) {
        return;
      }
      final pppplaylist = resp.data!.randomPlaylist;
      print("Playlist '${pppplaylist.name}'");
      final songs = pppplaylist.songs;
      List<MediaItem> nSources = [];
      playlist = [];
      for (var i = 0; i < songs.length; i++) {
        var p0 = songs[i];
        if (p0.signedUrl == null) {
          continue;
        }
        playlist!.add(p0);
        var nSource = MediaItem(
            id: p0.signedUrl!.signedUrl,
            artist: p0.artist?.name ?? "unknow",
            album: p0.album?.name ?? "unknow",
            title: p0.title);
        if (p0.coverUrl != null) {
          nSource = nSource.copyWith(artUri: Uri.parse(p0.coverUrl!));
        }
        if (i == 0) {
          current = songs[i];
          if (setState != null) setState!(() {});
        }
        nSources.add(nSource);
      }
      for (var i = 0; i < nSources.length; i++) {
        if (i == 0) {
          AudioService.playMediaItem(nSources[i]);
          continue;
        }
        await AudioService.addQueueItem(nSources[i]);
      }
      if (setState != null) setState!(() {});
    });
    await Future.delayed(const Duration(seconds: 20));

    return true;
  }

  Future<bool> addManySong(List<GSongCard> songs, bool clear) async {
    if (clear) {
      await clearPlaylist();
    }
    for (var song in songs) {
      if (song.signedUrl == null) {
        final songReq = GSongReq((b) => b
          ..requestId = 'SongReq${song.id}'
          ..fetchPolicy = FetchPolicy.CacheAndNetwork
          ..vars.id = song.id);
        client.request(songReq).listen((resp) async {
          Media? nMedia;
          MediaItem? nSource;
          if (!resp.loading &&
              resp.data != null &&
              resp.data!.song.signedUrl != null) {
            var signedUrl = resp.data!.song.signedUrl!.signedUrl;
            if (Platform.isLinux || Platform.isWindows) {
              nMedia = Media.network(signedUrl);
            } else {
              nSource = MediaItem(
                  id: signedUrl,
                  artist: song.artist?.name ?? "unknow",
                  album: song.album?.name ?? "unknow",
                  title: song.title);
            }
            if (song.coverUrl != null) {
              nSource = nSource!.copyWith(artUri: Uri.parse(song.coverUrl!));
            }
            if (playlist == null) {
              playlist = [resp.data!.song];
              current = resp.data!.song;
              currentSongID = resp.data!.song.id;
              if (Platform.isLinux || Platform.isWindows) {
                _desktopPlaylist = [nMedia!];
                // await desktopPlayer!.open(
                //   Playlist(
                //       medias: _desktopPlaylist!,
                //       playlistMode: PlaylistMode.single),
                // );
              } else {
                await AudioService.playMediaItem(nSource!);
              }
            } else if (!playlist!.contains(resp.data!.song)) {
              playlist!.add(resp.data!.song);
              if (Platform.isLinux || Platform.isWindows) {
                _desktopPlaylist!.add(nMedia!);
                // await desktopPlayer!.add(nMedia);
              } else {
                AudioService.addQueueItem(nSource!);
              }
            }

            // store.put("playlist", playlist.map((e) => e.toJson()));
            if (setState != null) setState!(() {});
          }
        });
        return true;
      }
      Media? nMedia;
      MediaItem? nSource;
      var signedUrl = song.signedUrl!.signedUrl;
      if (Platform.isLinux || Platform.isWindows) {
        nMedia = Media.network(signedUrl);
      } else {
        nSource = MediaItem(
            id: signedUrl,
            artist: song.artist?.name ?? "unknow",
            album: song.album?.name ?? "unknow",
            title: song.title);
      }
      if (song.coverUrl != null) {
        nSource = nSource!.copyWith(artUri: Uri.parse(song.coverUrl!));
      }
      if (playlist == null) {
        playlist = [song];
        current = song;
        currentSongID = song.id;
        if (Platform.isLinux || Platform.isWindows) {
          _desktopPlaylist = [nMedia!];
          // await desktopPlayer!.open(
          //   Playlist(
          //       medias: _desktopPlaylist!, playlistMode: PlaylistMode.single),
          // );
        } else {
          await AudioService.playMediaItem(nSource!);
        }
      } else if (!playlist!.contains(song)) {
        playlist!.add(song);
        if (Platform.isLinux || Platform.isWindows) {
          _desktopPlaylist!.add(nMedia!);
          // await desktopPlayer!.add(nMedia);
        } else {
          AudioService.addQueueItem(nSource!);
        }
      }
    }

    // store.put("playlist", playlist.map((e) => e.toJson()));
    if (setState != null) setState!(() {});
    return true;
  }

  Future<bool> addSong(GSongCard song) async {
    if (song.signedUrl == null) {
      final songReq = GSongReq((b) => b
        ..requestId = 'SongReq${song.id}'
        ..fetchPolicy = FetchPolicy.CacheAndNetwork
        ..vars.id = song.id);
      client.request(songReq).listen((resp) async {
        Media? nMedia;
        MediaItem? nSource;
        if (!resp.loading &&
            resp.data != null &&
            resp.data!.song.signedUrl != null) {
          var signedUrl = resp.data!.song.signedUrl!.signedUrl;
          if (Platform.isLinux || Platform.isWindows) {
            nMedia = Media.network(signedUrl);
          } else {
            nSource = MediaItem(
                id: signedUrl,
                artist: song.artist?.name ?? "unknow",
                album: song.album?.name ?? "unknow",
                title: song.title);
          }
          if (song.coverUrl != null) {
            nSource = nSource!.copyWith(artUri: Uri.parse(song.coverUrl!));
          }
          if (playlist == null) {
            playlist = [resp.data!.song];
            current = resp.data!.song;
            currentSongID = resp.data!.song.id;
            if (Platform.isLinux || Platform.isWindows) {
              _desktopPlaylist = [nMedia!];
              // await desktopPlayer!.open(
              //   Playlist(
              //       medias: _desktopPlaylist!,
              //       playlistMode: PlaylistMode.single),
              // );
            } else {
              await AudioService.playMediaItem(nSource!);
            }
          } else if (!playlist!.contains(resp.data!.song)) {
            playlist!.add(resp.data!.song);
            if (Platform.isLinux || Platform.isWindows) {
              _desktopPlaylist!.add(nMedia!);
              // await desktopPlayer!.add(nMedia);
            } else {
              AudioService.addQueueItem(nSource!);
            }
          }

          // store.put("playlist", playlist.map((e) => e.toJson()));
          if (setState != null) setState!(() {});
        }
      });
      return true;
    }
    Media? nMedia;
    MediaItem? nSource;
    var signedUrl = song.signedUrl!.signedUrl;
    if (Platform.isLinux || Platform.isWindows) {
      nMedia = Media.network(signedUrl);
    } else {
      nSource = MediaItem(
          id: signedUrl,
          artist: song.artist?.name ?? "unknow",
          album: song.album?.name ?? "unknow",
          title: song.title);
    }
    if (song.coverUrl != null) {
      nSource = nSource!.copyWith(artUri: Uri.parse(song.coverUrl!));
    }
    if (playlist == null) {
      playlist = [song];
      current = song;
      currentSongID = song.id;
      if (Platform.isLinux || Platform.isWindows) {
        _desktopPlaylist = [nMedia!];
        // await desktopPlayer!.open(
        //   Playlist(
        //       medias: _desktopPlaylist!, playlistMode: PlaylistMode.single),
        // );
      } else {
        await AudioService.playMediaItem(nSource!);
      }
    } else if (!playlist!.contains(song)) {
      playlist!.add(song);
      if (Platform.isLinux || Platform.isWindows) {
        _desktopPlaylist!.add(nMedia!);
        // await desktopPlayer!.add(nMedia);
      } else {
        AudioService.addQueueItem(nSource!);
      }
    }

    // store.put("playlist", playlist.map((e) => e.toJson()));
    if (setState != null) setState!(() {});
    return true;
  }

  Future<bool> replaceSong(GSongCard song) async {
    if (song.signedUrl == null) {
      final songReq = GSongReq((b) => b
        ..requestId = 'SongReq${song.id}'
        ..fetchPolicy = FetchPolicy.CacheAndNetwork
        ..vars.id = song.id);

      client.request(songReq).listen((resp) async {
        if (!resp.loading && resp.data != null) {
          playlist = [resp.data!.song];
          current = resp.data!.song;
          if (current!.signedUrl == null) {
            print("No signedUrl for this song ...");
            return;
          }
          var signedUrl = resp.data!.song.signedUrl!.signedUrl;

          if (Platform.isLinux || Platform.isWindows) {
            var nMedia = Media.network(signedUrl);
            _desktopPlaylist = [nMedia];
            try {
              // await desktopPlayer!.open(Playlist(medias: _desktopPlaylist!));
            } catch (e) {
              print("Failed to open playlist : $e");
            }
          } else {
            var nSource = MediaItem(
              id: signedUrl,
              album: song.album?.name ?? "unknow",
              artist: song.artist?.name ?? "unknow",
              title: song.title,
              displayTitle: song.title,
              displaySubtitle: song.album?.name,
            );
            if (song.coverUrl != null) {
              nSource = nSource.copyWith(artUri: Uri.parse(song.coverUrl!));
            }
            try {
              // if (!AudioService.connected) {
              //   await AudioService.connect();
              // }

              AudioService.playMediaItem(nSource);
            } catch (e) {
              print("Failed to playmediaItem : $e");
            }
          }
          _playlistIndex = playlist!.indexOf(song);
          // store.put("playlist", playlist.map((e) => e.toJson()));
          if (setState != null) setState!(() {});
        } else if (resp.hasErrors) {
          print("Failed to fetch song datas : ${resp.graphqlErrors}");
        }
      });
    } else {
      playlist = [song];
      current = song;
      if (current!.signedUrl == null) {
        print("No signedUrl for this song ...");
        return false;
      }
      var signedUrl = song.signedUrl!.signedUrl;

      if (Platform.isLinux || Platform.isWindows) {
        var nMedia = Media.network(signedUrl);
        _desktopPlaylist = [nMedia];
        try {
          // await desktopPlayer!.open(Playlist(medias: _desktopPlaylist!));
        } catch (e) {
          print("Failed to open playlist : $e");
        }
      } else {
        var nSource = MediaItem(
          id: signedUrl,
          album: song.album?.name ?? "unknow",
          title: song.title,
          artist: song.artist?.name ?? "unknow",
          displayTitle: song.title,
          displaySubtitle: song.album?.name,
        );
        if (song.coverUrl != null) {
          print("Adding art uri");
          nSource = nSource.copyWith(artUri: Uri.parse(song.coverUrl!));
        }
        try {
          // if (!AudioService.connected) {
          //   await AudioService.connect();
          // }

          AudioService.playMediaItem(nSource);
        } catch (e) {
          print("Failed to playmediaItem : $e");
        }
      }
      _playlistIndex = playlist!.indexOf(song);
      // store.put("playlist", playlist.map((e) => e.toJson()));
      if (setState != null) setState!(() {});
    }

    return true;
  }

  Future<int> playPause() async {
    int rt = 0;
    if (current == null) return rt;
    print("Play ${current!.title}");
    try {
      if ((Platform.isLinux || Platform.isWindows) && desktopPlayer != null) {
        // await desktopPlayer!.playOrPause();
        if (desktopPlayer!.playback.isPlaying) {
          rt = 1;
        } else {
          rt = 2;
        }
      } else {
        if (AudioService.playbackState.playing) {
          AudioService.pause();
          rt = 1;
        } else {
          AudioService.play();
          rt = 2;
        }
        print("Play pause rt : $rt");
        // AudioServiceBackground.setState();

        return rt;
      }
    } catch (e) {
      print("Error playpause : $e");
    }
    return rt;
  }

  Future jumpto(int index) async {
    if (playlist == null || playlist!.isEmpty) return;
    if ((Platform.isLinux || Platform.isWindows) && desktopPlayer != null) {
      desktopPlayer!.jump(index);
    } else if (playlist!.length - 1 >= _playlistIndex + 1) {
      current = playlist!.elementAt(index);
      _playlistIndex = playlist!.indexOf(current!);
      // AudioService.pl();
      // AudioServiceBackground.setState();
    } else {
      print(
          "No element ${_playlistIndex + 1} in ${playlist!.map((e) => e.title).join(", ")} len ${playlist!.length}");
    }
  }

  Future next() async {
    if (playlist == null || playlist!.isEmpty) return;
    if ((Platform.isLinux || Platform.isWindows) && desktopPlayer != null) {
      desktopPlayer!.next();
    } else if (playlist!.length - 1 >= _playlistIndex + 1) {
      current = playlist!.elementAt(_playlistIndex + 1);
      _playlistIndex = playlist!.indexOf(current!);
      AudioService.skipToNext();
      // AudioServiceBackground.setState();
    } else {
      print(
          "No element ${_playlistIndex + 1} in ${playlist!.map((e) => e.title).join(", ")} len ${playlist!.length}");
    }
  }

  Future prev() async {
    if (playlist == null || playlist!.isEmpty) return;
    if ((Platform.isLinux || Platform.isWindows) && desktopPlayer != null) {
      // desktopPlayer!.next();
      print("not yet");
    } else if (_playlistIndex - 1 >= 0) {
      print("Prev to index ${_playlistIndex - 1}");
      current = playlist!.elementAt(_playlistIndex - 1);
      _playlistIndex = playlist!.indexOf(current!);
      AudioService.skipToPrevious();
      // AudioServiceBackground.setState();
    } else {
      print(
          "No element ${_playlistIndex - 1} in ${playlist!.map((e) => e.title).join(", ")} len ${playlist!.length}");
    }
  }
}

Future<MusicPlayer> initMusicPlayer() async {
  final box = await Hive.openBox("eclektik");
  final store = HiveStore(box);
  MusicPlayer rt = MusicPlayer(store: store);
  return rt;
}
