import 'dart:io';

import 'package:gql_http_link/gql_http_link.dart';
import 'package:ferry/ferry.dart';
import 'package:ferry_hive_store/ferry_hive_store.dart';
import 'package:hive/hive.dart';

Future<Client> initClient() async {
  final box = await Hive.openBox("eclektik");
  final store = HiveStore(box);

  final cache = Cache(store: store);

  final httpLink =
      HttpLink('https://release-db.herokuapp.com/query', defaultHeaders: {
    "Authorization":
        store.get("auth") != null ? store.get("auth")!["token"] : "",
    "User-Agent": "EclektikPlayer/v0.1 " "( ;" +
        Platform.operatingSystem +
        " ;" +
        Platform.operatingSystemVersion +
        " ;" +
        Platform.localHostname +
        " )"
  });

  final client = Client(
    link: httpLink,
    cache: cache,
  );

  return client;
}
