import 'package:hive/hive.dart';
import 'package:ferry/ferry.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:ferry_hive_store/ferry_hive_store.dart';

import 'package:eclektik/app.dart';
import 'package:eclektik/jwt.dart';
import 'package:eclektik/music_player.dart';
import 'package:eclektik/graphql_client.dart';

// This is our global ServiceLocator
GetIt getIt = GetIt.instance;

void main() async {
  bool _isLogged = false;
  await Hive.initFlutter();
  final client = await initClient();
  getIt.registerLazySingleton<Client>(() => client);

  final musicPlayer = await initMusicPlayer();
  getIt.registerLazySingleton<MusicPlayer>(() => musicPlayer);

  final box = await Hive.openBox("eclektik");
  final store = HiveStore(box);
  final _jwt = await Jwt.fromStorage(store);

  if (_jwt != null && !_jwt.isexpired) {
    _isLogged = true;
  } else {
    store.clear();
  }

  runApp(MyApp(
    isLogged: _isLogged,
  ));
}
