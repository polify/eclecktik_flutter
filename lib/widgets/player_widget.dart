import 'dart:io';

import 'package:eclektik/widgets/current_playlist.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:audio_service/audio_service.dart';
import 'package:audio_session/audio_session.dart';

import 'package:eclektik/mobileplayer_bgtask.dart';
import 'package:eclektik/music_player.dart';

// Must be a top-level function
void _entrypoint() async => AudioServiceBackground.run(() => AudioPlayerTask());

class PlayerWidget extends StatefulWidget {
  const PlayerWidget({Key? key}) : super(key: key);

  @override
  _PlayerWidgetState createState() => _PlayerWidgetState();
}

class _PlayerWidgetState extends State<PlayerWidget> {
  final _musicPlayer = GetIt.I<MusicPlayer>();
  late AudioPlayer? _player;
  int _playerStatus = 0;
  int? mobileIndex() {
    return _musicPlayer.mobileindex;
  }

  IconData _getIcon() {
    switch (_playerStatus) {
      case 0:
        return Icons.stop;
      case 1:
        return Icons.play_arrow;
      case 2:
        return Icons.pause;
    }
    return Icons.stop;
  }

  @override
  void initState() {
    super.initState();
    _musicPlayer.setState = setState;
    if (Platform.isIOS || Platform.isAndroid) {
      _player = AudioPlayer();
      _initMobilePlayer();
    }
  }

  Future<void> _initMobilePlayer() async {
    final session = await AudioSession.instance;
    await session.configure(const AudioSessionConfiguration.music());
    // Listen to errors during playback.
    _player!.playbackEventStream.listen((event) {
      print("[FG_INFO] Got player event : ${event.toString()}");
      if (event.processingState == ProcessingState.completed) {
        print("Completed");
        AudioService.skipToNext();
      }
    }, onError: (Object e, StackTrace stackTrace) {
      print('[ERROR] A stream error occurred: $e');
    }, onDone: () {
      print("Done");
    });
    _player!.sequenceStateStream.listen((event) {
      print("[FG_INFO] Got state event ${event?.currentIndex}");
    });
    AudioService.currentMediaItemStream.listen((MediaItem? item) {
      if (item != null) {
        print("Item update : $item");
      }
    });

    try {
      await AudioService.start(
          backgroundTaskEntrypoint: _entrypoint,
          androidNotificationChannelName: 'Eclektik',
          androidShowNotificationBadge: true,
          androidEnableQueue: true);
    } catch (e) {
      print("Error starting audio service : $e");
    }
  }

  Future<void> _playPause() async {
    var rt = await _musicPlayer.playPause();
    print("Play pause : $rt");
    setState(() {
      _playerStatus = rt;
    });
  }

  Future<void> _next() async {
    var rt = await _musicPlayer.next();
    print("Next : $rt");
    setState(() {
      _playerStatus = 2;
    });
  }

  Future<void> _prev() async {
    var rt = await _musicPlayer.prev();
    print("Prev : $rt");
    setState(() {
      _playerStatus = 2;
    });
  }

  @override
  void dispose() {
    _musicPlayer.setState = null;
    if (Platform.isIOS || Platform.isAndroid) {
      _player!.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mobIndex = mobileIndex();
    var _currentTitle = mobIndex != null && _musicPlayer.playlist != null
        ? _musicPlayer.playlist![mobIndex].title
        : null;
    var _currentArtist = mobIndex != null && _musicPlayer.playlist != null
        ? _musicPlayer.playlist![mobIndex].artist?.name
        : null;
    return Container(
      width: double.infinity,
      height: 50,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(30), topLeft: Radius.circular(30)),
        boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        ],
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
              onPressed: () {
                setState(() {});
                showDialog(
                    context: context,
                    builder: (ctx) {
                      return AlertDialog(
                        content: CurrentPlaylist(),
                      );
                    });
              },
              padding: EdgeInsets.zero,
              icon: const Icon(
                Icons.fullscreen,
                size: 30,
                color: Colors.white,
              )),
          ConstrainedBox(
            constraints: const BoxConstraints(minWidth: 120, maxWidth: 150),
            child: Text(
              (_currentTitle ?? "x") + " - " + (_currentArtist ?? "x"),
              style: const TextStyle(color: Colors.white),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const Spacer(),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                  onPressed: _prev,
                  padding: EdgeInsets.zero,
                  icon: const Icon(
                    Icons.skip_previous,
                    size: 20,
                    color: Colors.white,
                  )),
              IconButton(
                  onPressed: _playPause,
                  padding: EdgeInsets.zero,
                  icon: Icon(
                    _getIcon(),
                    size: 30,
                    color: Colors.white,
                  )),
              IconButton(
                  onPressed: _next,
                  padding: EdgeInsets.zero,
                  icon: const Icon(
                    Icons.skip_next,
                    size: 20,
                    color: Colors.white,
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
