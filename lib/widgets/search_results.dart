import 'package:eclektik/palette.dart';
import 'package:flutter/material.dart';
import 'package:eclektik/graphql/search_query.data.gql.dart';

class SearchResults extends StatelessWidget {
  final GSearchData_search results;
  const SearchResults({Key? key, required this.results}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white.withOpacity(0.1),
      margin: EdgeInsets.all(3),
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          results.artists != null
              ? Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: results.artists!.artists.length,
                      itemBuilder: (ctx, i) {
                        return Card(
                          child: ListTile(
                            tileColor: Colors.black,
                            selectedTileColor: Colors.red,
                            contentPadding: EdgeInsets.zero,
                            enabled: true,
                            onTap: () => Navigator.of(context)
                                .pushNamed("artist", arguments: {
                              "artist": results.artists!.artists[i],
                            }),
                            leading: Image.network(
                                results.artists!.artists[i].coverUrl != null &&
                                        !results.artists!.artists[i].coverUrl!
                                            .contains(".gif")
                                    ? results.artists!.artists[i].coverUrl!
                                    : defaultNoCover),
                            title: Text(
                              results.artists!.artists[i].name,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        );
                      }),
                )
              : Container(),
          results.albums != null
              ? Expanded(
                  child: ListView.builder(
                      itemCount: results.albums!.albums.length,
                      shrinkWrap: true,
                      itemBuilder: (ctx, i) {
                        return Card(
                          child: ListTile(
                            tileColor: Colors.black,
                            selectedTileColor: Colors.red,
                            contentPadding: EdgeInsets.zero,
                            enabled: true,
                            onTap: () => Navigator.of(context)
                                .pushNamed("album", arguments: {
                              "album": results.albums!.albums[i]
                            }),
                            leading: Image.network(
                                results.albums!.albums[i].coverUrl ??
                                    defaultNoCover),
                            title: Text(
                              results.albums!.albums[i].name,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        );
                      }),
                )
              : Container(),
          results.songs != null
              ? Expanded(
                  child: ListView.builder(
                      itemCount: results.songs!.songs.length,
                      shrinkWrap: true,
                      itemBuilder: (ctx, i) {
                        return Card(
                          child: ListTile(
                            tileColor: Colors.black,
                            selectedTileColor: Colors.red,
                            contentPadding: EdgeInsets.zero,
                            enabled: true,
                            onTap: () {
                              print("should play");
                            },
                            leading: Image.network(
                                results.songs!.songs[i].coverUrl ??
                                    defaultNoCover),
                            title: Text(
                              results.songs!.songs[i].title,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        );
                      }),
                )
              : Container(),
        ],
      ),
    );
  }
}
