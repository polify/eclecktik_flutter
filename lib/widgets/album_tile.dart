import 'dart:io';

import 'package:eclektik/graphql/album_card_fragment.data.gql.dart';
import 'package:eclektik/palette.dart';
import 'package:flutter/material.dart';

class AlbumTile extends StatelessWidget {
  final GAlbumCard album;
  const AlbumTile({required this.album, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
          Navigator.of(context).pushNamed("album", arguments: {"album": album}),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          // mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Image.network(
                album.coverUrl ?? defaultNoCover,
                fit: BoxFit.fill,
                height: Platform.isAndroid || Platform.isIOS ? 150 : 200,
                width: double.infinity,
              ),
            ),
            Text(
              album.name,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 6,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(" Artist(s) : "),
                    SizedBox(
                      width: 200,
                      child: Text(
                        album.artists.map((a) => a.name).join(", "),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(" Song(s) : "),
                    Text(album.songs.length.toString()),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
