import 'package:eclektik/graphql/artist_card_fragment.data.gql.dart';
import 'package:flutter/material.dart';

class ArtistTile extends StatelessWidget {
  final GArtistCard artist;
  const ArtistTile({required this.artist, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () => Navigator.of(context).pushNamed("artist", arguments: {
          "artist": artist,
        }),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.black.withOpacity(0.7),
                  backgroundImage: NetworkImage(artist.coverUrl != null &&
                          !artist.coverUrl!.contains("gif")
                      ? artist.coverUrl!
                      : "https://robohash.org/${artist.name}.png"),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      artist.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      artist.albums.length.toString() +
                          " Album(s), " +
                          artist.songs.length.toString() +
                          " Song(s)",
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: FittedBox(
                  fit: BoxFit.cover,
                  alignment: AlignmentDirectional.topCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () {
                          Navigator.of(context).pushNamed("artist", arguments: {
                            "artist": artist,
                          });
                        },
                        icon: Icon(
                          Icons.search,
                          size: 50,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () => print("play"),
                        icon: Icon(
                          Icons.play_arrow,
                          size: 50,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () => print("play"),
                        icon: Icon(
                          Icons.playlist_add,
                          size: 50,
                          color: Colors.black.withOpacity(0.7),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
