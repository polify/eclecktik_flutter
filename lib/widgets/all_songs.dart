import 'package:built_collection/built_collection.dart';
import 'package:eclektik/graphql/songs_query.data.gql.dart';
import 'package:eclektik/graphql/songs_query.req.gql.dart';
import 'package:eclektik/graphql/songs_query.var.gql.dart';
import 'package:eclektik/music_player.dart';
import 'package:eclektik/widgets/song_tile.dart';
import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class AllSongsScreen extends StatefulWidget {
  const AllSongsScreen({Key? key}) : super(key: key);

  @override
  State<AllSongsScreen> createState() => _AllSongsScreenState();
}

class _AllSongsScreenState extends State<AllSongsScreen> {
  final client = GetIt.I<Client>();
  final player = GetIt.I<MusicPlayer>();

  int _page = 0;
  int _resultLenght = 0;
  bool _isloadingrandomplaylist = false;
  static bool _lastPage = false;
  final Map<int, int> _pagination = {
    0: 0,
  };

  double lastOffset = 0.0;
  final _scrollController = ScrollController();

  final songsReq = GSongsReq((b) => b
    ..requestId = 'AllSongsReq'
    ..fetchPolicy = FetchPolicy.CacheFirst
    ..vars.limit = 50
    ..vars.offset = 0);

  _nextPage() async {
    if ((_page > 0 && _pagination[_page - 1] == _pagination[_page])) {
      _lastPage = true;
      print("Last page $_lastPage $_pagination");
      return;
    }
    _page++;
    print("Page $_page Res $_resultLenght");
    // await client.requestController.done;
    final nextArtistsReq = songsReq.rebuild(
      (b) => b
        ..vars.offset = _page
        ..updateResult = (previous, result) =>
            previous
                ?.rebuild((b) => b..songs.songs.addAll(result!.songs.songs)) ??
            result,
    );
    client.requestController.add(nextArtistsReq);
    await client.requestController.done;
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      lastOffset = _scrollController.position.pixels;
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        print("BOTTOM-NEXT");
        if (!_lastPage) _nextPage();
      } else if (_scrollController.position.pixels ==
          _scrollController.position.minScrollExtent) {
        print("TOP-REFRESH");
        // client.requestController.add(songsReq);
        final refreshReq = songsReq
            .rebuild((b) => b..fetchPolicy = FetchPolicy.CacheAndNetwork);
        client.requestController.add(refreshReq);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Operation<GSongsData, GSongsVars>(
      client: client,
      operationRequest: songsReq,
      builder: (ctx, resp, err) {
        if (resp!.loading) {
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.red,
            ),
          );
        }
        final songs = resp.data?.songs.songs ?? BuiltList();
        _resultLenght = songs.length;
        _pagination[_page] = _resultLenght;
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$_resultLenght / ${resp.data?.songs.total}",
                  textScaleFactor: 2.3,
                  style: const TextStyle(color: Colors.white),
                ),
                const Spacer(),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Play all"),
                    icon: const Icon(
                      Icons.play_arrow,
                      size: 30,
                      color: Colors.white,
                    )),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Add all to playlist"),
                    icon: const Icon(
                      Icons.playlist_add,
                      size: 30,
                      color: Colors.white,
                    )),
                _isloadingrandomplaylist
                    ? const CircularProgressIndicator(
                        strokeWidth: 1.6,
                        color: Colors.white,
                      )
                    : IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () async {
                          setState(() {
                            _isloadingrandomplaylist = true;
                          });
                          await player.randomPlaylist(50);
                          setState(() {
                            _isloadingrandomplaylist = false;
                          });
                        },
                        icon: const Icon(
                          Icons.shuffle,
                          size: 30,
                          color: Colors.white,
                        ))
              ],
            ),
            Expanded(
              child: Scrollbar(
                isAlwaysShown: true,
                controller: _scrollController,
                child: ListView.builder(
                    itemCount: songs.length,
                    controller: _scrollController,
                    itemBuilder: (ctx, index) => SongTile(song: songs[index])),
              ),
            ),
          ],
        );
      },
    );
  }
}
