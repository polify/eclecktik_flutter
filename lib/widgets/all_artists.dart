import 'package:built_collection/built_collection.dart';
import 'package:eclektik/graphql/artists_query.req.gql.dart';
import 'package:eclektik/graphql/artists_query.var.gql.dart';
import 'package:eclektik/graphql/artists_query.data.gql.dart';
import 'package:eclektik/widgets/artist_tile.dart';

import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class AllArtistsScreen extends StatefulWidget {
  const AllArtistsScreen({Key? key}) : super(key: key);

  @override
  State<AllArtistsScreen> createState() => _AllArtistsScreenState();
}

class _AllArtistsScreenState extends State<AllArtistsScreen> {
  final client = GetIt.I<Client>();
  static int _page = 0;
  static int _resultLenght = 0;
  static bool _lastPage = false;
  final Map<int, int> _pagination = {_page: _resultLenght};

  double lastOffset = 0.0;
  final _scrollController = ScrollController();
  final artistReq = GArtistsReq((b) => b
    ..requestId = 'AllArtistsReq'
    ..fetchPolicy = FetchPolicy.CacheFirst
    ..vars.limit = 50
    ..vars.offset = 0);

  _nextPage() async {
    if ((_page > 0 && _pagination[_page - 1] == _pagination[_page])) {
      _lastPage = true;
      print("Last page $_lastPage $_pagination");
      return;
    }
    _page++;
    print("Page $_page Res $_resultLenght");

    final nextArtistsReq = artistReq.rebuild(
      (b) => b
        ..vars.offset = _page
        ..updateResult = (previous, result) =>
            previous?.rebuild((b) => b..artists.addAll(result!.artists)) ??
            result,
    );
    client.requestController.add(nextArtistsReq);
    print("done page $_pagination");
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      lastOffset = _scrollController.position.pixels;
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (!_lastPage) _nextPage();
      } else if (_scrollController.position.pixels ==
          _scrollController.position.minScrollExtent) {
        print("TOP-REFRESH");
        final refreshReq = artistReq
            .rebuild((b) => b..fetchPolicy = FetchPolicy.CacheAndNetwork);
        client.requestController.add(refreshReq);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Operation<GArtistsData, GArtistsVars>(
      client: client,
      operationRequest: artistReq,
      builder: (ctx, resp, err) {
        if (resp!.loading) {
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.red,
            ),
          );
        } else if (err != null) {
          return Center(
            child: Text(err.toString()),
          );
        }
        final artists = resp.data?.artists ?? BuiltList();
        _resultLenght = artists.length;
        _pagination[_page] = _resultLenght;

        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$_resultLenght Artists",
                  textScaleFactor: 2.5,
                  style: const TextStyle(color: Colors.white),
                ),
                const Spacer(),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Play all"),
                    icon: const Icon(
                      Icons.play_arrow,
                      size: 30,
                      color: Colors.white,
                    )),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Add all to playlist"),
                    icon: const Icon(
                      Icons.playlist_add,
                      size: 30,
                      color: Colors.white,
                    )),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Shuffle"),
                    icon: const Icon(
                      Icons.shuffle,
                      size: 30,
                      color: Colors.white,
                    ))
              ],
            ),
            Expanded(
              child: CupertinoScrollbar(
                isAlwaysShown: true,
                controller: _scrollController,
                child: GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200,
                            childAspectRatio: 4 / 2,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 20),
                    itemCount: artists.length,
                    controller: _scrollController,
                    itemBuilder: (BuildContext ctx, index) {
                      return ArtistTile(artist: artists[index]);
                    }),
              ),
            ),
          ],
        );
      },
    );
  }
}
