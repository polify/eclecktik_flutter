import 'package:eclektik/music_player.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class CurrentPlaylist extends StatelessWidget {
  final _musicPlayer = GetIt.I<MusicPlayer>();
  CurrentPlaylist({Key? key}) : super(key: key);
  mobileIndex() {
    return _musicPlayer.mobileindex;
  }

  @override
  Widget build(BuildContext context) {
    if (_musicPlayer.playlist == null) {
      return const SizedBox(
        width: 20,
        height: 20,
        child: Text("No playlist yet"),
      );
    }
    final _playlist = _musicPlayer.playlist!;
    return SizedBox(
      height: 300,
      child: Column(
        children: [
          Text(
            "${_playlist.length} Songs",
            textScaleFactor: 1.4,
          ),
          Expanded(
            child: ListView.builder(
                itemCount: _playlist.length,
                itemBuilder: (ctx, i) {
                  return Dismissible(
                    key: Key(_playlist[i].id),
                    onDismissed: (d) {
                      _playlist.removeAt(i);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Flexible(
                                // width: 70,
                                child: Text(
                                  _playlist[i].title,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              _musicPlayer.mobileindex == i
                                  ? const Text(" 🎵")
                                  : const Text(""),
                            ],
                          ),
                          Text(_playlist[i].artist?.name ?? "-")
                        ],
                      ),
                    ),
                  );
                }),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.red),
                  child: Icon(Icons.delete),
                  onPressed: () {
                    print("remove");
                  }),
              ElevatedButton(
                  child: Icon(Icons.save),
                  onPressed: () {
                    print("save");
                  }),
            ],
          )
        ],
      ),
    );
  }
}
