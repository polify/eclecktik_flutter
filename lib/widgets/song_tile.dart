import 'package:eclektik/graphql/song_card_fragment.data.gql.dart';
import 'package:eclektik/music_player.dart';
import 'package:eclektik/palette.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class SongTile extends StatelessWidget {
  final GSongCard song;
  final _musicPlayer = GetIt.I<MusicPlayer>();
  SongTile({required this.song, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Card(
        child: ListTile(
            contentPadding: EdgeInsets.zero,
            horizontalTitleGap: 0.0,
            isThreeLine: song.artist != null && song.album != null,
            leading: InkWell(
              onTap: () => print("Details ${song.id}"),
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Image.network(
                  song.coverUrl ?? defaultNoCover,
                  fit: BoxFit.contain,
                  width: 100,
                  height: 100,
                ),
              ),
            ),
            tileColor: Colors.white,
            selectedTileColor: Colors.orange,
            selected: false,
            title: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Title : ",
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontWeight: FontWeight.bold),
                ),
                Flexible(
                  child: Text(
                    song.title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.grey.withOpacity(0.8)),
                  ),
                ),
              ],
            ),
            subtitle: song.artist != null && song.album != null
                ? Column(
                    children: [
                      song.duration.isNotEmpty
                          ? Row(
                              children: [
                                Text("Duration : ",
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.6),
                                        fontWeight: FontWeight.bold)),
                                Text(song.duration,
                                    style: TextStyle(
                                        color: Colors.grey.withOpacity(0.8))),
                              ],
                            )
                          : Container(),
                      song.artist != null
                          ? Row(
                              children: [
                                Text("Artist : ",
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.6),
                                        fontWeight: FontWeight.bold)),
                                Flexible(
                                  child: Text(
                                    (song.artist?.name ?? 'Unknow artist'),
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.justify,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.grey.withOpacity(0.8)),
                                  ),
                                ),
                              ],
                            )
                          : Container(),
                      song.album != null
                          ? Row(
                              children: [
                                Text("Album : ",
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.6),
                                        fontWeight: FontWeight.bold)),
                                Flexible(
                                  child: Text(
                                    (song.album?.name ?? 'Unknow album'),
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.justify,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.grey.withOpacity(0.8)),
                                  ),
                                ),
                              ],
                            )
                          : Container()
                    ],
                  )
                : null,
            trailing: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  InkWell(
                    onTap: () => _musicPlayer.replaceSong(song),
                    child: const Icon(
                      Icons.play_arrow,
                      size: 20,
                      color: Colors.grey,
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: () => _musicPlayer.addSong(song),
                    child: const Icon(
                      Icons.playlist_add,
                      size: 20,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
