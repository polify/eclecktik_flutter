import 'package:built_collection/built_collection.dart';
import 'package:eclektik/graphql/albums_query.req.gql.dart';
import 'package:eclektik/graphql/albums_query.var.gql.dart';
import 'package:eclektik/graphql/albums_query.data.gql.dart';
import 'package:eclektik/widgets/album_tile.dart';

import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get_it/get_it.dart';

class AllAlbumsScreen extends StatefulWidget {
  const AllAlbumsScreen({Key? key}) : super(key: key);

  @override
  State<AllAlbumsScreen> createState() => _AllAlbumsScreenState();
}

class _AllAlbumsScreenState extends State<AllAlbumsScreen> {
  final client = GetIt.I<Client>();
  static int _page = 0;
  static int _resultLenght = 0;
  static bool _lastPage = false;
  final Map<int, int> _pagination = {_page: _resultLenght};

  double lastOffset = 0.0;
  final _scrollController = ScrollController();
  final albumsReq = GAlbumsReq((b) => b
    ..requestId = 'AllAlbumsReq'
    ..fetchPolicy = FetchPolicy.CacheFirst
    ..vars.limit = 50
    ..vars.offset = 0);

  _nextPage() async {
    if ((_page > 0 && _pagination[_page - 1] == _pagination[_page])) {
      _lastPage = true;
      print("Last page $_lastPage $_pagination");
      return;
    }
    _page++;
    print("Page $_page Res $_resultLenght");
    final nextArtistsReq = albumsReq.rebuild(
      (b) => b
        ..vars.offset = _page
        ..updateResult = (previous, result) =>
            previous?.rebuild(
                (b) => b..albums.albums.addAll(result!.albums.albums)) ??
            result,
    );
    client.requestController.add(nextArtistsReq);
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      lastOffset = _scrollController.position.pixels;
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (!_lastPage) _nextPage();
      } else if (_scrollController.position.pixels ==
          _scrollController.position.minScrollExtent) {
        print("TOP-REFRESH");
        final refreshReq = albumsReq
            .rebuild((b) => b..fetchPolicy = FetchPolicy.CacheAndNetwork);
        client.requestController.add(refreshReq);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Operation<GAlbumsData, GAlbumsVars>(
      client: client,
      operationRequest: albumsReq,
      builder: (ctx, resp, err) {
        if (resp!.loading) {
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.red,
            ),
          );
        }
        final albums = resp.data?.albums.albums ?? BuiltList();
        _resultLenght = albums.length;
        _pagination[_page] = _resultLenght;
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "$_resultLenght / ${resp.data?.albums.total}",
                  textScaleFactor: 2.3,
                  style: const TextStyle(color: Colors.white),
                ),
                const Spacer(),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Play all"),
                    icon: const Icon(
                      Icons.play_arrow,
                      size: 30,
                      color: Colors.white,
                    )),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Add all to playlist"),
                    icon: const Icon(
                      Icons.playlist_add,
                      size: 30,
                      color: Colors.white,
                    )),
                IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () => print("Shuffle"),
                    icon: const Icon(
                      Icons.shuffle,
                      size: 30,
                      color: Colors.white,
                    ))
              ],
            ),
            Expanded(
              child: Scrollbar(
                isAlwaysShown: true,
                controller: _scrollController,
                child: GridView.builder(
                    controller: _scrollController,
                    itemCount: albums.length,
                    scrollDirection: Axis.horizontal,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            // maxCrossAxisExtent: 300,
                            childAspectRatio: (2 / 3),
                            crossAxisCount: 2,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 5),
                    itemBuilder: (ctx, index) =>
                        AlbumTile(album: albums[index])),
              ),
            ),
          ],
        );
      },
    );
  }
}
