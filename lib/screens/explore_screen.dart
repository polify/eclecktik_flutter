import 'package:eclektik/palette.dart';
import 'package:eclektik/widgets/search_results.dart';
import 'package:ferry/ferry.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:eclektik/graphql/search_query.req.gql.dart';
import 'package:eclektik/graphql/search_query.data.gql.dart';

class ExploreScreen extends StatefulWidget {
  ExploreScreen({Key? key}) : super(key: key);

  @override
  State<ExploreScreen> createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen> {
  final client = GetIt.I<Client>();

  final searchController = TextEditingController();
  GSearchData_search? results;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            alignment: Alignment.centerLeft,
            decoration: kBoxDecorationStyle.copyWith(
                color: Colors.red.withOpacity(0.6)),
            height: 60.0,
            child: TextField(
              controller: searchController,
              onSubmitted: (v) {
                final searchReq = GSearchReq((b) => b
                  ..fetchPolicy = FetchPolicy.NetworkOnly
                  ..vars.query = v);
                setState(() {
                  _isLoading = true;
                });
                client.request(searchReq).listen((event) {
                  if (event.loading) {
                    setState(() {
                      _isLoading = true;
                    });
                  } else if (!event.loading && event.data != null) {
                    print(
                      "Query results : ${event.data!.search}",
                    );
                    setState(() {
                      _isLoading = false;
                      results = event.data!.search;
                    });
                  } else if (event.hasErrors) {
                    print(event.graphqlErrors.toString());
                    print(event.linkException.toString());
                  }
                }, onError: (e) => print("Error : $e"));
              },
              style: const TextStyle(
                color: Colors.white,
                fontFamily: 'OpenSans',
              ),
              decoration: const InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                hintText: 'Search for artist, songs ...',
                hintStyle: kHintTextStyle,
              ),
            ),
          ),
        ),
        const SizedBox(height: 10.0),
        _isLoading
            ? const Center(
                child: CircularProgressIndicator(
                color: Colors.red,
              ))
            : Container(),
        results != null
            ? Expanded(child: SearchResults(results: results!))
            : Container()
      ],
    );
  }
}
