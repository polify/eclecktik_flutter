import 'dart:convert';

import 'package:eclektik/graphql/login_query.req.gql.dart';
import 'package:eclektik/graphql_client.dart';
import 'package:eclektik/palette.dart';
import 'package:ferry/ferry.dart';
import 'package:ferry_hive_store/ferry_hive_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';

String _fbImg =
    'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAB8ElEQVR4nO3bv0sdQRAH8EcSYpvGLsVzZ638AyyEYJFGUgV83sxqIWnTWIcUr0gRCBZ5UROc2aeVhb2dhX9C0tmJhUGMlbUkqUIge0L2eexw3Hxh2mO+H+7gfnC9nsWiGr/KTz3ysg/xjSPZ9Cg7EPizR9lxlWy7SrZdiFuA8mmGeAQhfvRBBtp73yt+aTTlkV8DyTcg+ZU9lXzR7jBxZoM8A5KziYq3HcATrwHx7b3KtxXAhfHzRsq3EaC/vvcEUL43Ur6NAEDyrrHybQPwS6OpGeTrzgI4lBeNlm8dAMlmpwEg8HFuQYfyA0j2XYhbdQPIlXav/w4Qn2YBBDn3g/G09t6NBYgvsgBQ3mrv3GiA5DLvDIjr2js3GgMwAAMwgE4AeORlR3EjHb7JuxGKB/XH+TuA8aV23ySAfJJ7xzf5xAPtvkmKAoTdD9p9k5QEcBQ3tPsmKQngKa5o901S9BJYlQXtvklKAvRxr6/dN0k5AP45Nzh8rN03SSkAR3yl3bU2pQA87X7V7lqbYpdAJUfaXeszOHy4uDh89O/kvhJ3KK/qjvNnesPhA+2qWcn+JtDWh6G7YgAGYAAGYAAGYAAGYAAGYAAGYAAGYAAGYAAGYAAGYAAGYAAGYAAG0BEAj/L+rh8f6mYWx/PaO1u6lN93hhbZoJqj+AAAAABJRU5ErkJggg==';

String _ggloImg =
    'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAGhElEQVR4nO2afWwTZRzHn2PAJipREEdgvsRMxvo8124UxLH27uaYuPWug2Ah+wMy1CxAQhSUhMS1TMJL4A9JMBqIhK29bkDBILK2yJQRQREFBInEEJFAe52j6+vGmwiPf+DY2Gj3dLv2nO6b/P59nt/nc8/dc28ADGUoQxlKinKpksu4UgyntXK5s1tY1SIfSy/36mlzC6d+z8uql7Rw6te9JXl52MQ9pnSvsgQvy07/g1EbfRz61MuhMxKLbnsZiEnKw8BfvAy91aOHc7HJ9IjSLAnFW5xf4NUjm4eBEVLgeCUxMCpxsNZfNDVHaba4ufJKLiPp6SY5oGOIuCPp6R0tr+U9rzTrA/GX5EzwcPS+ZIH3LvSnxKKPI7MKxigKjgGgfHr4lpdF4dTBd1sRLLx0ldHkKwO/LDvdy6lEJcAfKBbebGFVi1IK316qHycx6Jji8N3Kx8ANqYPn0K9KA/coX6A4X5V0eFyaPVpi6FP/AuD7JTHw8tVZU7KTDn9Sqx3hZdFheZpG13ws+tHL0k6JQbvu7SDoiI+FLQmNpUe/+cpyn0s6PAAA+Di0doDgkqcIbfKW5OVhAIbFmsdfkjPBV0RXefWoOa5EFp33l+RMSAm8Z2YeJzHwbr+Oth4ebZ2ZOxObTGmJzuvT0VqJRYd6jcuhM+2l+nHJYO0VfBKMal08sXcTfZ+bUa8eLY53tInmB4DysZoFEos6/hn7xGUd/aRcfH0m7AarIi7qRsvirNPEAlh0OFA6PUvOPlqLoEbi0E5cmj1aznHjBh8EY8IuKhx2UjgBCV8Puqe3WIk6h28IOyncWX1J8LDwG7xA/ajSfcsS7AAjI07K311AXAks+tlvzHlc6b5lS9QNjD3hY0mQWHTbW5KXp3TPsibsovbGEtBbAr1Z6X5lDW4GGREndSuegE4JrUuyjl8qmzZe6Z5lzfVGMKMv+M4KNabZle5X9oScYAWpgHAjMCndr+wJu9McJPARJ3XLfwz8d678nQm7qLNEy99FHZNjPvaDoKAzBzqSWpaAIxEBHqIV4E7bKYcArjo0u9AcwMksvTlwnlyAk7pGIiDqHP7hYBGgMwc6iJrBzSCD9AIYdYOVg0VAoTmAAcZ9P5liF0gnFRBxgrcHkwDjRj/ZBTvspKIkAtrdoHowCdBW4RGkAn4nWgXutG2DRYDO3HaTuKGIi/qBbBegvhssAgrNwTbihhK5EQrtA08MDgFtp4kbCrnAO8Q7QSN4c6ACite3q5ia8NpES7e6bQP5KRDcTdzQ9QPgJeJnATfVPFAB/Y1+XXgKqQCmJryWeGDsACMjLuoG0Wngou42Nb3wbBI5Y4Yxt1UTr4DqIJ/Q4CEXdYBEwJ59WUeg3VCbJMaYMTnwyEJLQCJc/n+VbsGJvU2OHgTlRPA2HkMbj9W7hJeTxPrQMDWhN4iPviXwfcIT4JNgRMRJtZDAQxuPkchfpOsNKflgUbapY3yhJRggFTDDHFjRr4l6vhaPBd9ZtI131jTXDJeZt0cwVWgJfU68/VmCd8o2dfTvlR3eDzLDLipCAt+1EoS92m1aslvOxDuiCmtCWxLb/0P7BzRl6CB4lxT+folG92TrwrEyUd8LxsP0q0MfJXoDxNX4pw5s3nsfSC4Qw3ddEzyw3lgkB/u0HeXPINFwKP+T9UcSgddZ2r6QY35g+UxTBEX+biIC7peV36+pE2B/5p1sXThWbS9/H4p8pHM8Ugk6c9tN3froJFkEAAAArDeu6ZeArhXxLV1vqMqtn/siwICKNQ9qqMhEojAPiYbtSBSuP2wsEgmMObhKNngAADA5TGnQKnw1EAldJbTRVuE4beOdKqthN7ILX9KicArZhCukY8SToFsdPEr87J9IChymMdDO/ySPhIHXwyToLQHP9HUdmbLDd2bSgYqnaJE/pzT8wyTozMFgwZpQ8v8cRQ0VmdDGn1AavrsEnSXon7EulLov1FxtZQZtFXYoDQ9tPKZF/tzUjaICv9JjQNH1hipoE6KKwduMm7nayozUw3eLxmGaCO38npTCi0afpm72q4qC9wysNxbRNr4pmeBI5C9Ca/lS7baqUUrzxgxdZ9Cq7EZR5lPjBBKFecl/2pQx2a5l6RqrwCPRsB3ZhLNI5G8TA1v5C7TdsFVj5eejhork7eupDFdbmUHXGbTIXj5H1cAvom3ly2mrway28iuhtXypxsrPV9vKpqttc55WutehDGUo/4/8DR2zTJlnHizvAAAAAElFTkSuQmCC';

final getIt = GetIt.instance;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isLoading = false;

  final usernameController = TextEditingController();

  final passwordController = TextEditingController();

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          'Username',
          style: kLabelStyle,
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: usernameController,
            keyboardType: TextInputType.text,
            autocorrect: false,
            textCapitalization: TextCapitalization.none,
            style: const TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: const InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.account_circle,
                color: Colors.white,
              ),
              hintText: 'Enter your Username',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          'Password',
          style: kLabelStyle,
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            obscureText: true,
            controller: passwordController,
            onSubmitted: (v) => _doLogin(context),
            style: const TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: const InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Enter your Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () => print('Forgot Password Button Pressed'),
        // padding: EdgeInsets.only(right: 0.0),
        child: const Text(
          'Forgot Password?',
          style: kLabelStyle,
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return SizedBox(
      height: 20.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: true,
              checkColor: const Color(0xFF527DAA),
              activeColor: Colors.white,
              onChanged: (value) {
                // setState(() {
                //   _rememberMe = value!;
                // });
              },
            ),
          ),
          const Text(
            'Remember me',
            style: kLabelStyle,
          ),
        ],
      ),
    );
  }

  void _doLogin(BuildContext context) {
    setState(() {
      _isLoading = true;
    });
    final client = getIt<Client>();
    var req = client.request(GLoginReq(
      (b) => b
        ..fetchPolicy = FetchPolicy.NetworkOnly
        ..vars.username = usernameController.text
        ..vars.password = passwordController.text,
    ));
    print("Will login");
    req.listen((
      resp,
    ) async {
      setState(() {
        _isLoading = false;
      });
      print("Login : $resp");

      if (resp.linkException != null) {
        print(resp.linkException!.originalException);
      } else if (resp.hasErrors) {
        print("Errors : ${resp.graphqlErrors!.join('')}");
      } else if (!resp.loading && resp.data != null) {
        if (resp.data!.login.accessToken is String) {
          final box = await Hive.openBox("eclektik");
          final store = HiveStore(box);
          store.put("auth", {"token": resp.data!.login.accessToken});
          await GetIt.I.unregister<Client>();
          final client = await initClient();
          getIt.registerLazySingleton<Client>(() => client);

          Navigator.pushReplacementNamed(context, "home");
        }
      }
      setState(() {
        _isLoading = true;
      });
    }, onError: (e) => print(e), onDone: () => print('done'));
  }

  Widget _buildLoginBtn(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: _isLoading
          ? const Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.white,
              ),
            )
          : OutlinedButton(
              // elevation: 5.0,
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.resolveWith<Color>((states) {
                  return Colors.white;
                }),
                shape: MaterialStateProperty.resolveWith<OutlinedBorder>((_) {
                  return RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16));
                }),
              ),
              onPressed: () => _doLogin(context),
              child: const Text(
                'LOGIN',
                style: TextStyle(
                  color: Color(0xFF527DAA),
                  letterSpacing: 1.5,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
    );
  }

  Widget _buildSignInWithText() {
    return Column(
      children: const <Widget>[
        Text(
          '- OR -',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(height: 20.0),
        Text(
          'Sign in with',
          style: kLabelStyle,
        ),
      ],
    );
  }

  Widget _buildSocialBtn(void Function()? onTap, Image logo) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 60.0,
        width: 60.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0, 2),
              blurRadius: 6.0,
            ),
          ],
          image: DecorationImage(
            image: logo.image,
          ),
        ),
      ),
    );
  }

  Widget _buildSocialBtnRow() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _buildSocialBtn(
            () => print('Login with Facebook'),
            Image.memory(base64Decode(_fbImg)),
          ),
          _buildSocialBtn(
            () => print('Login with Google'),
            Image.memory(base64Decode(_ggloImg)),
          ),
        ],
      ),
    );
  }

  Widget _buildSignupBtn() {
    return GestureDetector(
      onTap: () => print('Sign Up Button Pressed'),
      child: RichText(
        text: const TextSpan(
          children: [
            TextSpan(
              text: 'Don\'t have an Account? ',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w400,
              ),
            ),
            TextSpan(
              text: 'Sign Up',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFF73AEF5),
                      Color(0xFF61A4F1),
                      Color(0xFF478DE0),
                      Color(0xFF398AE5),
                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              SizedBox(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 100.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Text(
                        'Sign In',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 30.0),
                      _buildEmailTF(),
                      const SizedBox(
                        height: 30.0,
                      ),
                      _buildPasswordTF(context),
                      _buildForgotPasswordBtn(),
                      _buildRememberMeCheckbox(),
                      _buildLoginBtn(context),
                      _buildSignInWithText(),
                      _buildSocialBtnRow(),
                      _buildSignupBtn(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
