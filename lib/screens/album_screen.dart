import 'package:eclektik/music_player.dart';
import 'package:eclektik/palette.dart';
import 'package:eclektik/widgets/song_tile.dart';
import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../graphql/album_query.req.gql.dart';
import '../graphql/album_query.var.gql.dart';
import '../graphql/album_query.data.gql.dart';
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart';

class AlbumScreen extends StatelessWidget {
  final GAlbumItem album;
  final client = GetIt.I<Client>();
  final _musicPlayer = GetIt.I<MusicPlayer>();

  AlbumScreen({required this.album, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(album.name),
      ),
      body: Container(
        decoration: kScaffoldBackgroundStyleDark,
        child: Operation<GAlbumData, GAlbumVars>(
          client: client,
          operationRequest: GAlbumReq((b) => b
            ..requestId = 'Album${album.id}Req'
            ..fetchPolicy = FetchPolicy.CacheAndNetwork
            ..vars.id = album.id),
          builder: (ctx, resp, err) {
            if (resp!.loading || resp.data == null) {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
            final al = resp.data!.album;
            return Column(
              children: [
                Image.network(album.coverUrl ?? defaultNoCover),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            album.name,
                            textScaleFactor: 1.3,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            al.artists.map((p0) => p0.name).join(","),
                            textScaleFactor: 1.1,
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.black),
                              // onPressed: () => _musicPlayer.,
                              onPressed: () {
                                print("ok");
                                List<GSongCard> _songs = [];
                                for (var sng in al.songs) {
                                  _songs.add(sng);
                                }
                                _musicPlayer.addManySong(_songs, true);
                              },
                              child: Icon(Icons.play_arrow)),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.black),
                              // onPressed: () => _musicPlayer.,
                              onPressed: () {
                                print("ok");
                                List<GSongCard> _songs = [];
                                for (var sng in al.songs) {
                                  _songs.add(sng);
                                }
                                _musicPlayer.addManySong(_songs, false);
                              },
                              child: Icon(Icons.add)),
                        ),
                      ],
                    )
                  ],
                ),
                Expanded(
                    child: ListView.builder(
                        itemCount: al.songs.length,
                        itemBuilder: (ctx, i) {
                          GSongCard _song = al.songs[i];
                          return SizedBox(
                            height: 80,
                            child: SongTile(song: _song),
                          );
                        }))
              ],
            );
          },
        ),
      ),
    );
  }
}
