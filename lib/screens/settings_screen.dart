import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:eclektik/graphql/profile_query.req.gql.dart';
import 'package:eclektik/graphql/profile_query.data.gql.dart';

class SettingsScreen extends StatelessWidget {
  final client = GetIt.I<Client>();
  final profileReq = GProfileReq((b) => b
    ..requestId = 'ProfileReq'
    ..fetchPolicy = FetchPolicy.CacheAndNetwork);

  SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Operation<GProfileData, dynamic>(
          client: client,
          operationRequest: profileReq,
          builder: (ctx, resp, err) {
            if (resp!.loading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            final profile = resp.data!.profile;
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(profile.name.toUpperCase()),
                Text(profile.createdAt.value),
                Text(profile.updatedAt.value),
                Text("Bucket :"),
                Row(
                  children: [
                    Text(profile.s3Credential?.id.substring(10) ?? "-"),
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Text("-"),
                    ),
                    Text(profile.s3Credential?.createdAt.value ?? "-")
                  ],
                ),
                Text("Devices :"),
                Expanded(
                  child: ListView.builder(
                      itemCount: profile.devices.length,
                      itemBuilder: (ctx, i) {
                        return Container(
                          child: Card(
                            child: Row(
                              children: [
                                Text(profile.devices[i].os),
                                Text(" - "),
                                Text(profile.devices[i].type),
                                Flexible(
                                  child: Text(
                                    "... ${profile.devices[i].userAgent}",
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ],
            );
          }),
    );
  }
}
