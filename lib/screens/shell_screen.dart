import 'dart:io';

// import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:audio_service/audio_service.dart';
import 'package:eclektik/screens/explore_screen.dart';
import 'package:ferry/ferry.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:eclektik/widgets/all_albums.dart';
import 'package:eclektik/widgets/all_artists.dart';
import 'package:eclektik/widgets/all_songs.dart';
import 'package:eclektik/palette.dart';
import 'package:eclektik/widgets/player_widget.dart';
import 'package:get_it/get_it.dart';

class ShellScreen extends StatefulWidget {
  const ShellScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<ShellScreen> createState() => _ShellScreenState();
}

class _ShellScreenState extends State<ShellScreen> {
  int index = 0;

  final _labels = [
    "Explore",
    "Albums",
    "Songs",
    "Artists",
  ];
  final _widgets = [
    ExploreScreen(),
    const AllAlbumsScreen(),
    const AllSongsScreen(),
    const AllArtistsScreen(),
  ];

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.black));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF2B2B2B),
      bottomNavigationBar: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
          ],
        ),
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          child: BottomNavigationBar(
            onTap: (v) => setState(() {
              index = v;
            }),
            currentIndex: index,
            // selectedItemColor: const Color(0xFF2B2B2B),
            selectedItemColor: Colors.red.shade300,

            items: const [
              BottomNavigationBarItem(
                  backgroundColor: Colors.black,
                  label: 'Explore',
                  icon: Icon(Icons.explore)),
              BottomNavigationBarItem(
                  backgroundColor: Colors.black,
                  label: 'Albums',
                  icon: Icon(Icons.my_library_music_sharp)),
              BottomNavigationBarItem(
                  backgroundColor: Colors.black,
                  label: 'Songs',
                  icon: Icon(Icons.music_note_rounded)),
              BottomNavigationBarItem(
                  backgroundColor: Colors.black,
                  label: 'Artists',
                  icon: Icon(Icons.music_video_rounded)),
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          // decoration: kScaffoldBackgroundStyle,
          decoration: kScaffoldBackgroundStyleDark,
          // decoration: BoxDecoration(color: Colors.black),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      _labels[index],
                      style: const TextStyle(
                        color: Colors.white,
                        fontFamily: 'OpenSans',
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                      onPressed: () {
                        print("logout");
                        final client = GetIt.I<Client>();
                        client.cache.clear();
                        Navigator.pushReplacementNamed(context, "login");
                      },
                      icon: const Icon(
                        Icons.logout,
                        color: Colors.white,
                      )),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: IconButton(
                        onPressed: () {
                          print("settings");
                          Navigator.pushNamed(context, "settings");
                        },
                        icon: const Icon(
                          Icons.settings,
                          color: Colors.white,
                        )),
                  ),
                ],
              ),
              const SizedBox(
                height: 10.0,
              ),
              Expanded(child: _widgets[index]),
              Platform.isAndroid || Platform.isIOS
                  ? AudioServiceWidget(child: const PlayerWidget())
                  : const PlayerWidget()
            ],
          ),
        ),
      ),
    );
  }
}
