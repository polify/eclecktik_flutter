import 'package:eclektik/graphql/artist_item_fragment.data.gql.dart';
import 'package:eclektik/graphql/artist_query.data.gql.dart';
import 'package:eclektik/graphql/artist_query.var.gql.dart';
import 'package:eclektik/graphql/artist_query.req.gql.dart';
import 'package:eclektik/palette.dart';
import 'package:ferry/ferry.dart';
import 'package:ferry_flutter/ferry_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class ArtistScreen extends StatelessWidget {
  final GArtistItem artist;
  final client = GetIt.I<Client>();
  ArtistScreen({required this.artist, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(artist.name),
      ),
      body: Container(
        decoration: kScaffoldBackgroundStyleDark,
        child: Operation<GArtistData, GArtistVars>(
          client: client,
          operationRequest: GArtistReq((b) => b
            ..requestId = 'Artist${artist.id}Req'
            ..fetchPolicy = FetchPolicy.CacheAndNetwork
            ..vars.id = artist.id),
          builder: (ctx, resp, err) {
            if (resp!.loading || resp.data == null) {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
            final ar = resp.data!.artist;
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network(
                    artist.coverUrl != null && !artist.coverUrl!.contains("gif")
                        ? artist.coverUrl!
                        : defaultNoCover,
                    height: 300,
                    width: double.infinity,
                    fit: BoxFit.fill,
                  ),
                ),
                const Text(
                  "Albums",
                  style: TextStyle(color: Colors.white),
                ),
                Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: ar.albums.length,
                      itemBuilder: (ctx, i) {
                        return SizedBox(
                          height: 20,
                          width: 88,
                          child: Card(
                            child: InkWell(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  const Image(
                                    image: NetworkImage(defaultNoCover),
                                  ),
                                  Text(ar.albums[i].name),
                                ],
                              ),
                              onTap: () => Navigator.of(context).pushNamed(
                                  "album",
                                  arguments: {"album": ar.albums[i]}),
                            ),
                          ),
                        );
                      }),
                ),
                const Text(
                  "Titles",
                  style: TextStyle(color: Colors.white),
                ),
                Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: ar.songs.length,
                      itemBuilder: (ctx, i) {
                        return SizedBox(
                          height: 40,
                          width: 20,
                          child: Card(
                            child: Text(ar.songs[i].title),
                          ),
                        );
                      }),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
