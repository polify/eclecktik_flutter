// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GAlbumData> _$gAlbumDataSerializer = new _$GAlbumDataSerializer();
Serializer<GAlbumData_album> _$gAlbumDataAlbumSerializer =
    new _$GAlbumData_albumSerializer();
Serializer<GAlbumData_album_artists> _$gAlbumDataAlbumArtistsSerializer =
    new _$GAlbumData_album_artistsSerializer();
Serializer<GAlbumData_album_songs> _$gAlbumDataAlbumSongsSerializer =
    new _$GAlbumData_album_songsSerializer();
Serializer<GAlbumData_album_songs_signedUrl>
    _$gAlbumDataAlbumSongsSignedUrlSerializer =
    new _$GAlbumData_album_songs_signedUrlSerializer();
Serializer<GAlbumData_album_songs_album> _$gAlbumDataAlbumSongsAlbumSerializer =
    new _$GAlbumData_album_songs_albumSerializer();
Serializer<GAlbumData_album_songs_artist>
    _$gAlbumDataAlbumSongsArtistSerializer =
    new _$GAlbumData_album_songs_artistSerializer();

class _$GAlbumDataSerializer implements StructuredSerializer<GAlbumData> {
  @override
  final Iterable<Type> types = const [GAlbumData, _$GAlbumData];
  @override
  final String wireName = 'GAlbumData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GAlbumData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'album',
      serializers.serialize(object.album,
          specifiedType: const FullType(GAlbumData_album)),
    ];

    return result;
  }

  @override
  GAlbumData deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'album':
          result.album.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GAlbumData_album))!
              as GAlbumData_album);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData_albumSerializer
    implements StructuredSerializer<GAlbumData_album> {
  @override
  final Iterable<Type> types = const [GAlbumData_album, _$GAlbumData_album];
  @override
  final String wireName = 'GAlbumData_album';

  @override
  Iterable<Object?> serialize(Serializers serializers, GAlbumData_album object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
      'artists',
      serializers.serialize(object.artists,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GAlbumData_album_artists)])),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GAlbumData_album_songs)])),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GAlbumData_album deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumData_albumBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'artists':
          result.artists.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GAlbumData_album_artists)
              ]))! as BuiltList<Object?>);
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GAlbumData_album_songs)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData_album_artistsSerializer
    implements StructuredSerializer<GAlbumData_album_artists> {
  @override
  final Iterable<Type> types = const [
    GAlbumData_album_artists,
    _$GAlbumData_album_artists
  ];
  @override
  final String wireName = 'GAlbumData_album_artists';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumData_album_artists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GAlbumData_album_artists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumData_album_artistsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData_album_songsSerializer
    implements StructuredSerializer<GAlbumData_album_songs> {
  @override
  final Iterable<Type> types = const [
    GAlbumData_album_songs,
    _$GAlbumData_album_songs
  ];
  @override
  final String wireName = 'GAlbumData_album_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumData_album_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.signedUrl;
    if (value != null) {
      result
        ..add('signedUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GAlbumData_album_songs_signedUrl)));
    }
    value = object.album;
    if (value != null) {
      result
        ..add('album')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GAlbumData_album_songs_album)));
    }
    value = object.artist;
    if (value != null) {
      result
        ..add('artist')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GAlbumData_album_songs_artist)));
    }
    return result;
  }

  @override
  GAlbumData_album_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumData_album_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'signedUrl':
          result.signedUrl.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GAlbumData_album_songs_signedUrl))!
              as GAlbumData_album_songs_signedUrl);
          break;
        case 'album':
          result.album.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GAlbumData_album_songs_album))!
              as GAlbumData_album_songs_album);
          break;
        case 'artist':
          result.artist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GAlbumData_album_songs_artist))!
              as GAlbumData_album_songs_artist);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData_album_songs_signedUrlSerializer
    implements StructuredSerializer<GAlbumData_album_songs_signedUrl> {
  @override
  final Iterable<Type> types = const [
    GAlbumData_album_songs_signedUrl,
    _$GAlbumData_album_songs_signedUrl
  ];
  @override
  final String wireName = 'GAlbumData_album_songs_signedUrl';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumData_album_songs_signedUrl object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'signedUrl',
      serializers.serialize(object.signedUrl,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumData_album_songs_signedUrl deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumData_album_songs_signedUrlBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'signedUrl':
          result.signedUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData_album_songs_albumSerializer
    implements StructuredSerializer<GAlbumData_album_songs_album> {
  @override
  final Iterable<Type> types = const [
    GAlbumData_album_songs_album,
    _$GAlbumData_album_songs_album
  ];
  @override
  final String wireName = 'GAlbumData_album_songs_album';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumData_album_songs_album object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumData_album_songs_album deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumData_album_songs_albumBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData_album_songs_artistSerializer
    implements StructuredSerializer<GAlbumData_album_songs_artist> {
  @override
  final Iterable<Type> types = const [
    GAlbumData_album_songs_artist,
    _$GAlbumData_album_songs_artist
  ];
  @override
  final String wireName = 'GAlbumData_album_songs_artist';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumData_album_songs_artist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumData_album_songs_artist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumData_album_songs_artistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumData extends GAlbumData {
  @override
  final String G__typename;
  @override
  final GAlbumData_album album;

  factory _$GAlbumData([void Function(GAlbumDataBuilder)? updates]) =>
      (new GAlbumDataBuilder()..update(updates)).build();

  _$GAlbumData._({required this.G__typename, required this.album}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(album, 'GAlbumData', 'album');
  }

  @override
  GAlbumData rebuild(void Function(GAlbumDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumDataBuilder toBuilder() => new GAlbumDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData &&
        G__typename == other.G__typename &&
        album == other.album;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), album.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData')
          ..add('G__typename', G__typename)
          ..add('album', album))
        .toString();
  }
}

class GAlbumDataBuilder implements Builder<GAlbumData, GAlbumDataBuilder> {
  _$GAlbumData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GAlbumData_albumBuilder? _album;
  GAlbumData_albumBuilder get album =>
      _$this._album ??= new GAlbumData_albumBuilder();
  set album(GAlbumData_albumBuilder? album) => _$this._album = album;

  GAlbumDataBuilder() {
    GAlbumData._initializeBuilder(this);
  }

  GAlbumDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _album = $v.album.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData;
  }

  @override
  void update(void Function(GAlbumDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData build() {
    _$GAlbumData _$result;
    try {
      _$result = _$v ??
          new _$GAlbumData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumData', 'G__typename'),
              album: album.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'album';
        album.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumData_album extends GAlbumData_album {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;
  @override
  final BuiltList<GAlbumData_album_artists> artists;
  @override
  final BuiltList<GAlbumData_album_songs> songs;

  factory _$GAlbumData_album(
          [void Function(GAlbumData_albumBuilder)? updates]) =>
      (new GAlbumData_albumBuilder()..update(updates)).build();

  _$GAlbumData_album._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt,
      required this.artists,
      required this.songs})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData_album', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GAlbumData_album', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GAlbumData_album', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GAlbumData_album', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GAlbumData_album', 'updatedAt');
    BuiltValueNullFieldError.checkNotNull(
        artists, 'GAlbumData_album', 'artists');
    BuiltValueNullFieldError.checkNotNull(songs, 'GAlbumData_album', 'songs');
  }

  @override
  GAlbumData_album rebuild(void Function(GAlbumData_albumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumData_albumBuilder toBuilder() =>
      new GAlbumData_albumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData_album &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        artists == other.artists &&
        songs == other.songs;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                            name.hashCode),
                        coverUrl.hashCode),
                    createdAt.hashCode),
                updatedAt.hashCode),
            artists.hashCode),
        songs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData_album')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('artists', artists)
          ..add('songs', songs))
        .toString();
  }
}

class GAlbumData_albumBuilder
    implements Builder<GAlbumData_album, GAlbumData_albumBuilder> {
  _$GAlbumData_album? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  ListBuilder<GAlbumData_album_artists>? _artists;
  ListBuilder<GAlbumData_album_artists> get artists =>
      _$this._artists ??= new ListBuilder<GAlbumData_album_artists>();
  set artists(ListBuilder<GAlbumData_album_artists>? artists) =>
      _$this._artists = artists;

  ListBuilder<GAlbumData_album_songs>? _songs;
  ListBuilder<GAlbumData_album_songs> get songs =>
      _$this._songs ??= new ListBuilder<GAlbumData_album_songs>();
  set songs(ListBuilder<GAlbumData_album_songs>? songs) =>
      _$this._songs = songs;

  GAlbumData_albumBuilder() {
    GAlbumData_album._initializeBuilder(this);
  }

  GAlbumData_albumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _artists = $v.artists.toBuilder();
      _songs = $v.songs.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData_album other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData_album;
  }

  @override
  void update(void Function(GAlbumData_albumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData_album build() {
    _$GAlbumData_album _$result;
    try {
      _$result = _$v ??
          new _$GAlbumData_album._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumData_album', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GAlbumData_album', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GAlbumData_album', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              artists: artists.build(),
              songs: songs.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 'artists';
        artists.build();
        _$failedField = 'songs';
        songs.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumData_album', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumData_album_artists extends GAlbumData_album_artists {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GAlbumData_album_artists(
          [void Function(GAlbumData_album_artistsBuilder)? updates]) =>
      (new GAlbumData_album_artistsBuilder()..update(updates)).build();

  _$GAlbumData_album_artists._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData_album_artists', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GAlbumData_album_artists', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GAlbumData_album_artists', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GAlbumData_album_artists', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GAlbumData_album_artists', 'updatedAt');
  }

  @override
  GAlbumData_album_artists rebuild(
          void Function(GAlbumData_album_artistsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumData_album_artistsBuilder toBuilder() =>
      new GAlbumData_album_artistsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData_album_artists &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData_album_artists')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GAlbumData_album_artistsBuilder
    implements
        Builder<GAlbumData_album_artists, GAlbumData_album_artistsBuilder> {
  _$GAlbumData_album_artists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GAlbumData_album_artistsBuilder() {
    GAlbumData_album_artists._initializeBuilder(this);
  }

  GAlbumData_album_artistsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData_album_artists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData_album_artists;
  }

  @override
  void update(void Function(GAlbumData_album_artistsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData_album_artists build() {
    _$GAlbumData_album_artists _$result;
    try {
      _$result = _$v ??
          new _$GAlbumData_album_artists._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumData_album_artists', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GAlbumData_album_artists', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GAlbumData_album_artists', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumData_album_artists', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumData_album_songs extends GAlbumData_album_songs {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final String duration;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;
  @override
  final GAlbumData_album_songs_signedUrl? signedUrl;
  @override
  final GAlbumData_album_songs_album? album;
  @override
  final GAlbumData_album_songs_artist? artist;

  factory _$GAlbumData_album_songs(
          [void Function(GAlbumData_album_songsBuilder)? updates]) =>
      (new GAlbumData_album_songsBuilder()..update(updates)).build();

  _$GAlbumData_album_songs._(
      {required this.G__typename,
      required this.s3Key,
      required this.duration,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt,
      this.signedUrl,
      this.album,
      this.artist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData_album_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        s3Key, 'GAlbumData_album_songs', 's3Key');
    BuiltValueNullFieldError.checkNotNull(
        duration, 'GAlbumData_album_songs', 'duration');
    BuiltValueNullFieldError.checkNotNull(id, 'GAlbumData_album_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GAlbumData_album_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GAlbumData_album_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GAlbumData_album_songs', 'updatedAt');
  }

  @override
  GAlbumData_album_songs rebuild(
          void Function(GAlbumData_album_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumData_album_songsBuilder toBuilder() =>
      new GAlbumData_album_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData_album_songs &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        duration == other.duration &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        signedUrl == other.signedUrl &&
        album == other.album &&
        artist == other.artist;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, G__typename.hashCode),
                                            s3Key.hashCode),
                                        duration.hashCode),
                                    id.hashCode),
                                title.hashCode),
                            coverUrl.hashCode),
                        createdAt.hashCode),
                    updatedAt.hashCode),
                signedUrl.hashCode),
            album.hashCode),
        artist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData_album_songs')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('duration', duration)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('signedUrl', signedUrl)
          ..add('album', album)
          ..add('artist', artist))
        .toString();
  }
}

class GAlbumData_album_songsBuilder
    implements Builder<GAlbumData_album_songs, GAlbumData_album_songsBuilder> {
  _$GAlbumData_album_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _duration;
  String? get duration => _$this._duration;
  set duration(String? duration) => _$this._duration = duration;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GAlbumData_album_songs_signedUrlBuilder? _signedUrl;
  GAlbumData_album_songs_signedUrlBuilder get signedUrl =>
      _$this._signedUrl ??= new GAlbumData_album_songs_signedUrlBuilder();
  set signedUrl(GAlbumData_album_songs_signedUrlBuilder? signedUrl) =>
      _$this._signedUrl = signedUrl;

  GAlbumData_album_songs_albumBuilder? _album;
  GAlbumData_album_songs_albumBuilder get album =>
      _$this._album ??= new GAlbumData_album_songs_albumBuilder();
  set album(GAlbumData_album_songs_albumBuilder? album) =>
      _$this._album = album;

  GAlbumData_album_songs_artistBuilder? _artist;
  GAlbumData_album_songs_artistBuilder get artist =>
      _$this._artist ??= new GAlbumData_album_songs_artistBuilder();
  set artist(GAlbumData_album_songs_artistBuilder? artist) =>
      _$this._artist = artist;

  GAlbumData_album_songsBuilder() {
    GAlbumData_album_songs._initializeBuilder(this);
  }

  GAlbumData_album_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _duration = $v.duration;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _signedUrl = $v.signedUrl?.toBuilder();
      _album = $v.album?.toBuilder();
      _artist = $v.artist?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData_album_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData_album_songs;
  }

  @override
  void update(void Function(GAlbumData_album_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData_album_songs build() {
    _$GAlbumData_album_songs _$result;
    try {
      _$result = _$v ??
          new _$GAlbumData_album_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumData_album_songs', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GAlbumData_album_songs', 's3Key'),
              duration: BuiltValueNullFieldError.checkNotNull(
                  duration, 'GAlbumData_album_songs', 'duration'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GAlbumData_album_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GAlbumData_album_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              signedUrl: _signedUrl?.build(),
              album: _album?.build(),
              artist: _artist?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 'signedUrl';
        _signedUrl?.build();
        _$failedField = 'album';
        _album?.build();
        _$failedField = 'artist';
        _artist?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumData_album_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumData_album_songs_signedUrl
    extends GAlbumData_album_songs_signedUrl {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String signedUrl;

  factory _$GAlbumData_album_songs_signedUrl(
          [void Function(GAlbumData_album_songs_signedUrlBuilder)? updates]) =>
      (new GAlbumData_album_songs_signedUrlBuilder()..update(updates)).build();

  _$GAlbumData_album_songs_signedUrl._(
      {required this.G__typename, required this.id, required this.signedUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData_album_songs_signedUrl', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GAlbumData_album_songs_signedUrl', 'id');
    BuiltValueNullFieldError.checkNotNull(
        signedUrl, 'GAlbumData_album_songs_signedUrl', 'signedUrl');
  }

  @override
  GAlbumData_album_songs_signedUrl rebuild(
          void Function(GAlbumData_album_songs_signedUrlBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumData_album_songs_signedUrlBuilder toBuilder() =>
      new GAlbumData_album_songs_signedUrlBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData_album_songs_signedUrl &&
        G__typename == other.G__typename &&
        id == other.id &&
        signedUrl == other.signedUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), id.hashCode), signedUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData_album_songs_signedUrl')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('signedUrl', signedUrl))
        .toString();
  }
}

class GAlbumData_album_songs_signedUrlBuilder
    implements
        Builder<GAlbumData_album_songs_signedUrl,
            GAlbumData_album_songs_signedUrlBuilder> {
  _$GAlbumData_album_songs_signedUrl? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _signedUrl;
  String? get signedUrl => _$this._signedUrl;
  set signedUrl(String? signedUrl) => _$this._signedUrl = signedUrl;

  GAlbumData_album_songs_signedUrlBuilder() {
    GAlbumData_album_songs_signedUrl._initializeBuilder(this);
  }

  GAlbumData_album_songs_signedUrlBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _signedUrl = $v.signedUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData_album_songs_signedUrl other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData_album_songs_signedUrl;
  }

  @override
  void update(void Function(GAlbumData_album_songs_signedUrlBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData_album_songs_signedUrl build() {
    final _$result = _$v ??
        new _$GAlbumData_album_songs_signedUrl._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GAlbumData_album_songs_signedUrl', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumData_album_songs_signedUrl', 'id'),
            signedUrl: BuiltValueNullFieldError.checkNotNull(
                signedUrl, 'GAlbumData_album_songs_signedUrl', 'signedUrl'));
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumData_album_songs_album extends GAlbumData_album_songs_album {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GAlbumData_album_songs_album(
          [void Function(GAlbumData_album_songs_albumBuilder)? updates]) =>
      (new GAlbumData_album_songs_albumBuilder()..update(updates)).build();

  _$GAlbumData_album_songs_album._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData_album_songs_album', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GAlbumData_album_songs_album', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GAlbumData_album_songs_album', 'name');
  }

  @override
  GAlbumData_album_songs_album rebuild(
          void Function(GAlbumData_album_songs_albumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumData_album_songs_albumBuilder toBuilder() =>
      new GAlbumData_album_songs_albumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData_album_songs_album &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData_album_songs_album')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GAlbumData_album_songs_albumBuilder
    implements
        Builder<GAlbumData_album_songs_album,
            GAlbumData_album_songs_albumBuilder> {
  _$GAlbumData_album_songs_album? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GAlbumData_album_songs_albumBuilder() {
    GAlbumData_album_songs_album._initializeBuilder(this);
  }

  GAlbumData_album_songs_albumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData_album_songs_album other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData_album_songs_album;
  }

  @override
  void update(void Function(GAlbumData_album_songs_albumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData_album_songs_album build() {
    final _$result = _$v ??
        new _$GAlbumData_album_songs_album._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GAlbumData_album_songs_album', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumData_album_songs_album', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GAlbumData_album_songs_album', 'name'));
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumData_album_songs_artist extends GAlbumData_album_songs_artist {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GAlbumData_album_songs_artist(
          [void Function(GAlbumData_album_songs_artistBuilder)? updates]) =>
      (new GAlbumData_album_songs_artistBuilder()..update(updates)).build();

  _$GAlbumData_album_songs_artist._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumData_album_songs_artist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GAlbumData_album_songs_artist', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GAlbumData_album_songs_artist', 'name');
  }

  @override
  GAlbumData_album_songs_artist rebuild(
          void Function(GAlbumData_album_songs_artistBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumData_album_songs_artistBuilder toBuilder() =>
      new GAlbumData_album_songs_artistBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumData_album_songs_artist &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumData_album_songs_artist')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GAlbumData_album_songs_artistBuilder
    implements
        Builder<GAlbumData_album_songs_artist,
            GAlbumData_album_songs_artistBuilder> {
  _$GAlbumData_album_songs_artist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GAlbumData_album_songs_artistBuilder() {
    GAlbumData_album_songs_artist._initializeBuilder(this);
  }

  GAlbumData_album_songs_artistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumData_album_songs_artist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumData_album_songs_artist;
  }

  @override
  void update(void Function(GAlbumData_album_songs_artistBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumData_album_songs_artist build() {
    final _$result = _$v ??
        new _$GAlbumData_album_songs_artist._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GAlbumData_album_songs_artist', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumData_album_songs_artist', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GAlbumData_album_songs_artist', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
