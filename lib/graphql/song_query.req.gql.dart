// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:eclektik/graphql/song_query.ast.gql.dart' as _i5;
import 'package:eclektik/graphql/song_query.data.gql.dart' as _i2;
import 'package:eclektik/graphql/song_query.var.gql.dart' as _i3;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;

part 'song_query.req.gql.g.dart';

abstract class GSongReq
    implements
        Built<GSongReq, GSongReqBuilder>,
        _i1.OperationRequest<_i2.GSongData, _i3.GSongVars> {
  GSongReq._();

  factory GSongReq([Function(GSongReqBuilder b) updates]) = _$GSongReq;

  static void _initializeBuilder(GSongReqBuilder b) => b
    ..operation = _i4.Operation(document: _i5.document, operationName: 'Song')
    ..executeOnListen = true;
  _i3.GSongVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GSongData? Function(_i2.GSongData?, _i2.GSongData?)? get updateResult;
  _i2.GSongData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GSongData? parseData(Map<String, dynamic> json) =>
      _i2.GSongData.fromJson(json);
  static Serializer<GSongReq> get serializer => _$gSongReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GSongReq.serializer, this)
          as Map<String, dynamic>);
  static GSongReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GSongReq.serializer, json);
}
