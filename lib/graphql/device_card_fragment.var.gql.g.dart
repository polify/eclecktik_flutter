// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_card_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GDeviceItemVars> _$gDeviceItemVarsSerializer =
    new _$GDeviceItemVarsSerializer();

class _$GDeviceItemVarsSerializer
    implements StructuredSerializer<GDeviceItemVars> {
  @override
  final Iterable<Type> types = const [GDeviceItemVars, _$GDeviceItemVars];
  @override
  final String wireName = 'GDeviceItemVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GDeviceItemVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GDeviceItemVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GDeviceItemVarsBuilder().build();
  }
}

class _$GDeviceItemVars extends GDeviceItemVars {
  factory _$GDeviceItemVars([void Function(GDeviceItemVarsBuilder)? updates]) =>
      (new GDeviceItemVarsBuilder()..update(updates)).build();

  _$GDeviceItemVars._() : super._();

  @override
  GDeviceItemVars rebuild(void Function(GDeviceItemVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GDeviceItemVarsBuilder toBuilder() =>
      new GDeviceItemVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GDeviceItemVars;
  }

  @override
  int get hashCode {
    return 18669677;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GDeviceItemVars').toString();
  }
}

class GDeviceItemVarsBuilder
    implements Builder<GDeviceItemVars, GDeviceItemVarsBuilder> {
  _$GDeviceItemVars? _$v;

  GDeviceItemVarsBuilder();

  @override
  void replace(GDeviceItemVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GDeviceItemVars;
  }

  @override
  void update(void Function(GDeviceItemVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GDeviceItemVars build() {
    final _$result = _$v ?? new _$GDeviceItemVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
