// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'randomplaylist_query.var.gql.g.dart';

abstract class GRandomPlaylistVars
    implements Built<GRandomPlaylistVars, GRandomPlaylistVarsBuilder> {
  GRandomPlaylistVars._();

  factory GRandomPlaylistVars(
      [Function(GRandomPlaylistVarsBuilder b) updates]) = _$GRandomPlaylistVars;

  int? get limit;
  static Serializer<GRandomPlaylistVars> get serializer =>
      _$gRandomPlaylistVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GRandomPlaylistVars.serializer, this)
          as Map<String, dynamic>);
  static GRandomPlaylistVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GRandomPlaylistVars.serializer, json);
}
