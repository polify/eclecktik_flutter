// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'artists_query.var.gql.g.dart';

abstract class GArtistsVars
    implements Built<GArtistsVars, GArtistsVarsBuilder> {
  GArtistsVars._();

  factory GArtistsVars([Function(GArtistsVarsBuilder b) updates]) =
      _$GArtistsVars;

  int? get limit;
  int? get offset;
  static Serializer<GArtistsVars> get serializer => _$gArtistsVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistsVars.serializer, this)
          as Map<String, dynamic>);
  static GArtistsVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistsVars.serializer, json);
}
