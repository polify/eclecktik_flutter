// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artists_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GArtistsData> _$gArtistsDataSerializer =
    new _$GArtistsDataSerializer();
Serializer<GArtistsData_artists> _$gArtistsDataArtistsSerializer =
    new _$GArtistsData_artistsSerializer();
Serializer<GArtistsData_artists_albums> _$gArtistsDataArtistsAlbumsSerializer =
    new _$GArtistsData_artists_albumsSerializer();
Serializer<GArtistsData_artists_songs> _$gArtistsDataArtistsSongsSerializer =
    new _$GArtistsData_artists_songsSerializer();

class _$GArtistsDataSerializer implements StructuredSerializer<GArtistsData> {
  @override
  final Iterable<Type> types = const [GArtistsData, _$GArtistsData];
  @override
  final String wireName = 'GArtistsData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GArtistsData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'artists',
      serializers.serialize(object.artists,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistsData_artists)])),
    ];

    return result;
  }

  @override
  GArtistsData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistsDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'artists':
          result.artists.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(GArtistsData_artists)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistsData_artistsSerializer
    implements StructuredSerializer<GArtistsData_artists> {
  @override
  final Iterable<Type> types = const [
    GArtistsData_artists,
    _$GArtistsData_artists
  ];
  @override
  final String wireName = 'GArtistsData_artists';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistsData_artists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'albums',
      serializers.serialize(object.albums,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistsData_artists_albums)])),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistsData_artists_songs)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistsData_artists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistsData_artistsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GArtistsData_artists_albums)
              ]))! as BuiltList<Object?>);
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GArtistsData_artists_songs)
              ]))! as BuiltList<Object?>);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistsData_artists_albumsSerializer
    implements StructuredSerializer<GArtistsData_artists_albums> {
  @override
  final Iterable<Type> types = const [
    GArtistsData_artists_albums,
    _$GArtistsData_artists_albums
  ];
  @override
  final String wireName = 'GArtistsData_artists_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistsData_artists_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistsData_artists_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistsData_artists_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistsData_artists_songsSerializer
    implements StructuredSerializer<GArtistsData_artists_songs> {
  @override
  final Iterable<Type> types = const [
    GArtistsData_artists_songs,
    _$GArtistsData_artists_songs
  ];
  @override
  final String wireName = 'GArtistsData_artists_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistsData_artists_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistsData_artists_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistsData_artists_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistsData extends GArtistsData {
  @override
  final String G__typename;
  @override
  final BuiltList<GArtistsData_artists> artists;

  factory _$GArtistsData([void Function(GArtistsDataBuilder)? updates]) =>
      (new GArtistsDataBuilder()..update(updates)).build();

  _$GArtistsData._({required this.G__typename, required this.artists})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistsData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(artists, 'GArtistsData', 'artists');
  }

  @override
  GArtistsData rebuild(void Function(GArtistsDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistsDataBuilder toBuilder() => new GArtistsDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistsData &&
        G__typename == other.G__typename &&
        artists == other.artists;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), artists.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistsData')
          ..add('G__typename', G__typename)
          ..add('artists', artists))
        .toString();
  }
}

class GArtistsDataBuilder
    implements Builder<GArtistsData, GArtistsDataBuilder> {
  _$GArtistsData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  ListBuilder<GArtistsData_artists>? _artists;
  ListBuilder<GArtistsData_artists> get artists =>
      _$this._artists ??= new ListBuilder<GArtistsData_artists>();
  set artists(ListBuilder<GArtistsData_artists>? artists) =>
      _$this._artists = artists;

  GArtistsDataBuilder() {
    GArtistsData._initializeBuilder(this);
  }

  GArtistsDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _artists = $v.artists.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistsData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistsData;
  }

  @override
  void update(void Function(GArtistsDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistsData build() {
    _$GArtistsData _$result;
    try {
      _$result = _$v ??
          new _$GArtistsData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistsData', 'G__typename'),
              artists: artists.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'artists';
        artists.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistsData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistsData_artists extends GArtistsData_artists {
  @override
  final String G__typename;
  @override
  final BuiltList<GArtistsData_artists_albums> albums;
  @override
  final BuiltList<GArtistsData_artists_songs> songs;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GArtistsData_artists(
          [void Function(GArtistsData_artistsBuilder)? updates]) =>
      (new GArtistsData_artistsBuilder()..update(updates)).build();

  _$GArtistsData_artists._(
      {required this.G__typename,
      required this.albums,
      required this.songs,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistsData_artists', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        albums, 'GArtistsData_artists', 'albums');
    BuiltValueNullFieldError.checkNotNull(
        songs, 'GArtistsData_artists', 'songs');
    BuiltValueNullFieldError.checkNotNull(id, 'GArtistsData_artists', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GArtistsData_artists', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistsData_artists', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistsData_artists', 'updatedAt');
  }

  @override
  GArtistsData_artists rebuild(
          void Function(GArtistsData_artistsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistsData_artistsBuilder toBuilder() =>
      new GArtistsData_artistsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistsData_artists &&
        G__typename == other.G__typename &&
        albums == other.albums &&
        songs == other.songs &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, G__typename.hashCode), albums.hashCode),
                            songs.hashCode),
                        id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistsData_artists')
          ..add('G__typename', G__typename)
          ..add('albums', albums)
          ..add('songs', songs)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistsData_artistsBuilder
    implements Builder<GArtistsData_artists, GArtistsData_artistsBuilder> {
  _$GArtistsData_artists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  ListBuilder<GArtistsData_artists_albums>? _albums;
  ListBuilder<GArtistsData_artists_albums> get albums =>
      _$this._albums ??= new ListBuilder<GArtistsData_artists_albums>();
  set albums(ListBuilder<GArtistsData_artists_albums>? albums) =>
      _$this._albums = albums;

  ListBuilder<GArtistsData_artists_songs>? _songs;
  ListBuilder<GArtistsData_artists_songs> get songs =>
      _$this._songs ??= new ListBuilder<GArtistsData_artists_songs>();
  set songs(ListBuilder<GArtistsData_artists_songs>? songs) =>
      _$this._songs = songs;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistsData_artistsBuilder() {
    GArtistsData_artists._initializeBuilder(this);
  }

  GArtistsData_artistsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _albums = $v.albums.toBuilder();
      _songs = $v.songs.toBuilder();
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistsData_artists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistsData_artists;
  }

  @override
  void update(void Function(GArtistsData_artistsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistsData_artists build() {
    _$GArtistsData_artists _$result;
    try {
      _$result = _$v ??
          new _$GArtistsData_artists._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistsData_artists', 'G__typename'),
              albums: albums.build(),
              songs: songs.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistsData_artists', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GArtistsData_artists', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
        _$failedField = 'songs';
        songs.build();

        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistsData_artists', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistsData_artists_albums extends GArtistsData_artists_albums {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GArtistsData_artists_albums(
          [void Function(GArtistsData_artists_albumsBuilder)? updates]) =>
      (new GArtistsData_artists_albumsBuilder()..update(updates)).build();

  _$GArtistsData_artists_albums._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistsData_artists_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GArtistsData_artists_albums', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GArtistsData_artists_albums', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistsData_artists_albums', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistsData_artists_albums', 'updatedAt');
  }

  @override
  GArtistsData_artists_albums rebuild(
          void Function(GArtistsData_artists_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistsData_artists_albumsBuilder toBuilder() =>
      new GArtistsData_artists_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistsData_artists_albums &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistsData_artists_albums')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistsData_artists_albumsBuilder
    implements
        Builder<GArtistsData_artists_albums,
            GArtistsData_artists_albumsBuilder> {
  _$GArtistsData_artists_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistsData_artists_albumsBuilder() {
    GArtistsData_artists_albums._initializeBuilder(this);
  }

  GArtistsData_artists_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistsData_artists_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistsData_artists_albums;
  }

  @override
  void update(void Function(GArtistsData_artists_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistsData_artists_albums build() {
    _$GArtistsData_artists_albums _$result;
    try {
      _$result = _$v ??
          new _$GArtistsData_artists_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistsData_artists_albums', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistsData_artists_albums', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GArtistsData_artists_albums', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistsData_artists_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistsData_artists_songs extends GArtistsData_artists_songs {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GArtistsData_artists_songs(
          [void Function(GArtistsData_artists_songsBuilder)? updates]) =>
      (new GArtistsData_artists_songsBuilder()..update(updates)).build();

  _$GArtistsData_artists_songs._(
      {required this.G__typename,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistsData_artists_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GArtistsData_artists_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GArtistsData_artists_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistsData_artists_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistsData_artists_songs', 'updatedAt');
  }

  @override
  GArtistsData_artists_songs rebuild(
          void Function(GArtistsData_artists_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistsData_artists_songsBuilder toBuilder() =>
      new GArtistsData_artists_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistsData_artists_songs &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    title.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistsData_artists_songs')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistsData_artists_songsBuilder
    implements
        Builder<GArtistsData_artists_songs, GArtistsData_artists_songsBuilder> {
  _$GArtistsData_artists_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistsData_artists_songsBuilder() {
    GArtistsData_artists_songs._initializeBuilder(this);
  }

  GArtistsData_artists_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistsData_artists_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistsData_artists_songs;
  }

  @override
  void update(void Function(GArtistsData_artists_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistsData_artists_songs build() {
    _$GArtistsData_artists_songs _$result;
    try {
      _$result = _$v ??
          new _$GArtistsData_artists_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistsData_artists_songs', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistsData_artists_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GArtistsData_artists_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistsData_artists_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
