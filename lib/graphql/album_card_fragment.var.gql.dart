// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'album_card_fragment.var.gql.g.dart';

abstract class GAlbumCardVars
    implements Built<GAlbumCardVars, GAlbumCardVarsBuilder> {
  GAlbumCardVars._();

  factory GAlbumCardVars([Function(GAlbumCardVarsBuilder b) updates]) =
      _$GAlbumCardVars;

  static Serializer<GAlbumCardVars> get serializer =>
      _$gAlbumCardVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumCardVars.serializer, this)
          as Map<String, dynamic>);
  static GAlbumCardVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumCardVars.serializer, json);
}
