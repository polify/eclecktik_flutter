// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'randomplaylist_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GRandomPlaylistData> _$gRandomPlaylistDataSerializer =
    new _$GRandomPlaylistDataSerializer();
Serializer<GRandomPlaylistData_randomPlaylist>
    _$gRandomPlaylistDataRandomPlaylistSerializer =
    new _$GRandomPlaylistData_randomPlaylistSerializer();
Serializer<GRandomPlaylistData_randomPlaylist_songs>
    _$gRandomPlaylistDataRandomPlaylistSongsSerializer =
    new _$GRandomPlaylistData_randomPlaylist_songsSerializer();
Serializer<GRandomPlaylistData_randomPlaylist_songs_signedUrl>
    _$gRandomPlaylistDataRandomPlaylistSongsSignedUrlSerializer =
    new _$GRandomPlaylistData_randomPlaylist_songs_signedUrlSerializer();
Serializer<GRandomPlaylistData_randomPlaylist_songs_album>
    _$gRandomPlaylistDataRandomPlaylistSongsAlbumSerializer =
    new _$GRandomPlaylistData_randomPlaylist_songs_albumSerializer();
Serializer<GRandomPlaylistData_randomPlaylist_songs_artist>
    _$gRandomPlaylistDataRandomPlaylistSongsArtistSerializer =
    new _$GRandomPlaylistData_randomPlaylist_songs_artistSerializer();

class _$GRandomPlaylistDataSerializer
    implements StructuredSerializer<GRandomPlaylistData> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistData,
    _$GRandomPlaylistData
  ];
  @override
  final String wireName = 'GRandomPlaylistData';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GRandomPlaylistData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'randomPlaylist',
      serializers.serialize(object.randomPlaylist,
          specifiedType: const FullType(GRandomPlaylistData_randomPlaylist)),
    ];

    return result;
  }

  @override
  GRandomPlaylistData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GRandomPlaylistDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'randomPlaylist':
          result.randomPlaylist.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GRandomPlaylistData_randomPlaylist))!
              as GRandomPlaylistData_randomPlaylist);
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistData_randomPlaylistSerializer
    implements StructuredSerializer<GRandomPlaylistData_randomPlaylist> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistData_randomPlaylist,
    _$GRandomPlaylistData_randomPlaylist
  ];
  @override
  final String wireName = 'GRandomPlaylistData_randomPlaylist';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GRandomPlaylistData_randomPlaylist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(BuiltList, const [
            const FullType(GRandomPlaylistData_randomPlaylist_songs)
          ])),
    ];

    return result;
  }

  @override
  GRandomPlaylistData_randomPlaylist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GRandomPlaylistData_randomPlaylistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GRandomPlaylistData_randomPlaylist_songs)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistData_randomPlaylist_songsSerializer
    implements StructuredSerializer<GRandomPlaylistData_randomPlaylist_songs> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistData_randomPlaylist_songs,
    _$GRandomPlaylistData_randomPlaylist_songs
  ];
  @override
  final String wireName = 'GRandomPlaylistData_randomPlaylist_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GRandomPlaylistData_randomPlaylist_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.signedUrl;
    if (value != null) {
      result
        ..add('signedUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GRandomPlaylistData_randomPlaylist_songs_signedUrl)));
    }
    value = object.album;
    if (value != null) {
      result
        ..add('album')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GRandomPlaylistData_randomPlaylist_songs_album)));
    }
    value = object.artist;
    if (value != null) {
      result
        ..add('artist')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                GRandomPlaylistData_randomPlaylist_songs_artist)));
    }
    return result;
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GRandomPlaylistData_randomPlaylist_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'signedUrl':
          result.signedUrl.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GRandomPlaylistData_randomPlaylist_songs_signedUrl))!
              as GRandomPlaylistData_randomPlaylist_songs_signedUrl);
          break;
        case 'album':
          result.album.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GRandomPlaylistData_randomPlaylist_songs_album))!
              as GRandomPlaylistData_randomPlaylist_songs_album);
          break;
        case 'artist':
          result.artist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      GRandomPlaylistData_randomPlaylist_songs_artist))!
              as GRandomPlaylistData_randomPlaylist_songs_artist);
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs_signedUrlSerializer
    implements
        StructuredSerializer<
            GRandomPlaylistData_randomPlaylist_songs_signedUrl> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistData_randomPlaylist_songs_signedUrl,
    _$GRandomPlaylistData_randomPlaylist_songs_signedUrl
  ];
  @override
  final String wireName = 'GRandomPlaylistData_randomPlaylist_songs_signedUrl';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GRandomPlaylistData_randomPlaylist_songs_signedUrl object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'signedUrl',
      serializers.serialize(object.signedUrl,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs_signedUrl deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result =
        new GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'signedUrl':
          result.signedUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs_albumSerializer
    implements
        StructuredSerializer<GRandomPlaylistData_randomPlaylist_songs_album> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistData_randomPlaylist_songs_album,
    _$GRandomPlaylistData_randomPlaylist_songs_album
  ];
  @override
  final String wireName = 'GRandomPlaylistData_randomPlaylist_songs_album';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GRandomPlaylistData_randomPlaylist_songs_album object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs_album deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GRandomPlaylistData_randomPlaylist_songs_albumBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs_artistSerializer
    implements
        StructuredSerializer<GRandomPlaylistData_randomPlaylist_songs_artist> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistData_randomPlaylist_songs_artist,
    _$GRandomPlaylistData_randomPlaylist_songs_artist
  ];
  @override
  final String wireName = 'GRandomPlaylistData_randomPlaylist_songs_artist';

  @override
  Iterable<Object?> serialize(Serializers serializers,
      GRandomPlaylistData_randomPlaylist_songs_artist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs_artist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GRandomPlaylistData_randomPlaylist_songs_artistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistData extends GRandomPlaylistData {
  @override
  final String G__typename;
  @override
  final GRandomPlaylistData_randomPlaylist randomPlaylist;

  factory _$GRandomPlaylistData(
          [void Function(GRandomPlaylistDataBuilder)? updates]) =>
      (new GRandomPlaylistDataBuilder()..update(updates)).build();

  _$GRandomPlaylistData._(
      {required this.G__typename, required this.randomPlaylist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GRandomPlaylistData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        randomPlaylist, 'GRandomPlaylistData', 'randomPlaylist');
  }

  @override
  GRandomPlaylistData rebuild(
          void Function(GRandomPlaylistDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistDataBuilder toBuilder() =>
      new GRandomPlaylistDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistData &&
        G__typename == other.G__typename &&
        randomPlaylist == other.randomPlaylist;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), randomPlaylist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GRandomPlaylistData')
          ..add('G__typename', G__typename)
          ..add('randomPlaylist', randomPlaylist))
        .toString();
  }
}

class GRandomPlaylistDataBuilder
    implements Builder<GRandomPlaylistData, GRandomPlaylistDataBuilder> {
  _$GRandomPlaylistData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GRandomPlaylistData_randomPlaylistBuilder? _randomPlaylist;
  GRandomPlaylistData_randomPlaylistBuilder get randomPlaylist =>
      _$this._randomPlaylist ??=
          new GRandomPlaylistData_randomPlaylistBuilder();
  set randomPlaylist(
          GRandomPlaylistData_randomPlaylistBuilder? randomPlaylist) =>
      _$this._randomPlaylist = randomPlaylist;

  GRandomPlaylistDataBuilder() {
    GRandomPlaylistData._initializeBuilder(this);
  }

  GRandomPlaylistDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _randomPlaylist = $v.randomPlaylist.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistData;
  }

  @override
  void update(void Function(GRandomPlaylistDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistData build() {
    _$GRandomPlaylistData _$result;
    try {
      _$result = _$v ??
          new _$GRandomPlaylistData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GRandomPlaylistData', 'G__typename'),
              randomPlaylist: randomPlaylist.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'randomPlaylist';
        randomPlaylist.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GRandomPlaylistData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GRandomPlaylistData_randomPlaylist
    extends GRandomPlaylistData_randomPlaylist {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final BuiltList<GRandomPlaylistData_randomPlaylist_songs> songs;

  factory _$GRandomPlaylistData_randomPlaylist(
          [void Function(GRandomPlaylistData_randomPlaylistBuilder)?
              updates]) =>
      (new GRandomPlaylistData_randomPlaylistBuilder()..update(updates))
          .build();

  _$GRandomPlaylistData_randomPlaylist._(
      {required this.G__typename,
      required this.id,
      required this.name,
      required this.songs})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GRandomPlaylistData_randomPlaylist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GRandomPlaylistData_randomPlaylist', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GRandomPlaylistData_randomPlaylist', 'name');
    BuiltValueNullFieldError.checkNotNull(
        songs, 'GRandomPlaylistData_randomPlaylist', 'songs');
  }

  @override
  GRandomPlaylistData_randomPlaylist rebuild(
          void Function(GRandomPlaylistData_randomPlaylistBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistData_randomPlaylistBuilder toBuilder() =>
      new GRandomPlaylistData_randomPlaylistBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistData_randomPlaylist &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        songs == other.songs;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode),
        songs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GRandomPlaylistData_randomPlaylist')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('songs', songs))
        .toString();
  }
}

class GRandomPlaylistData_randomPlaylistBuilder
    implements
        Builder<GRandomPlaylistData_randomPlaylist,
            GRandomPlaylistData_randomPlaylistBuilder> {
  _$GRandomPlaylistData_randomPlaylist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ListBuilder<GRandomPlaylistData_randomPlaylist_songs>? _songs;
  ListBuilder<GRandomPlaylistData_randomPlaylist_songs> get songs =>
      _$this._songs ??=
          new ListBuilder<GRandomPlaylistData_randomPlaylist_songs>();
  set songs(ListBuilder<GRandomPlaylistData_randomPlaylist_songs>? songs) =>
      _$this._songs = songs;

  GRandomPlaylistData_randomPlaylistBuilder() {
    GRandomPlaylistData_randomPlaylist._initializeBuilder(this);
  }

  GRandomPlaylistData_randomPlaylistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _songs = $v.songs.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistData_randomPlaylist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistData_randomPlaylist;
  }

  @override
  void update(
      void Function(GRandomPlaylistData_randomPlaylistBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistData_randomPlaylist build() {
    _$GRandomPlaylistData_randomPlaylist _$result;
    try {
      _$result = _$v ??
          new _$GRandomPlaylistData_randomPlaylist._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GRandomPlaylistData_randomPlaylist', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GRandomPlaylistData_randomPlaylist', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GRandomPlaylistData_randomPlaylist', 'name'),
              songs: songs.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'songs';
        songs.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GRandomPlaylistData_randomPlaylist', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs
    extends GRandomPlaylistData_randomPlaylist_songs {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final String duration;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;
  @override
  final GRandomPlaylistData_randomPlaylist_songs_signedUrl? signedUrl;
  @override
  final GRandomPlaylistData_randomPlaylist_songs_album? album;
  @override
  final GRandomPlaylistData_randomPlaylist_songs_artist? artist;

  factory _$GRandomPlaylistData_randomPlaylist_songs(
          [void Function(GRandomPlaylistData_randomPlaylist_songsBuilder)?
              updates]) =>
      (new GRandomPlaylistData_randomPlaylist_songsBuilder()..update(updates))
          .build();

  _$GRandomPlaylistData_randomPlaylist_songs._(
      {required this.G__typename,
      required this.s3Key,
      required this.duration,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt,
      this.signedUrl,
      this.album,
      this.artist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GRandomPlaylistData_randomPlaylist_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        s3Key, 'GRandomPlaylistData_randomPlaylist_songs', 's3Key');
    BuiltValueNullFieldError.checkNotNull(
        duration, 'GRandomPlaylistData_randomPlaylist_songs', 'duration');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GRandomPlaylistData_randomPlaylist_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GRandomPlaylistData_randomPlaylist_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GRandomPlaylistData_randomPlaylist_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GRandomPlaylistData_randomPlaylist_songs', 'updatedAt');
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs rebuild(
          void Function(GRandomPlaylistData_randomPlaylist_songsBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistData_randomPlaylist_songsBuilder toBuilder() =>
      new GRandomPlaylistData_randomPlaylist_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistData_randomPlaylist_songs &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        duration == other.duration &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        signedUrl == other.signedUrl &&
        album == other.album &&
        artist == other.artist;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, G__typename.hashCode),
                                            s3Key.hashCode),
                                        duration.hashCode),
                                    id.hashCode),
                                title.hashCode),
                            coverUrl.hashCode),
                        createdAt.hashCode),
                    updatedAt.hashCode),
                signedUrl.hashCode),
            album.hashCode),
        artist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GRandomPlaylistData_randomPlaylist_songs')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('duration', duration)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('signedUrl', signedUrl)
          ..add('album', album)
          ..add('artist', artist))
        .toString();
  }
}

class GRandomPlaylistData_randomPlaylist_songsBuilder
    implements
        Builder<GRandomPlaylistData_randomPlaylist_songs,
            GRandomPlaylistData_randomPlaylist_songsBuilder> {
  _$GRandomPlaylistData_randomPlaylist_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _duration;
  String? get duration => _$this._duration;
  set duration(String? duration) => _$this._duration = duration;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder? _signedUrl;
  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder get signedUrl =>
      _$this._signedUrl ??=
          new GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder();
  set signedUrl(
          GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder?
              signedUrl) =>
      _$this._signedUrl = signedUrl;

  GRandomPlaylistData_randomPlaylist_songs_albumBuilder? _album;
  GRandomPlaylistData_randomPlaylist_songs_albumBuilder get album =>
      _$this._album ??=
          new GRandomPlaylistData_randomPlaylist_songs_albumBuilder();
  set album(GRandomPlaylistData_randomPlaylist_songs_albumBuilder? album) =>
      _$this._album = album;

  GRandomPlaylistData_randomPlaylist_songs_artistBuilder? _artist;
  GRandomPlaylistData_randomPlaylist_songs_artistBuilder get artist =>
      _$this._artist ??=
          new GRandomPlaylistData_randomPlaylist_songs_artistBuilder();
  set artist(GRandomPlaylistData_randomPlaylist_songs_artistBuilder? artist) =>
      _$this._artist = artist;

  GRandomPlaylistData_randomPlaylist_songsBuilder() {
    GRandomPlaylistData_randomPlaylist_songs._initializeBuilder(this);
  }

  GRandomPlaylistData_randomPlaylist_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _duration = $v.duration;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _signedUrl = $v.signedUrl?.toBuilder();
      _album = $v.album?.toBuilder();
      _artist = $v.artist?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistData_randomPlaylist_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistData_randomPlaylist_songs;
  }

  @override
  void update(
      void Function(GRandomPlaylistData_randomPlaylist_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistData_randomPlaylist_songs build() {
    _$GRandomPlaylistData_randomPlaylist_songs _$result;
    try {
      _$result = _$v ??
          new _$GRandomPlaylistData_randomPlaylist_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GRandomPlaylistData_randomPlaylist_songs', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GRandomPlaylistData_randomPlaylist_songs', 's3Key'),
              duration: BuiltValueNullFieldError.checkNotNull(duration,
                  'GRandomPlaylistData_randomPlaylist_songs', 'duration'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GRandomPlaylistData_randomPlaylist_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GRandomPlaylistData_randomPlaylist_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              signedUrl: _signedUrl?.build(),
              album: _album?.build(),
              artist: _artist?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 'signedUrl';
        _signedUrl?.build();
        _$failedField = 'album';
        _album?.build();
        _$failedField = 'artist';
        _artist?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GRandomPlaylistData_randomPlaylist_songs',
            _$failedField,
            e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs_signedUrl
    extends GRandomPlaylistData_randomPlaylist_songs_signedUrl {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String signedUrl;

  factory _$GRandomPlaylistData_randomPlaylist_songs_signedUrl(
          [void Function(
                  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder)?
              updates]) =>
      (new GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder()
            ..update(updates))
          .build();

  _$GRandomPlaylistData_randomPlaylist_songs_signedUrl._(
      {required this.G__typename, required this.id, required this.signedUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GRandomPlaylistData_randomPlaylist_songs_signedUrl', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GRandomPlaylistData_randomPlaylist_songs_signedUrl', 'id');
    BuiltValueNullFieldError.checkNotNull(signedUrl,
        'GRandomPlaylistData_randomPlaylist_songs_signedUrl', 'signedUrl');
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs_signedUrl rebuild(
          void Function(
                  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder toBuilder() =>
      new GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistData_randomPlaylist_songs_signedUrl &&
        G__typename == other.G__typename &&
        id == other.id &&
        signedUrl == other.signedUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), id.hashCode), signedUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GRandomPlaylistData_randomPlaylist_songs_signedUrl')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('signedUrl', signedUrl))
        .toString();
  }
}

class GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder
    implements
        Builder<GRandomPlaylistData_randomPlaylist_songs_signedUrl,
            GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder> {
  _$GRandomPlaylistData_randomPlaylist_songs_signedUrl? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _signedUrl;
  String? get signedUrl => _$this._signedUrl;
  set signedUrl(String? signedUrl) => _$this._signedUrl = signedUrl;

  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder() {
    GRandomPlaylistData_randomPlaylist_songs_signedUrl._initializeBuilder(this);
  }

  GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _signedUrl = $v.signedUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistData_randomPlaylist_songs_signedUrl other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistData_randomPlaylist_songs_signedUrl;
  }

  @override
  void update(
      void Function(GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistData_randomPlaylist_songs_signedUrl build() {
    final _$result = _$v ??
        new _$GRandomPlaylistData_randomPlaylist_songs_signedUrl._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename,
                'GRandomPlaylistData_randomPlaylist_songs_signedUrl',
                'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GRandomPlaylistData_randomPlaylist_songs_signedUrl', 'id'),
            signedUrl: BuiltValueNullFieldError.checkNotNull(
                signedUrl,
                'GRandomPlaylistData_randomPlaylist_songs_signedUrl',
                'signedUrl'));
    replace(_$result);
    return _$result;
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs_album
    extends GRandomPlaylistData_randomPlaylist_songs_album {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GRandomPlaylistData_randomPlaylist_songs_album(
          [void Function(GRandomPlaylistData_randomPlaylist_songs_albumBuilder)?
              updates]) =>
      (new GRandomPlaylistData_randomPlaylist_songs_albumBuilder()
            ..update(updates))
          .build();

  _$GRandomPlaylistData_randomPlaylist_songs_album._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GRandomPlaylistData_randomPlaylist_songs_album', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GRandomPlaylistData_randomPlaylist_songs_album', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GRandomPlaylistData_randomPlaylist_songs_album', 'name');
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs_album rebuild(
          void Function(GRandomPlaylistData_randomPlaylist_songs_albumBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistData_randomPlaylist_songs_albumBuilder toBuilder() =>
      new GRandomPlaylistData_randomPlaylist_songs_albumBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistData_randomPlaylist_songs_album &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GRandomPlaylistData_randomPlaylist_songs_album')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GRandomPlaylistData_randomPlaylist_songs_albumBuilder
    implements
        Builder<GRandomPlaylistData_randomPlaylist_songs_album,
            GRandomPlaylistData_randomPlaylist_songs_albumBuilder> {
  _$GRandomPlaylistData_randomPlaylist_songs_album? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GRandomPlaylistData_randomPlaylist_songs_albumBuilder() {
    GRandomPlaylistData_randomPlaylist_songs_album._initializeBuilder(this);
  }

  GRandomPlaylistData_randomPlaylist_songs_albumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistData_randomPlaylist_songs_album other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistData_randomPlaylist_songs_album;
  }

  @override
  void update(
      void Function(GRandomPlaylistData_randomPlaylist_songs_albumBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistData_randomPlaylist_songs_album build() {
    final _$result = _$v ??
        new _$GRandomPlaylistData_randomPlaylist_songs_album._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename,
                'GRandomPlaylistData_randomPlaylist_songs_album',
                'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GRandomPlaylistData_randomPlaylist_songs_album', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(name,
                'GRandomPlaylistData_randomPlaylist_songs_album', 'name'));
    replace(_$result);
    return _$result;
  }
}

class _$GRandomPlaylistData_randomPlaylist_songs_artist
    extends GRandomPlaylistData_randomPlaylist_songs_artist {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GRandomPlaylistData_randomPlaylist_songs_artist(
          [void Function(
                  GRandomPlaylistData_randomPlaylist_songs_artistBuilder)?
              updates]) =>
      (new GRandomPlaylistData_randomPlaylist_songs_artistBuilder()
            ..update(updates))
          .build();

  _$GRandomPlaylistData_randomPlaylist_songs_artist._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(G__typename,
        'GRandomPlaylistData_randomPlaylist_songs_artist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GRandomPlaylistData_randomPlaylist_songs_artist', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GRandomPlaylistData_randomPlaylist_songs_artist', 'name');
  }

  @override
  GRandomPlaylistData_randomPlaylist_songs_artist rebuild(
          void Function(GRandomPlaylistData_randomPlaylist_songs_artistBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistData_randomPlaylist_songs_artistBuilder toBuilder() =>
      new GRandomPlaylistData_randomPlaylist_songs_artistBuilder()
        ..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistData_randomPlaylist_songs_artist &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            'GRandomPlaylistData_randomPlaylist_songs_artist')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GRandomPlaylistData_randomPlaylist_songs_artistBuilder
    implements
        Builder<GRandomPlaylistData_randomPlaylist_songs_artist,
            GRandomPlaylistData_randomPlaylist_songs_artistBuilder> {
  _$GRandomPlaylistData_randomPlaylist_songs_artist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GRandomPlaylistData_randomPlaylist_songs_artistBuilder() {
    GRandomPlaylistData_randomPlaylist_songs_artist._initializeBuilder(this);
  }

  GRandomPlaylistData_randomPlaylist_songs_artistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistData_randomPlaylist_songs_artist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistData_randomPlaylist_songs_artist;
  }

  @override
  void update(
      void Function(GRandomPlaylistData_randomPlaylist_songs_artistBuilder)?
          updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistData_randomPlaylist_songs_artist build() {
    final _$result = _$v ??
        new _$GRandomPlaylistData_randomPlaylist_songs_artist._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename,
                'GRandomPlaylistData_randomPlaylist_songs_artist',
                'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GRandomPlaylistData_randomPlaylist_songs_artist', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(name,
                'GRandomPlaylistData_randomPlaylist_songs_artist', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
