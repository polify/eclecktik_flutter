// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:eclektik/graphql/album_item_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/artist_item_fragment.ast.gql.dart' as _i5;
import 'package:eclektik/graphql/song_card_fragment.ast.gql.dart' as _i2;
import 'package:eclektik/graphql/song_item_fragment.ast.gql.dart' as _i3;
import 'package:gql/ast.dart' as _i1;

const Album = _i1.OperationDefinitionNode(
    type: _i1.OperationType.query,
    name: _i1.NameNode(value: 'Album'),
    variableDefinitions: [
      _i1.VariableDefinitionNode(
          variable: _i1.VariableNode(name: _i1.NameNode(value: 'id')),
          type: _i1.NamedTypeNode(
              name: _i1.NameNode(value: 'String'), isNonNull: true),
          defaultValue: _i1.DefaultValueNode(value: null),
          directives: [])
    ],
    directives: [],
    selectionSet: _i1.SelectionSetNode(selections: [
      _i1.FieldNode(
          name: _i1.NameNode(value: 'album'),
          alias: null,
          arguments: [
            _i1.ArgumentNode(
                name: _i1.NameNode(value: 'id'),
                value: _i1.VariableNode(name: _i1.NameNode(value: 'id')))
          ],
          directives: [],
          selectionSet: _i1.SelectionSetNode(selections: [
            _i1.FragmentSpreadNode(
                name: _i1.NameNode(value: 'AlbumItem'), directives: []),
            _i1.FieldNode(
                name: _i1.NameNode(value: 'artists'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: _i1.SelectionSetNode(selections: [
                  _i1.FragmentSpreadNode(
                      name: _i1.NameNode(value: 'ArtistItem'), directives: [])
                ])),
            _i1.FieldNode(
                name: _i1.NameNode(value: 'songs'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: _i1.SelectionSetNode(selections: [
                  _i1.FragmentSpreadNode(
                      name: _i1.NameNode(value: 'SongCard'), directives: [])
                ]))
          ]))
    ]));
const document = _i1.DocumentNode(definitions: [
  Album,
  _i2.SongCard,
  _i3.SongItem,
  _i4.AlbumItem,
  _i5.ArtistItem
]);
