// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:eclektik/graphql/song_card_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/song_card_fragment.var.gql.dart' as _i3;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;

part 'song_card_fragment.req.gql.g.dart';

abstract class GSongCardReq
    implements
        Built<GSongCardReq, GSongCardReqBuilder>,
        _i1.FragmentRequest<_i2.GSongCardData, _i3.GSongCardVars> {
  GSongCardReq._();

  factory GSongCardReq([Function(GSongCardReqBuilder b) updates]) =
      _$GSongCardReq;

  static void _initializeBuilder(GSongCardReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'SongCard';
  _i3.GSongCardVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GSongCardData? parseData(Map<String, dynamic> json) =>
      _i2.GSongCardData.fromJson(json);
  static Serializer<GSongCardReq> get serializer => _$gSongCardReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GSongCardReq.serializer, this)
          as Map<String, dynamic>);
  static GSongCardReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GSongCardReq.serializer, json);
}
