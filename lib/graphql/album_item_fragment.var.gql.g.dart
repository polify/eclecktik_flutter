// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_item_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GAlbumItemVars> _$gAlbumItemVarsSerializer =
    new _$GAlbumItemVarsSerializer();

class _$GAlbumItemVarsSerializer
    implements StructuredSerializer<GAlbumItemVars> {
  @override
  final Iterable<Type> types = const [GAlbumItemVars, _$GAlbumItemVars];
  @override
  final String wireName = 'GAlbumItemVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GAlbumItemVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GAlbumItemVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GAlbumItemVarsBuilder().build();
  }
}

class _$GAlbumItemVars extends GAlbumItemVars {
  factory _$GAlbumItemVars([void Function(GAlbumItemVarsBuilder)? updates]) =>
      (new GAlbumItemVarsBuilder()..update(updates)).build();

  _$GAlbumItemVars._() : super._();

  @override
  GAlbumItemVars rebuild(void Function(GAlbumItemVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumItemVarsBuilder toBuilder() =>
      new GAlbumItemVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumItemVars;
  }

  @override
  int get hashCode {
    return 64912618;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GAlbumItemVars').toString();
  }
}

class GAlbumItemVarsBuilder
    implements Builder<GAlbumItemVars, GAlbumItemVarsBuilder> {
  _$GAlbumItemVars? _$v;

  GAlbumItemVarsBuilder();

  @override
  void replace(GAlbumItemVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumItemVars;
  }

  @override
  void update(void Function(GAlbumItemVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumItemVars build() {
    final _$result = _$v ?? new _$GAlbumItemVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
