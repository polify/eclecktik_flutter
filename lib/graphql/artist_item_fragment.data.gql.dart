// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i1;
import 'package:eclektik/graphql/serializers.gql.dart' as _i2;

part 'artist_item_fragment.data.gql.g.dart';

abstract class GArtistItem {
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GArtistItemData
    implements Built<GArtistItemData, GArtistItemDataBuilder>, GArtistItem {
  GArtistItemData._();

  factory GArtistItemData([Function(GArtistItemDataBuilder b) updates]) =
      _$GArtistItemData;

  static void _initializeBuilder(GArtistItemDataBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  static Serializer<GArtistItemData> get serializer =>
      _$gArtistItemDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GArtistItemData.serializer, this)
          as Map<String, dynamic>);
  static GArtistItemData? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GArtistItemData.serializer, json);
}
