// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'album_item_fragment.var.gql.g.dart';

abstract class GAlbumItemVars
    implements Built<GAlbumItemVars, GAlbumItemVarsBuilder> {
  GAlbumItemVars._();

  factory GAlbumItemVars([Function(GAlbumItemVarsBuilder b) updates]) =
      _$GAlbumItemVars;

  static Serializer<GAlbumItemVars> get serializer =>
      _$gAlbumItemVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumItemVars.serializer, this)
          as Map<String, dynamic>);
  static GAlbumItemVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumItemVars.serializer, json);
}
