// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i1;
import 'package:eclektik/graphql/serializers.gql.dart' as _i2;

part 'device_card_fragment.data.gql.g.dart';

abstract class GDeviceItem {
  String get G__typename;
  String get id;
  String get lastIP;
  String get name;
  String get os;
  String get type;
  String get userAgent;
  _i1.GTime get lastConnection;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GDeviceItemData
    implements Built<GDeviceItemData, GDeviceItemDataBuilder>, GDeviceItem {
  GDeviceItemData._();

  factory GDeviceItemData([Function(GDeviceItemDataBuilder b) updates]) =
      _$GDeviceItemData;

  static void _initializeBuilder(GDeviceItemDataBuilder b) =>
      b..G__typename = 'Device';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get lastIP;
  String get name;
  String get os;
  String get type;
  String get userAgent;
  _i1.GTime get lastConnection;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  static Serializer<GDeviceItemData> get serializer =>
      _$gDeviceItemDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GDeviceItemData.serializer, this)
          as Map<String, dynamic>);
  static GDeviceItemData? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GDeviceItemData.serializer, json);
}
