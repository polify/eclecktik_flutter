// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/artist_item_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/artist_item_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/artist_item_fragment.var.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;

part 'artist_item_fragment.req.gql.g.dart';

abstract class GArtistItemReq
    implements
        Built<GArtistItemReq, GArtistItemReqBuilder>,
        _i1.FragmentRequest<_i2.GArtistItemData, _i3.GArtistItemVars> {
  GArtistItemReq._();

  factory GArtistItemReq([Function(GArtistItemReqBuilder b) updates]) =
      _$GArtistItemReq;

  static void _initializeBuilder(GArtistItemReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'ArtistItem';
  _i3.GArtistItemVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GArtistItemData? parseData(Map<String, dynamic> json) =>
      _i2.GArtistItemData.fromJson(json);
  static Serializer<GArtistItemReq> get serializer =>
      _$gArtistItemReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GArtistItemReq.serializer, this)
          as Map<String, dynamic>);
  static GArtistItemReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GArtistItemReq.serializer, json);
}
