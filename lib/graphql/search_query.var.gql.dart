// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'search_query.var.gql.g.dart';

abstract class GSearchVars implements Built<GSearchVars, GSearchVarsBuilder> {
  GSearchVars._();

  factory GSearchVars([Function(GSearchVarsBuilder b) updates]) = _$GSearchVars;

  String get query;
  static Serializer<GSearchVars> get serializer => _$gSearchVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSearchVars.serializer, this)
          as Map<String, dynamic>);
  static GSearchVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSearchVars.serializer, json);
}
