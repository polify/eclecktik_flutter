// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artist_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GArtistData> _$gArtistDataSerializer = new _$GArtistDataSerializer();
Serializer<GArtistData_artist> _$gArtistDataArtistSerializer =
    new _$GArtistData_artistSerializer();
Serializer<GArtistData_artist_albums> _$gArtistDataArtistAlbumsSerializer =
    new _$GArtistData_artist_albumsSerializer();
Serializer<GArtistData_artist_songs> _$gArtistDataArtistSongsSerializer =
    new _$GArtistData_artist_songsSerializer();

class _$GArtistDataSerializer implements StructuredSerializer<GArtistData> {
  @override
  final Iterable<Type> types = const [GArtistData, _$GArtistData];
  @override
  final String wireName = 'GArtistData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GArtistData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'artist',
      serializers.serialize(object.artist,
          specifiedType: const FullType(GArtistData_artist)),
    ];

    return result;
  }

  @override
  GArtistData deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'artist':
          result.artist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GArtistData_artist))!
              as GArtistData_artist);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistData_artistSerializer
    implements StructuredSerializer<GArtistData_artist> {
  @override
  final Iterable<Type> types = const [GArtistData_artist, _$GArtistData_artist];
  @override
  final String wireName = 'GArtistData_artist';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistData_artist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'albums',
      serializers.serialize(object.albums,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistData_artist_albums)])),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistData_artist_songs)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistData_artist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistData_artistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GArtistData_artist_albums)
              ]))! as BuiltList<Object?>);
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GArtistData_artist_songs)
              ]))! as BuiltList<Object?>);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistData_artist_albumsSerializer
    implements StructuredSerializer<GArtistData_artist_albums> {
  @override
  final Iterable<Type> types = const [
    GArtistData_artist_albums,
    _$GArtistData_artist_albums
  ];
  @override
  final String wireName = 'GArtistData_artist_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistData_artist_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistData_artist_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistData_artist_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistData_artist_songsSerializer
    implements StructuredSerializer<GArtistData_artist_songs> {
  @override
  final Iterable<Type> types = const [
    GArtistData_artist_songs,
    _$GArtistData_artist_songs
  ];
  @override
  final String wireName = 'GArtistData_artist_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistData_artist_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistData_artist_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistData_artist_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistData extends GArtistData {
  @override
  final String G__typename;
  @override
  final GArtistData_artist artist;

  factory _$GArtistData([void Function(GArtistDataBuilder)? updates]) =>
      (new GArtistDataBuilder()..update(updates)).build();

  _$GArtistData._({required this.G__typename, required this.artist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(artist, 'GArtistData', 'artist');
  }

  @override
  GArtistData rebuild(void Function(GArtistDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistDataBuilder toBuilder() => new GArtistDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistData &&
        G__typename == other.G__typename &&
        artist == other.artist;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), artist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistData')
          ..add('G__typename', G__typename)
          ..add('artist', artist))
        .toString();
  }
}

class GArtistDataBuilder implements Builder<GArtistData, GArtistDataBuilder> {
  _$GArtistData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GArtistData_artistBuilder? _artist;
  GArtistData_artistBuilder get artist =>
      _$this._artist ??= new GArtistData_artistBuilder();
  set artist(GArtistData_artistBuilder? artist) => _$this._artist = artist;

  GArtistDataBuilder() {
    GArtistData._initializeBuilder(this);
  }

  GArtistDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _artist = $v.artist.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistData;
  }

  @override
  void update(void Function(GArtistDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistData build() {
    _$GArtistData _$result;
    try {
      _$result = _$v ??
          new _$GArtistData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistData', 'G__typename'),
              artist: artist.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'artist';
        artist.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistData_artist extends GArtistData_artist {
  @override
  final String G__typename;
  @override
  final BuiltList<GArtistData_artist_albums> albums;
  @override
  final BuiltList<GArtistData_artist_songs> songs;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GArtistData_artist(
          [void Function(GArtistData_artistBuilder)? updates]) =>
      (new GArtistData_artistBuilder()..update(updates)).build();

  _$GArtistData_artist._(
      {required this.G__typename,
      required this.albums,
      required this.songs,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistData_artist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        albums, 'GArtistData_artist', 'albums');
    BuiltValueNullFieldError.checkNotNull(songs, 'GArtistData_artist', 'songs');
    BuiltValueNullFieldError.checkNotNull(id, 'GArtistData_artist', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GArtistData_artist', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistData_artist', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistData_artist', 'updatedAt');
  }

  @override
  GArtistData_artist rebuild(
          void Function(GArtistData_artistBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistData_artistBuilder toBuilder() =>
      new GArtistData_artistBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistData_artist &&
        G__typename == other.G__typename &&
        albums == other.albums &&
        songs == other.songs &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, G__typename.hashCode), albums.hashCode),
                            songs.hashCode),
                        id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistData_artist')
          ..add('G__typename', G__typename)
          ..add('albums', albums)
          ..add('songs', songs)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistData_artistBuilder
    implements Builder<GArtistData_artist, GArtistData_artistBuilder> {
  _$GArtistData_artist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  ListBuilder<GArtistData_artist_albums>? _albums;
  ListBuilder<GArtistData_artist_albums> get albums =>
      _$this._albums ??= new ListBuilder<GArtistData_artist_albums>();
  set albums(ListBuilder<GArtistData_artist_albums>? albums) =>
      _$this._albums = albums;

  ListBuilder<GArtistData_artist_songs>? _songs;
  ListBuilder<GArtistData_artist_songs> get songs =>
      _$this._songs ??= new ListBuilder<GArtistData_artist_songs>();
  set songs(ListBuilder<GArtistData_artist_songs>? songs) =>
      _$this._songs = songs;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistData_artistBuilder() {
    GArtistData_artist._initializeBuilder(this);
  }

  GArtistData_artistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _albums = $v.albums.toBuilder();
      _songs = $v.songs.toBuilder();
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistData_artist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistData_artist;
  }

  @override
  void update(void Function(GArtistData_artistBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistData_artist build() {
    _$GArtistData_artist _$result;
    try {
      _$result = _$v ??
          new _$GArtistData_artist._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistData_artist', 'G__typename'),
              albums: albums.build(),
              songs: songs.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistData_artist', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GArtistData_artist', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
        _$failedField = 'songs';
        songs.build();

        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistData_artist', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistData_artist_albums extends GArtistData_artist_albums {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GArtistData_artist_albums(
          [void Function(GArtistData_artist_albumsBuilder)? updates]) =>
      (new GArtistData_artist_albumsBuilder()..update(updates)).build();

  _$GArtistData_artist_albums._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistData_artist_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GArtistData_artist_albums', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GArtistData_artist_albums', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistData_artist_albums', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistData_artist_albums', 'updatedAt');
  }

  @override
  GArtistData_artist_albums rebuild(
          void Function(GArtistData_artist_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistData_artist_albumsBuilder toBuilder() =>
      new GArtistData_artist_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistData_artist_albums &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistData_artist_albums')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistData_artist_albumsBuilder
    implements
        Builder<GArtistData_artist_albums, GArtistData_artist_albumsBuilder> {
  _$GArtistData_artist_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistData_artist_albumsBuilder() {
    GArtistData_artist_albums._initializeBuilder(this);
  }

  GArtistData_artist_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistData_artist_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistData_artist_albums;
  }

  @override
  void update(void Function(GArtistData_artist_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistData_artist_albums build() {
    _$GArtistData_artist_albums _$result;
    try {
      _$result = _$v ??
          new _$GArtistData_artist_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistData_artist_albums', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistData_artist_albums', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GArtistData_artist_albums', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistData_artist_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistData_artist_songs extends GArtistData_artist_songs {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GArtistData_artist_songs(
          [void Function(GArtistData_artist_songsBuilder)? updates]) =>
      (new GArtistData_artist_songsBuilder()..update(updates)).build();

  _$GArtistData_artist_songs._(
      {required this.G__typename,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistData_artist_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GArtistData_artist_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GArtistData_artist_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistData_artist_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistData_artist_songs', 'updatedAt');
  }

  @override
  GArtistData_artist_songs rebuild(
          void Function(GArtistData_artist_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistData_artist_songsBuilder toBuilder() =>
      new GArtistData_artist_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistData_artist_songs &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    title.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistData_artist_songs')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistData_artist_songsBuilder
    implements
        Builder<GArtistData_artist_songs, GArtistData_artist_songsBuilder> {
  _$GArtistData_artist_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistData_artist_songsBuilder() {
    GArtistData_artist_songs._initializeBuilder(this);
  }

  GArtistData_artist_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistData_artist_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistData_artist_songs;
  }

  @override
  void update(void Function(GArtistData_artist_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistData_artist_songs build() {
    _$GArtistData_artist_songs _$result;
    try {
      _$result = _$v ??
          new _$GArtistData_artist_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistData_artist_songs', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistData_artist_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GArtistData_artist_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistData_artist_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
