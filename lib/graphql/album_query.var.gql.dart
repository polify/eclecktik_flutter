// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'album_query.var.gql.g.dart';

abstract class GAlbumVars implements Built<GAlbumVars, GAlbumVarsBuilder> {
  GAlbumVars._();

  factory GAlbumVars([Function(GAlbumVarsBuilder b) updates]) = _$GAlbumVars;

  String get id;
  static Serializer<GAlbumVars> get serializer => _$gAlbumVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumVars.serializer, this)
          as Map<String, dynamic>);
  static GAlbumVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumVars.serializer, json);
}
