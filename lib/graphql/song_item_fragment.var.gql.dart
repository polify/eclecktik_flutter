// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'song_item_fragment.var.gql.g.dart';

abstract class GSongItemVars
    implements Built<GSongItemVars, GSongItemVarsBuilder> {
  GSongItemVars._();

  factory GSongItemVars([Function(GSongItemVarsBuilder b) updates]) =
      _$GSongItemVars;

  static Serializer<GSongItemVars> get serializer => _$gSongItemVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongItemVars.serializer, this)
          as Map<String, dynamic>);
  static GSongItemVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongItemVars.serializer, json);
}
