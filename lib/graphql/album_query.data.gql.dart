// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/artist_item_fragment.data.gql.dart' as _i4;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart' as _i5;

part 'album_query.data.gql.g.dart';

abstract class GAlbumData implements Built<GAlbumData, GAlbumDataBuilder> {
  GAlbumData._();

  factory GAlbumData([Function(GAlbumDataBuilder b) updates]) = _$GAlbumData;

  static void _initializeBuilder(GAlbumDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GAlbumData_album get album;
  static Serializer<GAlbumData> get serializer => _$gAlbumDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumData.serializer, this)
          as Map<String, dynamic>);
  static GAlbumData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumData.serializer, json);
}

abstract class GAlbumData_album
    implements
        Built<GAlbumData_album, GAlbumData_albumBuilder>,
        _i2.GAlbumItem {
  GAlbumData_album._();

  factory GAlbumData_album([Function(GAlbumData_albumBuilder b) updates]) =
      _$GAlbumData_album;

  static void _initializeBuilder(GAlbumData_albumBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  BuiltList<GAlbumData_album_artists> get artists;
  BuiltList<GAlbumData_album_songs> get songs;
  static Serializer<GAlbumData_album> get serializer =>
      _$gAlbumDataAlbumSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumData_album.serializer, this)
          as Map<String, dynamic>);
  static GAlbumData_album? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumData_album.serializer, json);
}

abstract class GAlbumData_album_artists
    implements
        Built<GAlbumData_album_artists, GAlbumData_album_artistsBuilder>,
        _i4.GArtistItem {
  GAlbumData_album_artists._();

  factory GAlbumData_album_artists(
          [Function(GAlbumData_album_artistsBuilder b) updates]) =
      _$GAlbumData_album_artists;

  static void _initializeBuilder(GAlbumData_album_artistsBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GAlbumData_album_artists> get serializer =>
      _$gAlbumDataAlbumArtistsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumData_album_artists.serializer, this)
          as Map<String, dynamic>);
  static GAlbumData_album_artists? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumData_album_artists.serializer, json);
}

abstract class GAlbumData_album_songs
    implements
        Built<GAlbumData_album_songs, GAlbumData_album_songsBuilder>,
        _i5.GSongCard {
  GAlbumData_album_songs._();

  factory GAlbumData_album_songs(
          [Function(GAlbumData_album_songsBuilder b) updates]) =
      _$GAlbumData_album_songs;

  static void _initializeBuilder(GAlbumData_album_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  String get duration;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  GAlbumData_album_songs_signedUrl? get signedUrl;
  GAlbumData_album_songs_album? get album;
  GAlbumData_album_songs_artist? get artist;
  static Serializer<GAlbumData_album_songs> get serializer =>
      _$gAlbumDataAlbumSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumData_album_songs.serializer, this)
          as Map<String, dynamic>);
  static GAlbumData_album_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumData_album_songs.serializer, json);
}

abstract class GAlbumData_album_songs_signedUrl
    implements
        Built<GAlbumData_album_songs_signedUrl,
            GAlbumData_album_songs_signedUrlBuilder>,
        _i5.GSongCard_signedUrl {
  GAlbumData_album_songs_signedUrl._();

  factory GAlbumData_album_songs_signedUrl(
          [Function(GAlbumData_album_songs_signedUrlBuilder b) updates]) =
      _$GAlbumData_album_songs_signedUrl;

  static void _initializeBuilder(GAlbumData_album_songs_signedUrlBuilder b) =>
      b..G__typename = 'S3SignedUrl';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get signedUrl;
  static Serializer<GAlbumData_album_songs_signedUrl> get serializer =>
      _$gAlbumDataAlbumSongsSignedUrlSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GAlbumData_album_songs_signedUrl.serializer, this)
      as Map<String, dynamic>);
  static GAlbumData_album_songs_signedUrl? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumData_album_songs_signedUrl.serializer, json);
}

abstract class GAlbumData_album_songs_album
    implements
        Built<GAlbumData_album_songs_album,
            GAlbumData_album_songs_albumBuilder>,
        _i5.GSongCard_album {
  GAlbumData_album_songs_album._();

  factory GAlbumData_album_songs_album(
          [Function(GAlbumData_album_songs_albumBuilder b) updates]) =
      _$GAlbumData_album_songs_album;

  static void _initializeBuilder(GAlbumData_album_songs_albumBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GAlbumData_album_songs_album> get serializer =>
      _$gAlbumDataAlbumSongsAlbumSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GAlbumData_album_songs_album.serializer, this) as Map<String, dynamic>);
  static GAlbumData_album_songs_album? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumData_album_songs_album.serializer, json);
}

abstract class GAlbumData_album_songs_artist
    implements
        Built<GAlbumData_album_songs_artist,
            GAlbumData_album_songs_artistBuilder>,
        _i5.GSongCard_artist {
  GAlbumData_album_songs_artist._();

  factory GAlbumData_album_songs_artist(
          [Function(GAlbumData_album_songs_artistBuilder b) updates]) =
      _$GAlbumData_album_songs_artist;

  static void _initializeBuilder(GAlbumData_album_songs_artistBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GAlbumData_album_songs_artist> get serializer =>
      _$gAlbumDataAlbumSongsArtistSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GAlbumData_album_songs_artist.serializer, this) as Map<String, dynamic>);
  static GAlbumData_album_songs_artist? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumData_album_songs_artist.serializer, json);
}
