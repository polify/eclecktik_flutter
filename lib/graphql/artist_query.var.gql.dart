// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'artist_query.var.gql.g.dart';

abstract class GArtistVars implements Built<GArtistVars, GArtistVarsBuilder> {
  GArtistVars._();

  factory GArtistVars([Function(GArtistVarsBuilder b) updates]) = _$GArtistVars;

  String get id;
  static Serializer<GArtistVars> get serializer => _$gArtistVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistVars.serializer, this)
          as Map<String, dynamic>);
  static GArtistVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistVars.serializer, json);
}
