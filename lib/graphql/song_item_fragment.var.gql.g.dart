// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song_item_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongItemVars> _$gSongItemVarsSerializer =
    new _$GSongItemVarsSerializer();

class _$GSongItemVarsSerializer implements StructuredSerializer<GSongItemVars> {
  @override
  final Iterable<Type> types = const [GSongItemVars, _$GSongItemVars];
  @override
  final String wireName = 'GSongItemVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongItemVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GSongItemVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GSongItemVarsBuilder().build();
  }
}

class _$GSongItemVars extends GSongItemVars {
  factory _$GSongItemVars([void Function(GSongItemVarsBuilder)? updates]) =>
      (new GSongItemVarsBuilder()..update(updates)).build();

  _$GSongItemVars._() : super._();

  @override
  GSongItemVars rebuild(void Function(GSongItemVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongItemVarsBuilder toBuilder() => new GSongItemVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongItemVars;
  }

  @override
  int get hashCode {
    return 527987014;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GSongItemVars').toString();
  }
}

class GSongItemVarsBuilder
    implements Builder<GSongItemVars, GSongItemVarsBuilder> {
  _$GSongItemVars? _$v;

  GSongItemVarsBuilder();

  @override
  void replace(GSongItemVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongItemVars;
  }

  @override
  void update(void Function(GSongItemVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongItemVars build() {
    final _$result = _$v ?? new _$GSongItemVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
