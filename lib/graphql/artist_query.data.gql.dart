// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart' as _i4;
import 'package:eclektik/graphql/artist_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart' as _i5;

part 'artist_query.data.gql.g.dart';

abstract class GArtistData implements Built<GArtistData, GArtistDataBuilder> {
  GArtistData._();

  factory GArtistData([Function(GArtistDataBuilder b) updates]) = _$GArtistData;

  static void _initializeBuilder(GArtistDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GArtistData_artist get artist;
  static Serializer<GArtistData> get serializer => _$gArtistDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistData.serializer, this)
          as Map<String, dynamic>);
  static GArtistData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistData.serializer, json);
}

abstract class GArtistData_artist
    implements
        Built<GArtistData_artist, GArtistData_artistBuilder>,
        _i2.GArtistCard {
  GArtistData_artist._();

  factory GArtistData_artist([Function(GArtistData_artistBuilder b) updates]) =
      _$GArtistData_artist;

  static void _initializeBuilder(GArtistData_artistBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  BuiltList<GArtistData_artist_albums> get albums;
  BuiltList<GArtistData_artist_songs> get songs;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GArtistData_artist> get serializer =>
      _$gArtistDataArtistSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistData_artist.serializer, this)
          as Map<String, dynamic>);
  static GArtistData_artist? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistData_artist.serializer, json);
}

abstract class GArtistData_artist_albums
    implements
        Built<GArtistData_artist_albums, GArtistData_artist_albumsBuilder>,
        _i2.GArtistCard_albums,
        _i4.GAlbumItem {
  GArtistData_artist_albums._();

  factory GArtistData_artist_albums(
          [Function(GArtistData_artist_albumsBuilder b) updates]) =
      _$GArtistData_artist_albums;

  static void _initializeBuilder(GArtistData_artist_albumsBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GArtistData_artist_albums> get serializer =>
      _$gArtistDataArtistAlbumsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistData_artist_albums.serializer, this)
          as Map<String, dynamic>);
  static GArtistData_artist_albums? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GArtistData_artist_albums.serializer, json);
}

abstract class GArtistData_artist_songs
    implements
        Built<GArtistData_artist_songs, GArtistData_artist_songsBuilder>,
        _i2.GArtistCard_songs,
        _i5.GSongItem {
  GArtistData_artist_songs._();

  factory GArtistData_artist_songs(
          [Function(GArtistData_artist_songsBuilder b) updates]) =
      _$GArtistData_artist_songs;

  static void _initializeBuilder(GArtistData_artist_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GArtistData_artist_songs> get serializer =>
      _$gArtistDataArtistSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistData_artist_songs.serializer, this)
          as Map<String, dynamic>);
  static GArtistData_artist_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GArtistData_artist_songs.serializer, json);
}
