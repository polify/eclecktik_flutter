// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'albums_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GAlbumsData> _$gAlbumsDataSerializer = new _$GAlbumsDataSerializer();
Serializer<GAlbumsData_albums> _$gAlbumsDataAlbumsSerializer =
    new _$GAlbumsData_albumsSerializer();
Serializer<GAlbumsData_albums_albums> _$gAlbumsDataAlbumsAlbumsSerializer =
    new _$GAlbumsData_albums_albumsSerializer();
Serializer<GAlbumsData_albums_albums_songs>
    _$gAlbumsDataAlbumsAlbumsSongsSerializer =
    new _$GAlbumsData_albums_albums_songsSerializer();
Serializer<GAlbumsData_albums_albums_artists>
    _$gAlbumsDataAlbumsAlbumsArtistsSerializer =
    new _$GAlbumsData_albums_albums_artistsSerializer();

class _$GAlbumsDataSerializer implements StructuredSerializer<GAlbumsData> {
  @override
  final Iterable<Type> types = const [GAlbumsData, _$GAlbumsData];
  @override
  final String wireName = 'GAlbumsData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GAlbumsData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'albums',
      serializers.serialize(object.albums,
          specifiedType: const FullType(GAlbumsData_albums)),
    ];

    return result;
  }

  @override
  GAlbumsData deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumsDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GAlbumsData_albums))!
              as GAlbumsData_albums);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumsData_albumsSerializer
    implements StructuredSerializer<GAlbumsData_albums> {
  @override
  final Iterable<Type> types = const [GAlbumsData_albums, _$GAlbumsData_albums];
  @override
  final String wireName = 'GAlbumsData_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumsData_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'albums',
      serializers.serialize(object.albums,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GAlbumsData_albums_albums)])),
    ];
    Object? value;
    value = object.total;
    if (value != null) {
      result
        ..add('total')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GAlbumsData_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumsData_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'total':
          result.total = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GAlbumsData_albums_albums)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumsData_albums_albumsSerializer
    implements StructuredSerializer<GAlbumsData_albums_albums> {
  @override
  final Iterable<Type> types = const [
    GAlbumsData_albums_albums,
    _$GAlbumsData_albums_albums
  ];
  @override
  final String wireName = 'GAlbumsData_albums_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumsData_albums_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(BuiltList,
              const [const FullType(GAlbumsData_albums_albums_songs)])),
      'artists',
      serializers.serialize(object.artists,
          specifiedType: const FullType(BuiltList,
              const [const FullType(GAlbumsData_albums_albums_artists)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GAlbumsData_albums_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumsData_albums_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GAlbumsData_albums_albums_songs)
              ]))! as BuiltList<Object?>);
          break;
        case 'artists':
          result.artists.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GAlbumsData_albums_albums_artists)
              ]))! as BuiltList<Object?>);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumsData_albums_albums_songsSerializer
    implements StructuredSerializer<GAlbumsData_albums_albums_songs> {
  @override
  final Iterable<Type> types = const [
    GAlbumsData_albums_albums_songs,
    _$GAlbumsData_albums_albums_songs
  ];
  @override
  final String wireName = 'GAlbumsData_albums_albums_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumsData_albums_albums_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumsData_albums_albums_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumsData_albums_albums_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumsData_albums_albums_artistsSerializer
    implements StructuredSerializer<GAlbumsData_albums_albums_artists> {
  @override
  final Iterable<Type> types = const [
    GAlbumsData_albums_albums_artists,
    _$GAlbumsData_albums_albums_artists
  ];
  @override
  final String wireName = 'GAlbumsData_albums_albums_artists';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumsData_albums_albums_artists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumsData_albums_albums_artists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumsData_albums_albums_artistsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumsData extends GAlbumsData {
  @override
  final String G__typename;
  @override
  final GAlbumsData_albums albums;

  factory _$GAlbumsData([void Function(GAlbumsDataBuilder)? updates]) =>
      (new GAlbumsDataBuilder()..update(updates)).build();

  _$GAlbumsData._({required this.G__typename, required this.albums})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumsData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(albums, 'GAlbumsData', 'albums');
  }

  @override
  GAlbumsData rebuild(void Function(GAlbumsDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumsDataBuilder toBuilder() => new GAlbumsDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumsData &&
        G__typename == other.G__typename &&
        albums == other.albums;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), albums.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumsData')
          ..add('G__typename', G__typename)
          ..add('albums', albums))
        .toString();
  }
}

class GAlbumsDataBuilder implements Builder<GAlbumsData, GAlbumsDataBuilder> {
  _$GAlbumsData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GAlbumsData_albumsBuilder? _albums;
  GAlbumsData_albumsBuilder get albums =>
      _$this._albums ??= new GAlbumsData_albumsBuilder();
  set albums(GAlbumsData_albumsBuilder? albums) => _$this._albums = albums;

  GAlbumsDataBuilder() {
    GAlbumsData._initializeBuilder(this);
  }

  GAlbumsDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _albums = $v.albums.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumsData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumsData;
  }

  @override
  void update(void Function(GAlbumsDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumsData build() {
    _$GAlbumsData _$result;
    try {
      _$result = _$v ??
          new _$GAlbumsData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumsData', 'G__typename'),
              albums: albums.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumsData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumsData_albums extends GAlbumsData_albums {
  @override
  final String G__typename;
  @override
  final int? total;
  @override
  final BuiltList<GAlbumsData_albums_albums> albums;

  factory _$GAlbumsData_albums(
          [void Function(GAlbumsData_albumsBuilder)? updates]) =>
      (new GAlbumsData_albumsBuilder()..update(updates)).build();

  _$GAlbumsData_albums._(
      {required this.G__typename, this.total, required this.albums})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumsData_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        albums, 'GAlbumsData_albums', 'albums');
  }

  @override
  GAlbumsData_albums rebuild(
          void Function(GAlbumsData_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumsData_albumsBuilder toBuilder() =>
      new GAlbumsData_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumsData_albums &&
        G__typename == other.G__typename &&
        total == other.total &&
        albums == other.albums;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), total.hashCode), albums.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumsData_albums')
          ..add('G__typename', G__typename)
          ..add('total', total)
          ..add('albums', albums))
        .toString();
  }
}

class GAlbumsData_albumsBuilder
    implements Builder<GAlbumsData_albums, GAlbumsData_albumsBuilder> {
  _$GAlbumsData_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _total;
  int? get total => _$this._total;
  set total(int? total) => _$this._total = total;

  ListBuilder<GAlbumsData_albums_albums>? _albums;
  ListBuilder<GAlbumsData_albums_albums> get albums =>
      _$this._albums ??= new ListBuilder<GAlbumsData_albums_albums>();
  set albums(ListBuilder<GAlbumsData_albums_albums>? albums) =>
      _$this._albums = albums;

  GAlbumsData_albumsBuilder() {
    GAlbumsData_albums._initializeBuilder(this);
  }

  GAlbumsData_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _total = $v.total;
      _albums = $v.albums.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumsData_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumsData_albums;
  }

  @override
  void update(void Function(GAlbumsData_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumsData_albums build() {
    _$GAlbumsData_albums _$result;
    try {
      _$result = _$v ??
          new _$GAlbumsData_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumsData_albums', 'G__typename'),
              total: total,
              albums: albums.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumsData_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumsData_albums_albums extends GAlbumsData_albums_albums {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final BuiltList<GAlbumsData_albums_albums_songs> songs;
  @override
  final BuiltList<GAlbumsData_albums_albums_artists> artists;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GAlbumsData_albums_albums(
          [void Function(GAlbumsData_albums_albumsBuilder)? updates]) =>
      (new GAlbumsData_albums_albumsBuilder()..update(updates)).build();

  _$GAlbumsData_albums_albums._(
      {required this.G__typename,
      required this.s3Key,
      required this.songs,
      required this.artists,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumsData_albums_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        s3Key, 'GAlbumsData_albums_albums', 's3Key');
    BuiltValueNullFieldError.checkNotNull(
        songs, 'GAlbumsData_albums_albums', 'songs');
    BuiltValueNullFieldError.checkNotNull(
        artists, 'GAlbumsData_albums_albums', 'artists');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GAlbumsData_albums_albums', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GAlbumsData_albums_albums', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GAlbumsData_albums_albums', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GAlbumsData_albums_albums', 'updatedAt');
  }

  @override
  GAlbumsData_albums_albums rebuild(
          void Function(GAlbumsData_albums_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumsData_albums_albumsBuilder toBuilder() =>
      new GAlbumsData_albums_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumsData_albums_albums &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        songs == other.songs &&
        artists == other.artists &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, G__typename.hashCode),
                                    s3Key.hashCode),
                                songs.hashCode),
                            artists.hashCode),
                        id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumsData_albums_albums')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('songs', songs)
          ..add('artists', artists)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GAlbumsData_albums_albumsBuilder
    implements
        Builder<GAlbumsData_albums_albums, GAlbumsData_albums_albumsBuilder> {
  _$GAlbumsData_albums_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  ListBuilder<GAlbumsData_albums_albums_songs>? _songs;
  ListBuilder<GAlbumsData_albums_albums_songs> get songs =>
      _$this._songs ??= new ListBuilder<GAlbumsData_albums_albums_songs>();
  set songs(ListBuilder<GAlbumsData_albums_albums_songs>? songs) =>
      _$this._songs = songs;

  ListBuilder<GAlbumsData_albums_albums_artists>? _artists;
  ListBuilder<GAlbumsData_albums_albums_artists> get artists =>
      _$this._artists ??= new ListBuilder<GAlbumsData_albums_albums_artists>();
  set artists(ListBuilder<GAlbumsData_albums_albums_artists>? artists) =>
      _$this._artists = artists;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GAlbumsData_albums_albumsBuilder() {
    GAlbumsData_albums_albums._initializeBuilder(this);
  }

  GAlbumsData_albums_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _songs = $v.songs.toBuilder();
      _artists = $v.artists.toBuilder();
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumsData_albums_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumsData_albums_albums;
  }

  @override
  void update(void Function(GAlbumsData_albums_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumsData_albums_albums build() {
    _$GAlbumsData_albums_albums _$result;
    try {
      _$result = _$v ??
          new _$GAlbumsData_albums_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumsData_albums_albums', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GAlbumsData_albums_albums', 's3Key'),
              songs: songs.build(),
              artists: artists.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GAlbumsData_albums_albums', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GAlbumsData_albums_albums', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'songs';
        songs.build();
        _$failedField = 'artists';
        artists.build();

        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumsData_albums_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumsData_albums_albums_songs
    extends GAlbumsData_albums_albums_songs {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;

  factory _$GAlbumsData_albums_albums_songs(
          [void Function(GAlbumsData_albums_albums_songsBuilder)? updates]) =>
      (new GAlbumsData_albums_albums_songsBuilder()..update(updates)).build();

  _$GAlbumsData_albums_albums_songs._(
      {required this.G__typename, required this.id, required this.title})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumsData_albums_albums_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GAlbumsData_albums_albums_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GAlbumsData_albums_albums_songs', 'title');
  }

  @override
  GAlbumsData_albums_albums_songs rebuild(
          void Function(GAlbumsData_albums_albums_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumsData_albums_albums_songsBuilder toBuilder() =>
      new GAlbumsData_albums_albums_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumsData_albums_albums_songs &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), title.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumsData_albums_albums_songs')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title))
        .toString();
  }
}

class GAlbumsData_albums_albums_songsBuilder
    implements
        Builder<GAlbumsData_albums_albums_songs,
            GAlbumsData_albums_albums_songsBuilder> {
  _$GAlbumsData_albums_albums_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  GAlbumsData_albums_albums_songsBuilder() {
    GAlbumsData_albums_albums_songs._initializeBuilder(this);
  }

  GAlbumsData_albums_albums_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumsData_albums_albums_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumsData_albums_albums_songs;
  }

  @override
  void update(void Function(GAlbumsData_albums_albums_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumsData_albums_albums_songs build() {
    final _$result = _$v ??
        new _$GAlbumsData_albums_albums_songs._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GAlbumsData_albums_albums_songs', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumsData_albums_albums_songs', 'id'),
            title: BuiltValueNullFieldError.checkNotNull(
                title, 'GAlbumsData_albums_albums_songs', 'title'));
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumsData_albums_albums_artists
    extends GAlbumsData_albums_albums_artists {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GAlbumsData_albums_albums_artists(
          [void Function(GAlbumsData_albums_albums_artistsBuilder)? updates]) =>
      (new GAlbumsData_albums_albums_artistsBuilder()..update(updates)).build();

  _$GAlbumsData_albums_albums_artists._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumsData_albums_albums_artists', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GAlbumsData_albums_albums_artists', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GAlbumsData_albums_albums_artists', 'name');
  }

  @override
  GAlbumsData_albums_albums_artists rebuild(
          void Function(GAlbumsData_albums_albums_artistsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumsData_albums_albums_artistsBuilder toBuilder() =>
      new GAlbumsData_albums_albums_artistsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumsData_albums_albums_artists &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumsData_albums_albums_artists')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GAlbumsData_albums_albums_artistsBuilder
    implements
        Builder<GAlbumsData_albums_albums_artists,
            GAlbumsData_albums_albums_artistsBuilder> {
  _$GAlbumsData_albums_albums_artists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GAlbumsData_albums_albums_artistsBuilder() {
    GAlbumsData_albums_albums_artists._initializeBuilder(this);
  }

  GAlbumsData_albums_albums_artistsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumsData_albums_albums_artists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumsData_albums_albums_artists;
  }

  @override
  void update(
      void Function(GAlbumsData_albums_albums_artistsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumsData_albums_albums_artists build() {
    final _$result = _$v ??
        new _$GAlbumsData_albums_albums_artists._(
            G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                'GAlbumsData_albums_albums_artists', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumsData_albums_albums_artists', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GAlbumsData_albums_albums_artists', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
