// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:eclektik/graphql/album_card_fragment.ast.gql.dart' as _i2;
import 'package:eclektik/graphql/album_item_fragment.ast.gql.dart' as _i3;
import 'package:gql/ast.dart' as _i1;

const Albums = _i1.OperationDefinitionNode(
    type: _i1.OperationType.query,
    name: _i1.NameNode(value: 'Albums'),
    variableDefinitions: [
      _i1.VariableDefinitionNode(
          variable: _i1.VariableNode(name: _i1.NameNode(value: 'limit')),
          type: _i1.NamedTypeNode(
              name: _i1.NameNode(value: 'Int'), isNonNull: false),
          defaultValue: _i1.DefaultValueNode(value: null),
          directives: []),
      _i1.VariableDefinitionNode(
          variable: _i1.VariableNode(name: _i1.NameNode(value: 'offset')),
          type: _i1.NamedTypeNode(
              name: _i1.NameNode(value: 'Int'), isNonNull: false),
          defaultValue: _i1.DefaultValueNode(value: null),
          directives: [])
    ],
    directives: [],
    selectionSet: _i1.SelectionSetNode(selections: [
      _i1.FieldNode(
          name: _i1.NameNode(value: 'albums'),
          alias: null,
          arguments: [
            _i1.ArgumentNode(
                name: _i1.NameNode(value: 'pagination'),
                value: _i1.ObjectValueNode(fields: [
                  _i1.ObjectFieldNode(
                      name: _i1.NameNode(value: 'count'),
                      value:
                          _i1.VariableNode(name: _i1.NameNode(value: 'limit'))),
                  _i1.ObjectFieldNode(
                      name: _i1.NameNode(value: 'page'),
                      value:
                          _i1.VariableNode(name: _i1.NameNode(value: 'offset')))
                ]))
          ],
          directives: [],
          selectionSet: _i1.SelectionSetNode(selections: [
            _i1.FieldNode(
                name: _i1.NameNode(value: 'total'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null),
            _i1.FieldNode(
                name: _i1.NameNode(value: 'albums'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: _i1.SelectionSetNode(selections: [
                  _i1.FragmentSpreadNode(
                      name: _i1.NameNode(value: 'AlbumCard'), directives: [])
                ]))
          ]))
    ]));
const document =
    _i1.DocumentNode(definitions: [Albums, _i2.AlbumCard, _i3.AlbumItem]);
