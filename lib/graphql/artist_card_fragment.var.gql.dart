// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'artist_card_fragment.var.gql.g.dart';

abstract class GArtistCardVars
    implements Built<GArtistCardVars, GArtistCardVarsBuilder> {
  GArtistCardVars._();

  factory GArtistCardVars([Function(GArtistCardVarsBuilder b) updates]) =
      _$GArtistCardVars;

  static Serializer<GArtistCardVars> get serializer =>
      _$gArtistCardVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistCardVars.serializer, this)
          as Map<String, dynamic>);
  static GArtistCardVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistCardVars.serializer, json);
}
