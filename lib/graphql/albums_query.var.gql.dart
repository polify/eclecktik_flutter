// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'albums_query.var.gql.g.dart';

abstract class GAlbumsVars implements Built<GAlbumsVars, GAlbumsVarsBuilder> {
  GAlbumsVars._();

  factory GAlbumsVars([Function(GAlbumsVarsBuilder b) updates]) = _$GAlbumsVars;

  int? get limit;
  int? get offset;
  static Serializer<GAlbumsVars> get serializer => _$gAlbumsVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumsVars.serializer, this)
          as Map<String, dynamic>);
  static GAlbumsVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumsVars.serializer, json);
}
