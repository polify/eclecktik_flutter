// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'song_card_fragment.var.gql.g.dart';

abstract class GSongCardVars
    implements Built<GSongCardVars, GSongCardVarsBuilder> {
  GSongCardVars._();

  factory GSongCardVars([Function(GSongCardVarsBuilder b) updates]) =
      _$GSongCardVars;

  static Serializer<GSongCardVars> get serializer => _$gSongCardVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongCardVars.serializer, this)
          as Map<String, dynamic>);
  static GSongCardVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongCardVars.serializer, json);
}
