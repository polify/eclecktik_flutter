// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'albums_query.data.gql.g.dart';

abstract class GAlbumsData implements Built<GAlbumsData, GAlbumsDataBuilder> {
  GAlbumsData._();

  factory GAlbumsData([Function(GAlbumsDataBuilder b) updates]) = _$GAlbumsData;

  static void _initializeBuilder(GAlbumsDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GAlbumsData_albums get albums;
  static Serializer<GAlbumsData> get serializer => _$gAlbumsDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumsData.serializer, this)
          as Map<String, dynamic>);
  static GAlbumsData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumsData.serializer, json);
}

abstract class GAlbumsData_albums
    implements Built<GAlbumsData_albums, GAlbumsData_albumsBuilder> {
  GAlbumsData_albums._();

  factory GAlbumsData_albums([Function(GAlbumsData_albumsBuilder b) updates]) =
      _$GAlbumsData_albums;

  static void _initializeBuilder(GAlbumsData_albumsBuilder b) =>
      b..G__typename = 'AlbumResults';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int? get total;
  BuiltList<GAlbumsData_albums_albums> get albums;
  static Serializer<GAlbumsData_albums> get serializer =>
      _$gAlbumsDataAlbumsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumsData_albums.serializer, this)
          as Map<String, dynamic>);
  static GAlbumsData_albums? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GAlbumsData_albums.serializer, json);
}

abstract class GAlbumsData_albums_albums
    implements
        Built<GAlbumsData_albums_albums, GAlbumsData_albums_albumsBuilder>,
        _i2.GAlbumCard {
  GAlbumsData_albums_albums._();

  factory GAlbumsData_albums_albums(
          [Function(GAlbumsData_albums_albumsBuilder b) updates]) =
      _$GAlbumsData_albums_albums;

  static void _initializeBuilder(GAlbumsData_albums_albumsBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  BuiltList<GAlbumsData_albums_albums_songs> get songs;
  BuiltList<GAlbumsData_albums_albums_artists> get artists;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GAlbumsData_albums_albums> get serializer =>
      _$gAlbumsDataAlbumsAlbumsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GAlbumsData_albums_albums.serializer, this)
          as Map<String, dynamic>);
  static GAlbumsData_albums_albums? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumsData_albums_albums.serializer, json);
}

abstract class GAlbumsData_albums_albums_songs
    implements
        Built<GAlbumsData_albums_albums_songs,
            GAlbumsData_albums_albums_songsBuilder>,
        _i2.GAlbumCard_songs {
  GAlbumsData_albums_albums_songs._();

  factory GAlbumsData_albums_albums_songs(
          [Function(GAlbumsData_albums_albums_songsBuilder b) updates]) =
      _$GAlbumsData_albums_albums_songs;

  static void _initializeBuilder(GAlbumsData_albums_albums_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  static Serializer<GAlbumsData_albums_albums_songs> get serializer =>
      _$gAlbumsDataAlbumsAlbumsSongsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GAlbumsData_albums_albums_songs.serializer, this)
      as Map<String, dynamic>);
  static GAlbumsData_albums_albums_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumsData_albums_albums_songs.serializer, json);
}

abstract class GAlbumsData_albums_albums_artists
    implements
        Built<GAlbumsData_albums_albums_artists,
            GAlbumsData_albums_albums_artistsBuilder>,
        _i2.GAlbumCard_artists {
  GAlbumsData_albums_albums_artists._();

  factory GAlbumsData_albums_albums_artists(
          [Function(GAlbumsData_albums_albums_artistsBuilder b) updates]) =
      _$GAlbumsData_albums_albums_artists;

  static void _initializeBuilder(GAlbumsData_albums_albums_artistsBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GAlbumsData_albums_albums_artists> get serializer =>
      _$gAlbumsDataAlbumsAlbumsArtistsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GAlbumsData_albums_albums_artists.serializer, this)
      as Map<String, dynamic>);
  static GAlbumsData_albums_albums_artists? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GAlbumsData_albums_albums_artists.serializer, json);
}
