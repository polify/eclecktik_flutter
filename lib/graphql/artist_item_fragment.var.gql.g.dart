// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artist_item_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GArtistItemVars> _$gArtistItemVarsSerializer =
    new _$GArtistItemVarsSerializer();

class _$GArtistItemVarsSerializer
    implements StructuredSerializer<GArtistItemVars> {
  @override
  final Iterable<Type> types = const [GArtistItemVars, _$GArtistItemVars];
  @override
  final String wireName = 'GArtistItemVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GArtistItemVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GArtistItemVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GArtistItemVarsBuilder().build();
  }
}

class _$GArtistItemVars extends GArtistItemVars {
  factory _$GArtistItemVars([void Function(GArtistItemVarsBuilder)? updates]) =>
      (new GArtistItemVarsBuilder()..update(updates)).build();

  _$GArtistItemVars._() : super._();

  @override
  GArtistItemVars rebuild(void Function(GArtistItemVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistItemVarsBuilder toBuilder() =>
      new GArtistItemVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistItemVars;
  }

  @override
  int get hashCode {
    return 888990477;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GArtistItemVars').toString();
  }
}

class GArtistItemVarsBuilder
    implements Builder<GArtistItemVars, GArtistItemVarsBuilder> {
  _$GArtistItemVars? _$v;

  GArtistItemVarsBuilder();

  @override
  void replace(GArtistItemVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistItemVars;
  }

  @override
  void update(void Function(GArtistItemVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistItemVars build() {
    final _$result = _$v ?? new _$GArtistItemVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
