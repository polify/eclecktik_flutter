// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(GAlbumCardData.serializer)
      ..add(GAlbumCardData_artists.serializer)
      ..add(GAlbumCardData_songs.serializer)
      ..add(GAlbumCardReq.serializer)
      ..add(GAlbumCardVars.serializer)
      ..add(GAlbumData.serializer)
      ..add(GAlbumData_album.serializer)
      ..add(GAlbumData_album_artists.serializer)
      ..add(GAlbumData_album_songs.serializer)
      ..add(GAlbumData_album_songs_album.serializer)
      ..add(GAlbumData_album_songs_artist.serializer)
      ..add(GAlbumData_album_songs_signedUrl.serializer)
      ..add(GAlbumItemData.serializer)
      ..add(GAlbumItemReq.serializer)
      ..add(GAlbumItemVars.serializer)
      ..add(GAlbumReq.serializer)
      ..add(GAlbumVars.serializer)
      ..add(GAlbumsData.serializer)
      ..add(GAlbumsData_albums.serializer)
      ..add(GAlbumsData_albums_albums.serializer)
      ..add(GAlbumsData_albums_albums_artists.serializer)
      ..add(GAlbumsData_albums_albums_songs.serializer)
      ..add(GAlbumsReq.serializer)
      ..add(GAlbumsVars.serializer)
      ..add(GArtistCardData.serializer)
      ..add(GArtistCardData_albums.serializer)
      ..add(GArtistCardData_songs.serializer)
      ..add(GArtistCardReq.serializer)
      ..add(GArtistCardVars.serializer)
      ..add(GArtistData.serializer)
      ..add(GArtistData_artist.serializer)
      ..add(GArtistData_artist_albums.serializer)
      ..add(GArtistData_artist_songs.serializer)
      ..add(GArtistItemData.serializer)
      ..add(GArtistItemReq.serializer)
      ..add(GArtistItemVars.serializer)
      ..add(GArtistReq.serializer)
      ..add(GArtistVars.serializer)
      ..add(GArtistsData.serializer)
      ..add(GArtistsData_artists.serializer)
      ..add(GArtistsData_artists_albums.serializer)
      ..add(GArtistsData_artists_songs.serializer)
      ..add(GArtistsReq.serializer)
      ..add(GArtistsVars.serializer)
      ..add(GDeviceItemData.serializer)
      ..add(GDeviceItemReq.serializer)
      ..add(GDeviceItemVars.serializer)
      ..add(GItemsInput.serializer)
      ..add(GLoginData.serializer)
      ..add(GLoginData_login.serializer)
      ..add(GLoginData_login_user.serializer)
      ..add(GLoginReq.serializer)
      ..add(GLoginVars.serializer)
      ..add(GNewUser.serializer)
      ..add(GPaginationInput.serializer)
      ..add(GProfileData.serializer)
      ..add(GProfileData_profile.serializer)
      ..add(GProfileData_profile_devices.serializer)
      ..add(GProfileData_profile_s3Credential.serializer)
      ..add(GProfileReq.serializer)
      ..add(GProfileVars.serializer)
      ..add(GRandomPlaylistData.serializer)
      ..add(GRandomPlaylistData_randomPlaylist.serializer)
      ..add(GRandomPlaylistData_randomPlaylist_songs.serializer)
      ..add(GRandomPlaylistData_randomPlaylist_songs_album.serializer)
      ..add(GRandomPlaylistData_randomPlaylist_songs_artist.serializer)
      ..add(GRandomPlaylistData_randomPlaylist_songs_signedUrl.serializer)
      ..add(GRandomPlaylistReq.serializer)
      ..add(GRandomPlaylistVars.serializer)
      ..add(GS3CredentialCreateInput.serializer)
      ..add(GS3CredentialUpdateInput.serializer)
      ..add(GSearchData.serializer)
      ..add(GSearchData_search.serializer)
      ..add(GSearchData_search_albums.serializer)
      ..add(GSearchData_search_albums_albums.serializer)
      ..add(GSearchData_search_artists.serializer)
      ..add(GSearchData_search_artists_artists.serializer)
      ..add(GSearchData_search_songs.serializer)
      ..add(GSearchData_search_songs_songs.serializer)
      ..add(GSearchReq.serializer)
      ..add(GSearchSongInput.serializer)
      ..add(GSearchVars.serializer)
      ..add(GSongCardData.serializer)
      ..add(GSongCardData_album.serializer)
      ..add(GSongCardData_artist.serializer)
      ..add(GSongCardData_signedUrl.serializer)
      ..add(GSongCardReq.serializer)
      ..add(GSongCardVars.serializer)
      ..add(GSongData.serializer)
      ..add(GSongData_song.serializer)
      ..add(GSongData_song_album.serializer)
      ..add(GSongData_song_artist.serializer)
      ..add(GSongData_song_signedUrl.serializer)
      ..add(GSongItemData.serializer)
      ..add(GSongItemReq.serializer)
      ..add(GSongItemVars.serializer)
      ..add(GSongReq.serializer)
      ..add(GSongVars.serializer)
      ..add(GSongsData.serializer)
      ..add(GSongsData_songs.serializer)
      ..add(GSongsData_songs_songs.serializer)
      ..add(GSongsData_songs_songs_album.serializer)
      ..add(GSongsData_songs_songs_artist.serializer)
      ..add(GSongsData_songs_songs_signedUrl.serializer)
      ..add(GSongsReq.serializer)
      ..add(GSongsVars.serializer)
      ..add(GTime.serializer)
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GAlbumCardData_songs)]),
          () => new ListBuilder<GAlbumCardData_songs>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GAlbumCardData_artists)]),
          () => new ListBuilder<GAlbumCardData_artists>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GAlbumData_album_artists)]),
          () => new ListBuilder<GAlbumData_album_artists>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GAlbumData_album_songs)]),
          () => new ListBuilder<GAlbumData_album_songs>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GAlbumsData_albums_albums)]),
          () => new ListBuilder<GAlbumsData_albums_albums>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GAlbumsData_albums_albums_songs)]),
          () => new ListBuilder<GAlbumsData_albums_albums_songs>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GAlbumsData_albums_albums_artists)]),
          () => new ListBuilder<GAlbumsData_albums_albums_artists>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistCardData_albums)]),
          () => new ListBuilder<GArtistCardData_albums>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistCardData_songs)]),
          () => new ListBuilder<GArtistCardData_songs>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistData_artist_albums)]),
          () => new ListBuilder<GArtistData_artist_albums>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistData_artist_songs)]),
          () => new ListBuilder<GArtistData_artist_songs>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistsData_artists)]),
          () => new ListBuilder<GArtistsData_artists>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistsData_artists_albums)]),
          () => new ListBuilder<GArtistsData_artists_albums>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GArtistsData_artists_songs)]),
          () => new ListBuilder<GArtistsData_artists_songs>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GProfileData_profile_devices)]),
          () => new ListBuilder<GProfileData_profile_devices>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GRandomPlaylistData_randomPlaylist_songs)]),
          () => new ListBuilder<GRandomPlaylistData_randomPlaylist_songs>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GSearchData_search_albums_albums)]),
          () => new ListBuilder<GSearchData_search_albums_albums>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GSearchData_search_artists_artists)]),
          () => new ListBuilder<GSearchData_search_artists_artists>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(GSearchData_search_songs_songs)]),
          () => new ListBuilder<GSearchData_search_songs_songs>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(GSongsData_songs_songs)]),
          () => new ListBuilder<GSongsData_songs_songs>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
