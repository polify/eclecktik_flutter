// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:eclektik/graphql/song_item_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/song_item_fragment.var.gql.dart' as _i3;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;

part 'song_item_fragment.req.gql.g.dart';

abstract class GSongItemReq
    implements
        Built<GSongItemReq, GSongItemReqBuilder>,
        _i1.FragmentRequest<_i2.GSongItemData, _i3.GSongItemVars> {
  GSongItemReq._();

  factory GSongItemReq([Function(GSongItemReqBuilder b) updates]) =
      _$GSongItemReq;

  static void _initializeBuilder(GSongItemReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'SongItem';
  _i3.GSongItemVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GSongItemData? parseData(Map<String, dynamic> json) =>
      _i2.GSongItemData.fromJson(json);
  static Serializer<GSongItemReq> get serializer => _$gSongItemReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GSongItemReq.serializer, this)
          as Map<String, dynamic>);
  static GSongItemReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GSongItemReq.serializer, json);
}
