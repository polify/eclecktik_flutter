// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i2;
import 'package:eclektik/graphql/serializers.gql.dart' as _i3;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart' as _i1;

part 'song_card_fragment.data.gql.g.dart';

abstract class GSongCard implements _i1.GSongItem {
  String get G__typename;
  String get s3Key;
  String get duration;
  String get id;
  String get title;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  GSongCard_signedUrl? get signedUrl;
  GSongCard_album? get album;
  GSongCard_artist? get artist;
  Map<String, dynamic> toJson();
}

abstract class GSongCard_signedUrl {
  String get G__typename;
  String get id;
  String get signedUrl;
  Map<String, dynamic> toJson();
}

abstract class GSongCard_album {
  String get G__typename;
  String get id;
  String get name;
  Map<String, dynamic> toJson();
}

abstract class GSongCard_artist {
  String get G__typename;
  String get id;
  String get name;
  Map<String, dynamic> toJson();
}

abstract class GSongCardData
    implements
        Built<GSongCardData, GSongCardDataBuilder>,
        GSongCard,
        _i1.GSongItem {
  GSongCardData._();

  factory GSongCardData([Function(GSongCardDataBuilder b) updates]) =
      _$GSongCardData;

  static void _initializeBuilder(GSongCardDataBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  String get duration;
  String get id;
  String get title;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  GSongCardData_signedUrl? get signedUrl;
  GSongCardData_album? get album;
  GSongCardData_artist? get artist;
  static Serializer<GSongCardData> get serializer => _$gSongCardDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GSongCardData.serializer, this)
          as Map<String, dynamic>);
  static GSongCardData? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GSongCardData.serializer, json);
}

abstract class GSongCardData_signedUrl
    implements
        Built<GSongCardData_signedUrl, GSongCardData_signedUrlBuilder>,
        GSongCard_signedUrl {
  GSongCardData_signedUrl._();

  factory GSongCardData_signedUrl(
          [Function(GSongCardData_signedUrlBuilder b) updates]) =
      _$GSongCardData_signedUrl;

  static void _initializeBuilder(GSongCardData_signedUrlBuilder b) =>
      b..G__typename = 'S3SignedUrl';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get signedUrl;
  static Serializer<GSongCardData_signedUrl> get serializer =>
      _$gSongCardDataSignedUrlSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GSongCardData_signedUrl.serializer, this)
          as Map<String, dynamic>);
  static GSongCardData_signedUrl? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GSongCardData_signedUrl.serializer, json);
}

abstract class GSongCardData_album
    implements
        Built<GSongCardData_album, GSongCardData_albumBuilder>,
        GSongCard_album {
  GSongCardData_album._();

  factory GSongCardData_album(
      [Function(GSongCardData_albumBuilder b) updates]) = _$GSongCardData_album;

  static void _initializeBuilder(GSongCardData_albumBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GSongCardData_album> get serializer =>
      _$gSongCardDataAlbumSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GSongCardData_album.serializer, this)
          as Map<String, dynamic>);
  static GSongCardData_album? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GSongCardData_album.serializer, json);
}

abstract class GSongCardData_artist
    implements
        Built<GSongCardData_artist, GSongCardData_artistBuilder>,
        GSongCard_artist {
  GSongCardData_artist._();

  factory GSongCardData_artist(
          [Function(GSongCardData_artistBuilder b) updates]) =
      _$GSongCardData_artist;

  static void _initializeBuilder(GSongCardData_artistBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GSongCardData_artist> get serializer =>
      _$gSongCardDataArtistSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GSongCardData_artist.serializer, this)
          as Map<String, dynamic>);
  static GSongCardData_artist? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GSongCardData_artist.serializer, json);
}
