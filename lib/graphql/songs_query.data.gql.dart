// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart' as _i2;

part 'songs_query.data.gql.g.dart';

abstract class GSongsData implements Built<GSongsData, GSongsDataBuilder> {
  GSongsData._();

  factory GSongsData([Function(GSongsDataBuilder b) updates]) = _$GSongsData;

  static void _initializeBuilder(GSongsDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GSongsData_songs get songs;
  static Serializer<GSongsData> get serializer => _$gSongsDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongsData.serializer, this)
          as Map<String, dynamic>);
  static GSongsData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongsData.serializer, json);
}

abstract class GSongsData_songs
    implements Built<GSongsData_songs, GSongsData_songsBuilder> {
  GSongsData_songs._();

  factory GSongsData_songs([Function(GSongsData_songsBuilder b) updates]) =
      _$GSongsData_songs;

  static void _initializeBuilder(GSongsData_songsBuilder b) =>
      b..G__typename = 'SongResults';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int? get total;
  BuiltList<GSongsData_songs_songs> get songs;
  static Serializer<GSongsData_songs> get serializer =>
      _$gSongsDataSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongsData_songs.serializer, this)
          as Map<String, dynamic>);
  static GSongsData_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongsData_songs.serializer, json);
}

abstract class GSongsData_songs_songs
    implements
        Built<GSongsData_songs_songs, GSongsData_songs_songsBuilder>,
        _i2.GSongCard {
  GSongsData_songs_songs._();

  factory GSongsData_songs_songs(
          [Function(GSongsData_songs_songsBuilder b) updates]) =
      _$GSongsData_songs_songs;

  static void _initializeBuilder(GSongsData_songs_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  String get duration;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  GSongsData_songs_songs_signedUrl? get signedUrl;
  GSongsData_songs_songs_album? get album;
  GSongsData_songs_songs_artist? get artist;
  static Serializer<GSongsData_songs_songs> get serializer =>
      _$gSongsDataSongsSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongsData_songs_songs.serializer, this)
          as Map<String, dynamic>);
  static GSongsData_songs_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongsData_songs_songs.serializer, json);
}

abstract class GSongsData_songs_songs_signedUrl
    implements
        Built<GSongsData_songs_songs_signedUrl,
            GSongsData_songs_songs_signedUrlBuilder>,
        _i2.GSongCard_signedUrl {
  GSongsData_songs_songs_signedUrl._();

  factory GSongsData_songs_songs_signedUrl(
          [Function(GSongsData_songs_songs_signedUrlBuilder b) updates]) =
      _$GSongsData_songs_songs_signedUrl;

  static void _initializeBuilder(GSongsData_songs_songs_signedUrlBuilder b) =>
      b..G__typename = 'S3SignedUrl';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get signedUrl;
  static Serializer<GSongsData_songs_songs_signedUrl> get serializer =>
      _$gSongsDataSongsSongsSignedUrlSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GSongsData_songs_songs_signedUrl.serializer, this)
      as Map<String, dynamic>);
  static GSongsData_songs_songs_signedUrl? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSongsData_songs_songs_signedUrl.serializer, json);
}

abstract class GSongsData_songs_songs_album
    implements
        Built<GSongsData_songs_songs_album,
            GSongsData_songs_songs_albumBuilder>,
        _i2.GSongCard_album {
  GSongsData_songs_songs_album._();

  factory GSongsData_songs_songs_album(
          [Function(GSongsData_songs_songs_albumBuilder b) updates]) =
      _$GSongsData_songs_songs_album;

  static void _initializeBuilder(GSongsData_songs_songs_albumBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GSongsData_songs_songs_album> get serializer =>
      _$gSongsDataSongsSongsAlbumSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GSongsData_songs_songs_album.serializer, this) as Map<String, dynamic>);
  static GSongsData_songs_songs_album? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSongsData_songs_songs_album.serializer, json);
}

abstract class GSongsData_songs_songs_artist
    implements
        Built<GSongsData_songs_songs_artist,
            GSongsData_songs_songs_artistBuilder>,
        _i2.GSongCard_artist {
  GSongsData_songs_songs_artist._();

  factory GSongsData_songs_songs_artist(
          [Function(GSongsData_songs_songs_artistBuilder b) updates]) =
      _$GSongsData_songs_songs_artist;

  static void _initializeBuilder(GSongsData_songs_songs_artistBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GSongsData_songs_songs_artist> get serializer =>
      _$gSongsDataSongsSongsArtistSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GSongsData_songs_songs_artist.serializer, this) as Map<String, dynamic>);
  static GSongsData_songs_songs_artist? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSongsData_songs_songs_artist.serializer, json);
}
