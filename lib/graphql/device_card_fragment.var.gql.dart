// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'device_card_fragment.var.gql.g.dart';

abstract class GDeviceItemVars
    implements Built<GDeviceItemVars, GDeviceItemVarsBuilder> {
  GDeviceItemVars._();

  factory GDeviceItemVars([Function(GDeviceItemVarsBuilder b) updates]) =
      _$GDeviceItemVars;

  static Serializer<GDeviceItemVars> get serializer =>
      _$gDeviceItemVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GDeviceItemVars.serializer, this)
          as Map<String, dynamic>);
  static GDeviceItemVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GDeviceItemVars.serializer, json);
}
