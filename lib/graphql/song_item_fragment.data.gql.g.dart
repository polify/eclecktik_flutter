// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song_item_fragment.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongItemData> _$gSongItemDataSerializer =
    new _$GSongItemDataSerializer();

class _$GSongItemDataSerializer implements StructuredSerializer<GSongItemData> {
  @override
  final Iterable<Type> types = const [GSongItemData, _$GSongItemData];
  @override
  final String wireName = 'GSongItemData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongItemData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i1.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i1.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GSongItemData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongItemDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i1.GTime))! as _i1.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i1.GTime))! as _i1.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongItemData extends GSongItemData {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i1.GTime createdAt;
  @override
  final _i1.GTime updatedAt;

  factory _$GSongItemData([void Function(GSongItemDataBuilder)? updates]) =>
      (new GSongItemDataBuilder()..update(updates)).build();

  _$GSongItemData._(
      {required this.G__typename,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongItemData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongItemData', 'id');
    BuiltValueNullFieldError.checkNotNull(title, 'GSongItemData', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSongItemData', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSongItemData', 'updatedAt');
  }

  @override
  GSongItemData rebuild(void Function(GSongItemDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongItemDataBuilder toBuilder() => new GSongItemDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongItemData &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    title.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongItemData')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GSongItemDataBuilder
    implements Builder<GSongItemData, GSongItemDataBuilder> {
  _$GSongItemData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i1.GTimeBuilder? _createdAt;
  _i1.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i1.GTimeBuilder();
  set createdAt(_i1.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i1.GTimeBuilder? _updatedAt;
  _i1.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i1.GTimeBuilder();
  set updatedAt(_i1.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSongItemDataBuilder() {
    GSongItemData._initializeBuilder(this);
  }

  GSongItemDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongItemData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongItemData;
  }

  @override
  void update(void Function(GSongItemDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongItemData build() {
    _$GSongItemData _$result;
    try {
      _$result = _$v ??
          new _$GSongItemData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongItemData', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSongItemData', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GSongItemData', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongItemData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
