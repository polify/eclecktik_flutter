// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schema.schema.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GItemsInput> _$gItemsInputSerializer = new _$GItemsInputSerializer();
Serializer<GNewUser> _$gNewUserSerializer = new _$GNewUserSerializer();
Serializer<GPaginationInput> _$gPaginationInputSerializer =
    new _$GPaginationInputSerializer();
Serializer<GS3CredentialCreateInput> _$gS3CredentialCreateInputSerializer =
    new _$GS3CredentialCreateInputSerializer();
Serializer<GS3CredentialUpdateInput> _$gS3CredentialUpdateInputSerializer =
    new _$GS3CredentialUpdateInputSerializer();
Serializer<GSearchSongInput> _$gSearchSongInputSerializer =
    new _$GSearchSongInputSerializer();

class _$GItemsInputSerializer implements StructuredSerializer<GItemsInput> {
  @override
  final Iterable<Type> types = const [GItemsInput, _$GItemsInput];
  @override
  final String wireName = 'GItemsInput';

  @override
  Iterable<Object?> serialize(Serializers serializers, GItemsInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.ids;
    if (value != null) {
      result
        ..add('ids')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.collectionName;
    if (value != null) {
      result
        ..add('collectionName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GItemsInput deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GItemsInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'ids':
          result.ids.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'collectionName':
          result.collectionName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GNewUserSerializer implements StructuredSerializer<GNewUser> {
  @override
  final Iterable<Type> types = const [GNewUser, _$GNewUser];
  @override
  final String wireName = 'GNewUser';

  @override
  Iterable<Object?> serialize(Serializers serializers, GNewUser object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'password',
      serializers.serialize(object.password,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GNewUser deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GNewUserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'password':
          result.password = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GPaginationInputSerializer
    implements StructuredSerializer<GPaginationInput> {
  @override
  final Iterable<Type> types = const [GPaginationInput, _$GPaginationInput];
  @override
  final String wireName = 'GPaginationInput';

  @override
  Iterable<Object?> serialize(Serializers serializers, GPaginationInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.count;
    if (value != null) {
      result
        ..add('count')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.page;
    if (value != null) {
      result
        ..add('page')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GPaginationInput deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GPaginationInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'page':
          result.page = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$GS3CredentialCreateInputSerializer
    implements StructuredSerializer<GS3CredentialCreateInput> {
  @override
  final Iterable<Type> types = const [
    GS3CredentialCreateInput,
    _$GS3CredentialCreateInput
  ];
  @override
  final String wireName = 'GS3CredentialCreateInput';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GS3CredentialCreateInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'keyId',
      serializers.serialize(object.keyId,
          specifiedType: const FullType(String)),
      'keySecret',
      serializers.serialize(object.keySecret,
          specifiedType: const FullType(String)),
      'bucketHost',
      serializers.serialize(object.bucketHost,
          specifiedType: const FullType(String)),
      'bucketName',
      serializers.serialize(object.bucketName,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.clientRegion;
    if (value != null) {
      result
        ..add('clientRegion')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bucketRegion;
    if (value != null) {
      result
        ..add('bucketRegion')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GS3CredentialCreateInput deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GS3CredentialCreateInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'keyId':
          result.keyId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'keySecret':
          result.keySecret = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'bucketHost':
          result.bucketHost = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'bucketName':
          result.bucketName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'clientRegion':
          result.clientRegion = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bucketRegion':
          result.bucketRegion = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GS3CredentialUpdateInputSerializer
    implements StructuredSerializer<GS3CredentialUpdateInput> {
  @override
  final Iterable<Type> types = const [
    GS3CredentialUpdateInput,
    _$GS3CredentialUpdateInput
  ];
  @override
  final String wireName = 'GS3CredentialUpdateInput';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GS3CredentialUpdateInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.keyId;
    if (value != null) {
      result
        ..add('keyId')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.keySecret;
    if (value != null) {
      result
        ..add('keySecret')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bucketHost;
    if (value != null) {
      result
        ..add('bucketHost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bucketName;
    if (value != null) {
      result
        ..add('bucketName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GS3CredentialUpdateInput deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GS3CredentialUpdateInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'keyId':
          result.keyId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'keySecret':
          result.keySecret = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bucketHost':
          result.bucketHost = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bucketName':
          result.bucketName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchSongInputSerializer
    implements StructuredSerializer<GSearchSongInput> {
  @override
  final Iterable<Type> types = const [GSearchSongInput, _$GSearchSongInput];
  @override
  final String wireName = 'GSearchSongInput';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSearchSongInput object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.artistName;
    if (value != null) {
      result
        ..add('artistName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.albumName;
    if (value != null) {
      result
        ..add('albumName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.songName;
    if (value != null) {
      result
        ..add('songName')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.random;
    if (value != null) {
      result
        ..add('random')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  GSearchSongInput deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchSongInputBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'artistName':
          result.artistName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'albumName':
          result.albumName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'songName':
          result.songName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'random':
          result.random = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
      }
    }

    return result.build();
  }
}

class _$GItemsInput extends GItemsInput {
  @override
  final BuiltList<String>? ids;
  @override
  final String? collectionName;

  factory _$GItemsInput([void Function(GItemsInputBuilder)? updates]) =>
      (new GItemsInputBuilder()..update(updates)).build();

  _$GItemsInput._({this.ids, this.collectionName}) : super._();

  @override
  GItemsInput rebuild(void Function(GItemsInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GItemsInputBuilder toBuilder() => new GItemsInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GItemsInput &&
        ids == other.ids &&
        collectionName == other.collectionName;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, ids.hashCode), collectionName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GItemsInput')
          ..add('ids', ids)
          ..add('collectionName', collectionName))
        .toString();
  }
}

class GItemsInputBuilder implements Builder<GItemsInput, GItemsInputBuilder> {
  _$GItemsInput? _$v;

  ListBuilder<String>? _ids;
  ListBuilder<String> get ids => _$this._ids ??= new ListBuilder<String>();
  set ids(ListBuilder<String>? ids) => _$this._ids = ids;

  String? _collectionName;
  String? get collectionName => _$this._collectionName;
  set collectionName(String? collectionName) =>
      _$this._collectionName = collectionName;

  GItemsInputBuilder();

  GItemsInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _ids = $v.ids?.toBuilder();
      _collectionName = $v.collectionName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GItemsInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GItemsInput;
  }

  @override
  void update(void Function(GItemsInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GItemsInput build() {
    _$GItemsInput _$result;
    try {
      _$result = _$v ??
          new _$GItemsInput._(
              ids: _ids?.build(), collectionName: collectionName);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'ids';
        _ids?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GItemsInput', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GNewUser extends GNewUser {
  @override
  final String name;
  @override
  final String password;

  factory _$GNewUser([void Function(GNewUserBuilder)? updates]) =>
      (new GNewUserBuilder()..update(updates)).build();

  _$GNewUser._({required this.name, required this.password}) : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'GNewUser', 'name');
    BuiltValueNullFieldError.checkNotNull(password, 'GNewUser', 'password');
  }

  @override
  GNewUser rebuild(void Function(GNewUserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GNewUserBuilder toBuilder() => new GNewUserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GNewUser &&
        name == other.name &&
        password == other.password;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), password.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GNewUser')
          ..add('name', name)
          ..add('password', password))
        .toString();
  }
}

class GNewUserBuilder implements Builder<GNewUser, GNewUserBuilder> {
  _$GNewUser? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _password;
  String? get password => _$this._password;
  set password(String? password) => _$this._password = password;

  GNewUserBuilder();

  GNewUserBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _password = $v.password;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GNewUser other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GNewUser;
  }

  @override
  void update(void Function(GNewUserBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GNewUser build() {
    final _$result = _$v ??
        new _$GNewUser._(
            name:
                BuiltValueNullFieldError.checkNotNull(name, 'GNewUser', 'name'),
            password: BuiltValueNullFieldError.checkNotNull(
                password, 'GNewUser', 'password'));
    replace(_$result);
    return _$result;
  }
}

class _$GPaginationInput extends GPaginationInput {
  @override
  final int? count;
  @override
  final int? page;

  factory _$GPaginationInput(
          [void Function(GPaginationInputBuilder)? updates]) =>
      (new GPaginationInputBuilder()..update(updates)).build();

  _$GPaginationInput._({this.count, this.page}) : super._();

  @override
  GPaginationInput rebuild(void Function(GPaginationInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GPaginationInputBuilder toBuilder() =>
      new GPaginationInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GPaginationInput &&
        count == other.count &&
        page == other.page;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, count.hashCode), page.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GPaginationInput')
          ..add('count', count)
          ..add('page', page))
        .toString();
  }
}

class GPaginationInputBuilder
    implements Builder<GPaginationInput, GPaginationInputBuilder> {
  _$GPaginationInput? _$v;

  int? _count;
  int? get count => _$this._count;
  set count(int? count) => _$this._count = count;

  int? _page;
  int? get page => _$this._page;
  set page(int? page) => _$this._page = page;

  GPaginationInputBuilder();

  GPaginationInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _count = $v.count;
      _page = $v.page;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GPaginationInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GPaginationInput;
  }

  @override
  void update(void Function(GPaginationInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GPaginationInput build() {
    final _$result = _$v ?? new _$GPaginationInput._(count: count, page: page);
    replace(_$result);
    return _$result;
  }
}

class _$GS3CredentialCreateInput extends GS3CredentialCreateInput {
  @override
  final String keyId;
  @override
  final String keySecret;
  @override
  final String bucketHost;
  @override
  final String bucketName;
  @override
  final String? clientRegion;
  @override
  final String? bucketRegion;

  factory _$GS3CredentialCreateInput(
          [void Function(GS3CredentialCreateInputBuilder)? updates]) =>
      (new GS3CredentialCreateInputBuilder()..update(updates)).build();

  _$GS3CredentialCreateInput._(
      {required this.keyId,
      required this.keySecret,
      required this.bucketHost,
      required this.bucketName,
      this.clientRegion,
      this.bucketRegion})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        keyId, 'GS3CredentialCreateInput', 'keyId');
    BuiltValueNullFieldError.checkNotNull(
        keySecret, 'GS3CredentialCreateInput', 'keySecret');
    BuiltValueNullFieldError.checkNotNull(
        bucketHost, 'GS3CredentialCreateInput', 'bucketHost');
    BuiltValueNullFieldError.checkNotNull(
        bucketName, 'GS3CredentialCreateInput', 'bucketName');
  }

  @override
  GS3CredentialCreateInput rebuild(
          void Function(GS3CredentialCreateInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GS3CredentialCreateInputBuilder toBuilder() =>
      new GS3CredentialCreateInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GS3CredentialCreateInput &&
        keyId == other.keyId &&
        keySecret == other.keySecret &&
        bucketHost == other.bucketHost &&
        bucketName == other.bucketName &&
        clientRegion == other.clientRegion &&
        bucketRegion == other.bucketRegion;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, keyId.hashCode), keySecret.hashCode),
                    bucketHost.hashCode),
                bucketName.hashCode),
            clientRegion.hashCode),
        bucketRegion.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GS3CredentialCreateInput')
          ..add('keyId', keyId)
          ..add('keySecret', keySecret)
          ..add('bucketHost', bucketHost)
          ..add('bucketName', bucketName)
          ..add('clientRegion', clientRegion)
          ..add('bucketRegion', bucketRegion))
        .toString();
  }
}

class GS3CredentialCreateInputBuilder
    implements
        Builder<GS3CredentialCreateInput, GS3CredentialCreateInputBuilder> {
  _$GS3CredentialCreateInput? _$v;

  String? _keyId;
  String? get keyId => _$this._keyId;
  set keyId(String? keyId) => _$this._keyId = keyId;

  String? _keySecret;
  String? get keySecret => _$this._keySecret;
  set keySecret(String? keySecret) => _$this._keySecret = keySecret;

  String? _bucketHost;
  String? get bucketHost => _$this._bucketHost;
  set bucketHost(String? bucketHost) => _$this._bucketHost = bucketHost;

  String? _bucketName;
  String? get bucketName => _$this._bucketName;
  set bucketName(String? bucketName) => _$this._bucketName = bucketName;

  String? _clientRegion;
  String? get clientRegion => _$this._clientRegion;
  set clientRegion(String? clientRegion) => _$this._clientRegion = clientRegion;

  String? _bucketRegion;
  String? get bucketRegion => _$this._bucketRegion;
  set bucketRegion(String? bucketRegion) => _$this._bucketRegion = bucketRegion;

  GS3CredentialCreateInputBuilder();

  GS3CredentialCreateInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _keyId = $v.keyId;
      _keySecret = $v.keySecret;
      _bucketHost = $v.bucketHost;
      _bucketName = $v.bucketName;
      _clientRegion = $v.clientRegion;
      _bucketRegion = $v.bucketRegion;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GS3CredentialCreateInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GS3CredentialCreateInput;
  }

  @override
  void update(void Function(GS3CredentialCreateInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GS3CredentialCreateInput build() {
    final _$result = _$v ??
        new _$GS3CredentialCreateInput._(
            keyId: BuiltValueNullFieldError.checkNotNull(
                keyId, 'GS3CredentialCreateInput', 'keyId'),
            keySecret: BuiltValueNullFieldError.checkNotNull(
                keySecret, 'GS3CredentialCreateInput', 'keySecret'),
            bucketHost: BuiltValueNullFieldError.checkNotNull(
                bucketHost, 'GS3CredentialCreateInput', 'bucketHost'),
            bucketName: BuiltValueNullFieldError.checkNotNull(
                bucketName, 'GS3CredentialCreateInput', 'bucketName'),
            clientRegion: clientRegion,
            bucketRegion: bucketRegion);
    replace(_$result);
    return _$result;
  }
}

class _$GS3CredentialUpdateInput extends GS3CredentialUpdateInput {
  @override
  final String? keyId;
  @override
  final String? keySecret;
  @override
  final String? bucketHost;
  @override
  final String? bucketName;

  factory _$GS3CredentialUpdateInput(
          [void Function(GS3CredentialUpdateInputBuilder)? updates]) =>
      (new GS3CredentialUpdateInputBuilder()..update(updates)).build();

  _$GS3CredentialUpdateInput._(
      {this.keyId, this.keySecret, this.bucketHost, this.bucketName})
      : super._();

  @override
  GS3CredentialUpdateInput rebuild(
          void Function(GS3CredentialUpdateInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GS3CredentialUpdateInputBuilder toBuilder() =>
      new GS3CredentialUpdateInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GS3CredentialUpdateInput &&
        keyId == other.keyId &&
        keySecret == other.keySecret &&
        bucketHost == other.bucketHost &&
        bucketName == other.bucketName;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, keyId.hashCode), keySecret.hashCode),
            bucketHost.hashCode),
        bucketName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GS3CredentialUpdateInput')
          ..add('keyId', keyId)
          ..add('keySecret', keySecret)
          ..add('bucketHost', bucketHost)
          ..add('bucketName', bucketName))
        .toString();
  }
}

class GS3CredentialUpdateInputBuilder
    implements
        Builder<GS3CredentialUpdateInput, GS3CredentialUpdateInputBuilder> {
  _$GS3CredentialUpdateInput? _$v;

  String? _keyId;
  String? get keyId => _$this._keyId;
  set keyId(String? keyId) => _$this._keyId = keyId;

  String? _keySecret;
  String? get keySecret => _$this._keySecret;
  set keySecret(String? keySecret) => _$this._keySecret = keySecret;

  String? _bucketHost;
  String? get bucketHost => _$this._bucketHost;
  set bucketHost(String? bucketHost) => _$this._bucketHost = bucketHost;

  String? _bucketName;
  String? get bucketName => _$this._bucketName;
  set bucketName(String? bucketName) => _$this._bucketName = bucketName;

  GS3CredentialUpdateInputBuilder();

  GS3CredentialUpdateInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _keyId = $v.keyId;
      _keySecret = $v.keySecret;
      _bucketHost = $v.bucketHost;
      _bucketName = $v.bucketName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GS3CredentialUpdateInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GS3CredentialUpdateInput;
  }

  @override
  void update(void Function(GS3CredentialUpdateInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GS3CredentialUpdateInput build() {
    final _$result = _$v ??
        new _$GS3CredentialUpdateInput._(
            keyId: keyId,
            keySecret: keySecret,
            bucketHost: bucketHost,
            bucketName: bucketName);
    replace(_$result);
    return _$result;
  }
}

class _$GSearchSongInput extends GSearchSongInput {
  @override
  final String? artistName;
  @override
  final String? albumName;
  @override
  final String? songName;
  @override
  final bool? random;

  factory _$GSearchSongInput(
          [void Function(GSearchSongInputBuilder)? updates]) =>
      (new GSearchSongInputBuilder()..update(updates)).build();

  _$GSearchSongInput._(
      {this.artistName, this.albumName, this.songName, this.random})
      : super._();

  @override
  GSearchSongInput rebuild(void Function(GSearchSongInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchSongInputBuilder toBuilder() =>
      new GSearchSongInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchSongInput &&
        artistName == other.artistName &&
        albumName == other.albumName &&
        songName == other.songName &&
        random == other.random;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, artistName.hashCode), albumName.hashCode),
            songName.hashCode),
        random.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchSongInput')
          ..add('artistName', artistName)
          ..add('albumName', albumName)
          ..add('songName', songName)
          ..add('random', random))
        .toString();
  }
}

class GSearchSongInputBuilder
    implements Builder<GSearchSongInput, GSearchSongInputBuilder> {
  _$GSearchSongInput? _$v;

  String? _artistName;
  String? get artistName => _$this._artistName;
  set artistName(String? artistName) => _$this._artistName = artistName;

  String? _albumName;
  String? get albumName => _$this._albumName;
  set albumName(String? albumName) => _$this._albumName = albumName;

  String? _songName;
  String? get songName => _$this._songName;
  set songName(String? songName) => _$this._songName = songName;

  bool? _random;
  bool? get random => _$this._random;
  set random(bool? random) => _$this._random = random;

  GSearchSongInputBuilder();

  GSearchSongInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _artistName = $v.artistName;
      _albumName = $v.albumName;
      _songName = $v.songName;
      _random = $v.random;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchSongInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchSongInput;
  }

  @override
  void update(void Function(GSearchSongInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchSongInput build() {
    final _$result = _$v ??
        new _$GSearchSongInput._(
            artistName: artistName,
            albumName: albumName,
            songName: songName,
            random: random);
    replace(_$result);
    return _$result;
  }
}

class _$GTime extends GTime {
  @override
  final String value;

  factory _$GTime([void Function(GTimeBuilder)? updates]) =>
      (new GTimeBuilder()..update(updates)).build();

  _$GTime._({required this.value}) : super._() {
    BuiltValueNullFieldError.checkNotNull(value, 'GTime', 'value');
  }

  @override
  GTime rebuild(void Function(GTimeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GTimeBuilder toBuilder() => new GTimeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GTime && value == other.value;
  }

  @override
  int get hashCode {
    return $jf($jc(0, value.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GTime')..add('value', value))
        .toString();
  }
}

class GTimeBuilder implements Builder<GTime, GTimeBuilder> {
  _$GTime? _$v;

  String? _value;
  String? get value => _$this._value;
  set value(String? value) => _$this._value = value;

  GTimeBuilder();

  GTimeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GTime other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GTime;
  }

  @override
  void update(void Function(GTimeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GTime build() {
    final _$result = _$v ??
        new _$GTime._(
            value:
                BuiltValueNullFieldError.checkNotNull(value, 'GTime', 'value'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
