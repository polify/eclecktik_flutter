// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artist_card_fragment.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GArtistCardData> _$gArtistCardDataSerializer =
    new _$GArtistCardDataSerializer();
Serializer<GArtistCardData_albums> _$gArtistCardDataAlbumsSerializer =
    new _$GArtistCardData_albumsSerializer();
Serializer<GArtistCardData_songs> _$gArtistCardDataSongsSerializer =
    new _$GArtistCardData_songsSerializer();

class _$GArtistCardDataSerializer
    implements StructuredSerializer<GArtistCardData> {
  @override
  final Iterable<Type> types = const [GArtistCardData, _$GArtistCardData];
  @override
  final String wireName = 'GArtistCardData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GArtistCardData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'albums',
      serializers.serialize(object.albums,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistCardData_albums)])),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GArtistCardData_songs)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistCardData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistCardDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GArtistCardData_albums)
              ]))! as BuiltList<Object?>);
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GArtistCardData_songs)
              ]))! as BuiltList<Object?>);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistCardData_albumsSerializer
    implements StructuredSerializer<GArtistCardData_albums> {
  @override
  final Iterable<Type> types = const [
    GArtistCardData_albums,
    _$GArtistCardData_albums
  ];
  @override
  final String wireName = 'GArtistCardData_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistCardData_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistCardData_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistCardData_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistCardData_songsSerializer
    implements StructuredSerializer<GArtistCardData_songs> {
  @override
  final Iterable<Type> types = const [
    GArtistCardData_songs,
    _$GArtistCardData_songs
  ];
  @override
  final String wireName = 'GArtistCardData_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GArtistCardData_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GArtistCardData_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GArtistCardData_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GArtistCardData extends GArtistCardData {
  @override
  final String G__typename;
  @override
  final BuiltList<GArtistCardData_albums> albums;
  @override
  final BuiltList<GArtistCardData_songs> songs;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;

  factory _$GArtistCardData([void Function(GArtistCardDataBuilder)? updates]) =>
      (new GArtistCardDataBuilder()..update(updates)).build();

  _$GArtistCardData._(
      {required this.G__typename,
      required this.albums,
      required this.songs,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistCardData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(albums, 'GArtistCardData', 'albums');
    BuiltValueNullFieldError.checkNotNull(songs, 'GArtistCardData', 'songs');
    BuiltValueNullFieldError.checkNotNull(id, 'GArtistCardData', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GArtistCardData', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistCardData', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistCardData', 'updatedAt');
  }

  @override
  GArtistCardData rebuild(void Function(GArtistCardDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistCardDataBuilder toBuilder() =>
      new GArtistCardDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistCardData &&
        G__typename == other.G__typename &&
        albums == other.albums &&
        songs == other.songs &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, G__typename.hashCode), albums.hashCode),
                            songs.hashCode),
                        id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistCardData')
          ..add('G__typename', G__typename)
          ..add('albums', albums)
          ..add('songs', songs)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistCardDataBuilder
    implements Builder<GArtistCardData, GArtistCardDataBuilder> {
  _$GArtistCardData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  ListBuilder<GArtistCardData_albums>? _albums;
  ListBuilder<GArtistCardData_albums> get albums =>
      _$this._albums ??= new ListBuilder<GArtistCardData_albums>();
  set albums(ListBuilder<GArtistCardData_albums>? albums) =>
      _$this._albums = albums;

  ListBuilder<GArtistCardData_songs>? _songs;
  ListBuilder<GArtistCardData_songs> get songs =>
      _$this._songs ??= new ListBuilder<GArtistCardData_songs>();
  set songs(ListBuilder<GArtistCardData_songs>? songs) => _$this._songs = songs;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistCardDataBuilder() {
    GArtistCardData._initializeBuilder(this);
  }

  GArtistCardDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _albums = $v.albums.toBuilder();
      _songs = $v.songs.toBuilder();
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistCardData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistCardData;
  }

  @override
  void update(void Function(GArtistCardDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistCardData build() {
    _$GArtistCardData _$result;
    try {
      _$result = _$v ??
          new _$GArtistCardData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistCardData', 'G__typename'),
              albums: albums.build(),
              songs: songs.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistCardData', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GArtistCardData', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
        _$failedField = 'songs';
        songs.build();

        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistCardData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistCardData_albums extends GArtistCardData_albums {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;

  factory _$GArtistCardData_albums(
          [void Function(GArtistCardData_albumsBuilder)? updates]) =>
      (new GArtistCardData_albumsBuilder()..update(updates)).build();

  _$GArtistCardData_albums._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistCardData_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GArtistCardData_albums', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GArtistCardData_albums', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistCardData_albums', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistCardData_albums', 'updatedAt');
  }

  @override
  GArtistCardData_albums rebuild(
          void Function(GArtistCardData_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistCardData_albumsBuilder toBuilder() =>
      new GArtistCardData_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistCardData_albums &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistCardData_albums')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistCardData_albumsBuilder
    implements Builder<GArtistCardData_albums, GArtistCardData_albumsBuilder> {
  _$GArtistCardData_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistCardData_albumsBuilder() {
    GArtistCardData_albums._initializeBuilder(this);
  }

  GArtistCardData_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistCardData_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistCardData_albums;
  }

  @override
  void update(void Function(GArtistCardData_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistCardData_albums build() {
    _$GArtistCardData_albums _$result;
    try {
      _$result = _$v ??
          new _$GArtistCardData_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistCardData_albums', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistCardData_albums', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GArtistCardData_albums', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistCardData_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GArtistCardData_songs extends GArtistCardData_songs {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;

  factory _$GArtistCardData_songs(
          [void Function(GArtistCardData_songsBuilder)? updates]) =>
      (new GArtistCardData_songsBuilder()..update(updates)).build();

  _$GArtistCardData_songs._(
      {required this.G__typename,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GArtistCardData_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GArtistCardData_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GArtistCardData_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GArtistCardData_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GArtistCardData_songs', 'updatedAt');
  }

  @override
  GArtistCardData_songs rebuild(
          void Function(GArtistCardData_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistCardData_songsBuilder toBuilder() =>
      new GArtistCardData_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistCardData_songs &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    title.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GArtistCardData_songs')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GArtistCardData_songsBuilder
    implements Builder<GArtistCardData_songs, GArtistCardData_songsBuilder> {
  _$GArtistCardData_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GArtistCardData_songsBuilder() {
    GArtistCardData_songs._initializeBuilder(this);
  }

  GArtistCardData_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GArtistCardData_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistCardData_songs;
  }

  @override
  void update(void Function(GArtistCardData_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistCardData_songs build() {
    _$GArtistCardData_songs _$result;
    try {
      _$result = _$v ??
          new _$GArtistCardData_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GArtistCardData_songs', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GArtistCardData_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GArtistCardData_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GArtistCardData_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
