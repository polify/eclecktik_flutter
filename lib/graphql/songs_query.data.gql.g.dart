// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'songs_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongsData> _$gSongsDataSerializer = new _$GSongsDataSerializer();
Serializer<GSongsData_songs> _$gSongsDataSongsSerializer =
    new _$GSongsData_songsSerializer();
Serializer<GSongsData_songs_songs> _$gSongsDataSongsSongsSerializer =
    new _$GSongsData_songs_songsSerializer();
Serializer<GSongsData_songs_songs_signedUrl>
    _$gSongsDataSongsSongsSignedUrlSerializer =
    new _$GSongsData_songs_songs_signedUrlSerializer();
Serializer<GSongsData_songs_songs_album> _$gSongsDataSongsSongsAlbumSerializer =
    new _$GSongsData_songs_songs_albumSerializer();
Serializer<GSongsData_songs_songs_artist>
    _$gSongsDataSongsSongsArtistSerializer =
    new _$GSongsData_songs_songs_artistSerializer();

class _$GSongsDataSerializer implements StructuredSerializer<GSongsData> {
  @override
  final Iterable<Type> types = const [GSongsData, _$GSongsData];
  @override
  final String wireName = 'GSongsData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongsData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(GSongsData_songs)),
    ];

    return result;
  }

  @override
  GSongsData deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongsDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongsData_songs))!
              as GSongsData_songs);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongsData_songsSerializer
    implements StructuredSerializer<GSongsData_songs> {
  @override
  final Iterable<Type> types = const [GSongsData_songs, _$GSongsData_songs];
  @override
  final String wireName = 'GSongsData_songs';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongsData_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GSongsData_songs_songs)])),
    ];
    Object? value;
    value = object.total;
    if (value != null) {
      result
        ..add('total')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GSongsData_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongsData_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'total':
          result.total = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GSongsData_songs_songs)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongsData_songs_songsSerializer
    implements StructuredSerializer<GSongsData_songs_songs> {
  @override
  final Iterable<Type> types = const [
    GSongsData_songs_songs,
    _$GSongsData_songs_songs
  ];
  @override
  final String wireName = 'GSongsData_songs_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongsData_songs_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.signedUrl;
    if (value != null) {
      result
        ..add('signedUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongsData_songs_songs_signedUrl)));
    }
    value = object.album;
    if (value != null) {
      result
        ..add('album')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongsData_songs_songs_album)));
    }
    value = object.artist;
    if (value != null) {
      result
        ..add('artist')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongsData_songs_songs_artist)));
    }
    return result;
  }

  @override
  GSongsData_songs_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongsData_songs_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'signedUrl':
          result.signedUrl.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GSongsData_songs_songs_signedUrl))!
              as GSongsData_songs_songs_signedUrl);
          break;
        case 'album':
          result.album.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongsData_songs_songs_album))!
              as GSongsData_songs_songs_album);
          break;
        case 'artist':
          result.artist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongsData_songs_songs_artist))!
              as GSongsData_songs_songs_artist);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongsData_songs_songs_signedUrlSerializer
    implements StructuredSerializer<GSongsData_songs_songs_signedUrl> {
  @override
  final Iterable<Type> types = const [
    GSongsData_songs_songs_signedUrl,
    _$GSongsData_songs_songs_signedUrl
  ];
  @override
  final String wireName = 'GSongsData_songs_songs_signedUrl';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongsData_songs_songs_signedUrl object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'signedUrl',
      serializers.serialize(object.signedUrl,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongsData_songs_songs_signedUrl deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongsData_songs_songs_signedUrlBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'signedUrl':
          result.signedUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongsData_songs_songs_albumSerializer
    implements StructuredSerializer<GSongsData_songs_songs_album> {
  @override
  final Iterable<Type> types = const [
    GSongsData_songs_songs_album,
    _$GSongsData_songs_songs_album
  ];
  @override
  final String wireName = 'GSongsData_songs_songs_album';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongsData_songs_songs_album object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongsData_songs_songs_album deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongsData_songs_songs_albumBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongsData_songs_songs_artistSerializer
    implements StructuredSerializer<GSongsData_songs_songs_artist> {
  @override
  final Iterable<Type> types = const [
    GSongsData_songs_songs_artist,
    _$GSongsData_songs_songs_artist
  ];
  @override
  final String wireName = 'GSongsData_songs_songs_artist';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongsData_songs_songs_artist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongsData_songs_songs_artist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongsData_songs_songs_artistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongsData extends GSongsData {
  @override
  final String G__typename;
  @override
  final GSongsData_songs songs;

  factory _$GSongsData([void Function(GSongsDataBuilder)? updates]) =>
      (new GSongsDataBuilder()..update(updates)).build();

  _$GSongsData._({required this.G__typename, required this.songs}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongsData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(songs, 'GSongsData', 'songs');
  }

  @override
  GSongsData rebuild(void Function(GSongsDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongsDataBuilder toBuilder() => new GSongsDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongsData &&
        G__typename == other.G__typename &&
        songs == other.songs;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), songs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongsData')
          ..add('G__typename', G__typename)
          ..add('songs', songs))
        .toString();
  }
}

class GSongsDataBuilder implements Builder<GSongsData, GSongsDataBuilder> {
  _$GSongsData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GSongsData_songsBuilder? _songs;
  GSongsData_songsBuilder get songs =>
      _$this._songs ??= new GSongsData_songsBuilder();
  set songs(GSongsData_songsBuilder? songs) => _$this._songs = songs;

  GSongsDataBuilder() {
    GSongsData._initializeBuilder(this);
  }

  GSongsDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _songs = $v.songs.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongsData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongsData;
  }

  @override
  void update(void Function(GSongsDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongsData build() {
    _$GSongsData _$result;
    try {
      _$result = _$v ??
          new _$GSongsData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongsData', 'G__typename'),
              songs: songs.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'songs';
        songs.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongsData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSongsData_songs extends GSongsData_songs {
  @override
  final String G__typename;
  @override
  final int? total;
  @override
  final BuiltList<GSongsData_songs_songs> songs;

  factory _$GSongsData_songs(
          [void Function(GSongsData_songsBuilder)? updates]) =>
      (new GSongsData_songsBuilder()..update(updates)).build();

  _$GSongsData_songs._(
      {required this.G__typename, this.total, required this.songs})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongsData_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(songs, 'GSongsData_songs', 'songs');
  }

  @override
  GSongsData_songs rebuild(void Function(GSongsData_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongsData_songsBuilder toBuilder() =>
      new GSongsData_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongsData_songs &&
        G__typename == other.G__typename &&
        total == other.total &&
        songs == other.songs;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), total.hashCode), songs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongsData_songs')
          ..add('G__typename', G__typename)
          ..add('total', total)
          ..add('songs', songs))
        .toString();
  }
}

class GSongsData_songsBuilder
    implements Builder<GSongsData_songs, GSongsData_songsBuilder> {
  _$GSongsData_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _total;
  int? get total => _$this._total;
  set total(int? total) => _$this._total = total;

  ListBuilder<GSongsData_songs_songs>? _songs;
  ListBuilder<GSongsData_songs_songs> get songs =>
      _$this._songs ??= new ListBuilder<GSongsData_songs_songs>();
  set songs(ListBuilder<GSongsData_songs_songs>? songs) =>
      _$this._songs = songs;

  GSongsData_songsBuilder() {
    GSongsData_songs._initializeBuilder(this);
  }

  GSongsData_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _total = $v.total;
      _songs = $v.songs.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongsData_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongsData_songs;
  }

  @override
  void update(void Function(GSongsData_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongsData_songs build() {
    _$GSongsData_songs _$result;
    try {
      _$result = _$v ??
          new _$GSongsData_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongsData_songs', 'G__typename'),
              total: total,
              songs: songs.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'songs';
        songs.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongsData_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSongsData_songs_songs extends GSongsData_songs_songs {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final String duration;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;
  @override
  final GSongsData_songs_songs_signedUrl? signedUrl;
  @override
  final GSongsData_songs_songs_album? album;
  @override
  final GSongsData_songs_songs_artist? artist;

  factory _$GSongsData_songs_songs(
          [void Function(GSongsData_songs_songsBuilder)? updates]) =>
      (new GSongsData_songs_songsBuilder()..update(updates)).build();

  _$GSongsData_songs_songs._(
      {required this.G__typename,
      required this.s3Key,
      required this.duration,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt,
      this.signedUrl,
      this.album,
      this.artist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongsData_songs_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        s3Key, 'GSongsData_songs_songs', 's3Key');
    BuiltValueNullFieldError.checkNotNull(
        duration, 'GSongsData_songs_songs', 'duration');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongsData_songs_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GSongsData_songs_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSongsData_songs_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSongsData_songs_songs', 'updatedAt');
  }

  @override
  GSongsData_songs_songs rebuild(
          void Function(GSongsData_songs_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongsData_songs_songsBuilder toBuilder() =>
      new GSongsData_songs_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongsData_songs_songs &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        duration == other.duration &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        signedUrl == other.signedUrl &&
        album == other.album &&
        artist == other.artist;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, G__typename.hashCode),
                                            s3Key.hashCode),
                                        duration.hashCode),
                                    id.hashCode),
                                title.hashCode),
                            coverUrl.hashCode),
                        createdAt.hashCode),
                    updatedAt.hashCode),
                signedUrl.hashCode),
            album.hashCode),
        artist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongsData_songs_songs')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('duration', duration)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('signedUrl', signedUrl)
          ..add('album', album)
          ..add('artist', artist))
        .toString();
  }
}

class GSongsData_songs_songsBuilder
    implements Builder<GSongsData_songs_songs, GSongsData_songs_songsBuilder> {
  _$GSongsData_songs_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _duration;
  String? get duration => _$this._duration;
  set duration(String? duration) => _$this._duration = duration;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSongsData_songs_songs_signedUrlBuilder? _signedUrl;
  GSongsData_songs_songs_signedUrlBuilder get signedUrl =>
      _$this._signedUrl ??= new GSongsData_songs_songs_signedUrlBuilder();
  set signedUrl(GSongsData_songs_songs_signedUrlBuilder? signedUrl) =>
      _$this._signedUrl = signedUrl;

  GSongsData_songs_songs_albumBuilder? _album;
  GSongsData_songs_songs_albumBuilder get album =>
      _$this._album ??= new GSongsData_songs_songs_albumBuilder();
  set album(GSongsData_songs_songs_albumBuilder? album) =>
      _$this._album = album;

  GSongsData_songs_songs_artistBuilder? _artist;
  GSongsData_songs_songs_artistBuilder get artist =>
      _$this._artist ??= new GSongsData_songs_songs_artistBuilder();
  set artist(GSongsData_songs_songs_artistBuilder? artist) =>
      _$this._artist = artist;

  GSongsData_songs_songsBuilder() {
    GSongsData_songs_songs._initializeBuilder(this);
  }

  GSongsData_songs_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _duration = $v.duration;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _signedUrl = $v.signedUrl?.toBuilder();
      _album = $v.album?.toBuilder();
      _artist = $v.artist?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongsData_songs_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongsData_songs_songs;
  }

  @override
  void update(void Function(GSongsData_songs_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongsData_songs_songs build() {
    _$GSongsData_songs_songs _$result;
    try {
      _$result = _$v ??
          new _$GSongsData_songs_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongsData_songs_songs', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GSongsData_songs_songs', 's3Key'),
              duration: BuiltValueNullFieldError.checkNotNull(
                  duration, 'GSongsData_songs_songs', 'duration'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSongsData_songs_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GSongsData_songs_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              signedUrl: _signedUrl?.build(),
              album: _album?.build(),
              artist: _artist?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 'signedUrl';
        _signedUrl?.build();
        _$failedField = 'album';
        _album?.build();
        _$failedField = 'artist';
        _artist?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongsData_songs_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSongsData_songs_songs_signedUrl
    extends GSongsData_songs_songs_signedUrl {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String signedUrl;

  factory _$GSongsData_songs_songs_signedUrl(
          [void Function(GSongsData_songs_songs_signedUrlBuilder)? updates]) =>
      (new GSongsData_songs_songs_signedUrlBuilder()..update(updates)).build();

  _$GSongsData_songs_songs_signedUrl._(
      {required this.G__typename, required this.id, required this.signedUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongsData_songs_songs_signedUrl', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GSongsData_songs_songs_signedUrl', 'id');
    BuiltValueNullFieldError.checkNotNull(
        signedUrl, 'GSongsData_songs_songs_signedUrl', 'signedUrl');
  }

  @override
  GSongsData_songs_songs_signedUrl rebuild(
          void Function(GSongsData_songs_songs_signedUrlBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongsData_songs_songs_signedUrlBuilder toBuilder() =>
      new GSongsData_songs_songs_signedUrlBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongsData_songs_songs_signedUrl &&
        G__typename == other.G__typename &&
        id == other.id &&
        signedUrl == other.signedUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), id.hashCode), signedUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongsData_songs_songs_signedUrl')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('signedUrl', signedUrl))
        .toString();
  }
}

class GSongsData_songs_songs_signedUrlBuilder
    implements
        Builder<GSongsData_songs_songs_signedUrl,
            GSongsData_songs_songs_signedUrlBuilder> {
  _$GSongsData_songs_songs_signedUrl? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _signedUrl;
  String? get signedUrl => _$this._signedUrl;
  set signedUrl(String? signedUrl) => _$this._signedUrl = signedUrl;

  GSongsData_songs_songs_signedUrlBuilder() {
    GSongsData_songs_songs_signedUrl._initializeBuilder(this);
  }

  GSongsData_songs_songs_signedUrlBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _signedUrl = $v.signedUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongsData_songs_songs_signedUrl other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongsData_songs_songs_signedUrl;
  }

  @override
  void update(void Function(GSongsData_songs_songs_signedUrlBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongsData_songs_songs_signedUrl build() {
    final _$result = _$v ??
        new _$GSongsData_songs_songs_signedUrl._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongsData_songs_songs_signedUrl', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongsData_songs_songs_signedUrl', 'id'),
            signedUrl: BuiltValueNullFieldError.checkNotNull(
                signedUrl, 'GSongsData_songs_songs_signedUrl', 'signedUrl'));
    replace(_$result);
    return _$result;
  }
}

class _$GSongsData_songs_songs_album extends GSongsData_songs_songs_album {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GSongsData_songs_songs_album(
          [void Function(GSongsData_songs_songs_albumBuilder)? updates]) =>
      (new GSongsData_songs_songs_albumBuilder()..update(updates)).build();

  _$GSongsData_songs_songs_album._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongsData_songs_songs_album', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GSongsData_songs_songs_album', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GSongsData_songs_songs_album', 'name');
  }

  @override
  GSongsData_songs_songs_album rebuild(
          void Function(GSongsData_songs_songs_albumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongsData_songs_songs_albumBuilder toBuilder() =>
      new GSongsData_songs_songs_albumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongsData_songs_songs_album &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongsData_songs_songs_album')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GSongsData_songs_songs_albumBuilder
    implements
        Builder<GSongsData_songs_songs_album,
            GSongsData_songs_songs_albumBuilder> {
  _$GSongsData_songs_songs_album? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GSongsData_songs_songs_albumBuilder() {
    GSongsData_songs_songs_album._initializeBuilder(this);
  }

  GSongsData_songs_songs_albumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongsData_songs_songs_album other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongsData_songs_songs_album;
  }

  @override
  void update(void Function(GSongsData_songs_songs_albumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongsData_songs_songs_album build() {
    final _$result = _$v ??
        new _$GSongsData_songs_songs_album._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongsData_songs_songs_album', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongsData_songs_songs_album', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GSongsData_songs_songs_album', 'name'));
    replace(_$result);
    return _$result;
  }
}

class _$GSongsData_songs_songs_artist extends GSongsData_songs_songs_artist {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GSongsData_songs_songs_artist(
          [void Function(GSongsData_songs_songs_artistBuilder)? updates]) =>
      (new GSongsData_songs_songs_artistBuilder()..update(updates)).build();

  _$GSongsData_songs_songs_artist._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongsData_songs_songs_artist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GSongsData_songs_songs_artist', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GSongsData_songs_songs_artist', 'name');
  }

  @override
  GSongsData_songs_songs_artist rebuild(
          void Function(GSongsData_songs_songs_artistBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongsData_songs_songs_artistBuilder toBuilder() =>
      new GSongsData_songs_songs_artistBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongsData_songs_songs_artist &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongsData_songs_songs_artist')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GSongsData_songs_songs_artistBuilder
    implements
        Builder<GSongsData_songs_songs_artist,
            GSongsData_songs_songs_artistBuilder> {
  _$GSongsData_songs_songs_artist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GSongsData_songs_songs_artistBuilder() {
    GSongsData_songs_songs_artist._initializeBuilder(this);
  }

  GSongsData_songs_songs_artistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongsData_songs_songs_artist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongsData_songs_songs_artist;
  }

  @override
  void update(void Function(GSongsData_songs_songs_artistBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongsData_songs_songs_artist build() {
    final _$result = _$v ??
        new _$GSongsData_songs_songs_artist._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongsData_songs_songs_artist', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongsData_songs_songs_artist', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GSongsData_songs_songs_artist', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
