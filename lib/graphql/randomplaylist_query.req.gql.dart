// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/randomplaylist_query.ast.gql.dart' as _i5;
import 'package:eclektik/graphql/randomplaylist_query.data.gql.dart' as _i2;
import 'package:eclektik/graphql/randomplaylist_query.var.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;

part 'randomplaylist_query.req.gql.g.dart';

abstract class GRandomPlaylistReq
    implements
        Built<GRandomPlaylistReq, GRandomPlaylistReqBuilder>,
        _i1.OperationRequest<_i2.GRandomPlaylistData, _i3.GRandomPlaylistVars> {
  GRandomPlaylistReq._();

  factory GRandomPlaylistReq([Function(GRandomPlaylistReqBuilder b) updates]) =
      _$GRandomPlaylistReq;

  static void _initializeBuilder(GRandomPlaylistReqBuilder b) => b
    ..operation =
        _i4.Operation(document: _i5.document, operationName: 'RandomPlaylist')
    ..executeOnListen = true;
  _i3.GRandomPlaylistVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GRandomPlaylistData? Function(
      _i2.GRandomPlaylistData?, _i2.GRandomPlaylistData?)? get updateResult;
  _i2.GRandomPlaylistData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GRandomPlaylistData? parseData(Map<String, dynamic> json) =>
      _i2.GRandomPlaylistData.fromJson(json);
  static Serializer<GRandomPlaylistReq> get serializer =>
      _$gRandomPlaylistReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GRandomPlaylistReq.serializer, this)
          as Map<String, dynamic>);
  static GRandomPlaylistReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GRandomPlaylistReq.serializer, json);
}
