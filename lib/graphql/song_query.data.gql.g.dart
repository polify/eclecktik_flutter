// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongData> _$gSongDataSerializer = new _$GSongDataSerializer();
Serializer<GSongData_song> _$gSongDataSongSerializer =
    new _$GSongData_songSerializer();
Serializer<GSongData_song_signedUrl> _$gSongDataSongSignedUrlSerializer =
    new _$GSongData_song_signedUrlSerializer();
Serializer<GSongData_song_album> _$gSongDataSongAlbumSerializer =
    new _$GSongData_song_albumSerializer();
Serializer<GSongData_song_artist> _$gSongDataSongArtistSerializer =
    new _$GSongData_song_artistSerializer();

class _$GSongDataSerializer implements StructuredSerializer<GSongData> {
  @override
  final Iterable<Type> types = const [GSongData, _$GSongData];
  @override
  final String wireName = 'GSongData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'song',
      serializers.serialize(object.song,
          specifiedType: const FullType(GSongData_song)),
    ];

    return result;
  }

  @override
  GSongData deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'song':
          result.song.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongData_song))!
              as GSongData_song);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongData_songSerializer
    implements StructuredSerializer<GSongData_song> {
  @override
  final Iterable<Type> types = const [GSongData_song, _$GSongData_song];
  @override
  final String wireName = 'GSongData_song';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongData_song object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.signedUrl;
    if (value != null) {
      result
        ..add('signedUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongData_song_signedUrl)));
    }
    value = object.album;
    if (value != null) {
      result
        ..add('album')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongData_song_album)));
    }
    value = object.artist;
    if (value != null) {
      result
        ..add('artist')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongData_song_artist)));
    }
    return result;
  }

  @override
  GSongData_song deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongData_songBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'signedUrl':
          result.signedUrl.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongData_song_signedUrl))!
              as GSongData_song_signedUrl);
          break;
        case 'album':
          result.album.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongData_song_album))!
              as GSongData_song_album);
          break;
        case 'artist':
          result.artist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongData_song_artist))!
              as GSongData_song_artist);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongData_song_signedUrlSerializer
    implements StructuredSerializer<GSongData_song_signedUrl> {
  @override
  final Iterable<Type> types = const [
    GSongData_song_signedUrl,
    _$GSongData_song_signedUrl
  ];
  @override
  final String wireName = 'GSongData_song_signedUrl';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongData_song_signedUrl object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'signedUrl',
      serializers.serialize(object.signedUrl,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongData_song_signedUrl deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongData_song_signedUrlBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'signedUrl':
          result.signedUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongData_song_albumSerializer
    implements StructuredSerializer<GSongData_song_album> {
  @override
  final Iterable<Type> types = const [
    GSongData_song_album,
    _$GSongData_song_album
  ];
  @override
  final String wireName = 'GSongData_song_album';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongData_song_album object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongData_song_album deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongData_song_albumBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongData_song_artistSerializer
    implements StructuredSerializer<GSongData_song_artist> {
  @override
  final Iterable<Type> types = const [
    GSongData_song_artist,
    _$GSongData_song_artist
  ];
  @override
  final String wireName = 'GSongData_song_artist';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongData_song_artist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongData_song_artist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongData_song_artistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongData extends GSongData {
  @override
  final String G__typename;
  @override
  final GSongData_song song;

  factory _$GSongData([void Function(GSongDataBuilder)? updates]) =>
      (new GSongDataBuilder()..update(updates)).build();

  _$GSongData._({required this.G__typename, required this.song}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(song, 'GSongData', 'song');
  }

  @override
  GSongData rebuild(void Function(GSongDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongDataBuilder toBuilder() => new GSongDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongData &&
        G__typename == other.G__typename &&
        song == other.song;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), song.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongData')
          ..add('G__typename', G__typename)
          ..add('song', song))
        .toString();
  }
}

class GSongDataBuilder implements Builder<GSongData, GSongDataBuilder> {
  _$GSongData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GSongData_songBuilder? _song;
  GSongData_songBuilder get song =>
      _$this._song ??= new GSongData_songBuilder();
  set song(GSongData_songBuilder? song) => _$this._song = song;

  GSongDataBuilder() {
    GSongData._initializeBuilder(this);
  }

  GSongDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _song = $v.song.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongData;
  }

  @override
  void update(void Function(GSongDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongData build() {
    _$GSongData _$result;
    try {
      _$result = _$v ??
          new _$GSongData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongData', 'G__typename'),
              song: song.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'song';
        song.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSongData_song extends GSongData_song {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final String duration;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;
  @override
  final GSongData_song_signedUrl? signedUrl;
  @override
  final GSongData_song_album? album;
  @override
  final GSongData_song_artist? artist;

  factory _$GSongData_song([void Function(GSongData_songBuilder)? updates]) =>
      (new GSongData_songBuilder()..update(updates)).build();

  _$GSongData_song._(
      {required this.G__typename,
      required this.s3Key,
      required this.duration,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt,
      this.signedUrl,
      this.album,
      this.artist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongData_song', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(s3Key, 'GSongData_song', 's3Key');
    BuiltValueNullFieldError.checkNotNull(
        duration, 'GSongData_song', 'duration');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongData_song', 'id');
    BuiltValueNullFieldError.checkNotNull(title, 'GSongData_song', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSongData_song', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSongData_song', 'updatedAt');
  }

  @override
  GSongData_song rebuild(void Function(GSongData_songBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongData_songBuilder toBuilder() =>
      new GSongData_songBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongData_song &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        duration == other.duration &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        signedUrl == other.signedUrl &&
        album == other.album &&
        artist == other.artist;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, G__typename.hashCode),
                                            s3Key.hashCode),
                                        duration.hashCode),
                                    id.hashCode),
                                title.hashCode),
                            coverUrl.hashCode),
                        createdAt.hashCode),
                    updatedAt.hashCode),
                signedUrl.hashCode),
            album.hashCode),
        artist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongData_song')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('duration', duration)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('signedUrl', signedUrl)
          ..add('album', album)
          ..add('artist', artist))
        .toString();
  }
}

class GSongData_songBuilder
    implements Builder<GSongData_song, GSongData_songBuilder> {
  _$GSongData_song? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _duration;
  String? get duration => _$this._duration;
  set duration(String? duration) => _$this._duration = duration;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSongData_song_signedUrlBuilder? _signedUrl;
  GSongData_song_signedUrlBuilder get signedUrl =>
      _$this._signedUrl ??= new GSongData_song_signedUrlBuilder();
  set signedUrl(GSongData_song_signedUrlBuilder? signedUrl) =>
      _$this._signedUrl = signedUrl;

  GSongData_song_albumBuilder? _album;
  GSongData_song_albumBuilder get album =>
      _$this._album ??= new GSongData_song_albumBuilder();
  set album(GSongData_song_albumBuilder? album) => _$this._album = album;

  GSongData_song_artistBuilder? _artist;
  GSongData_song_artistBuilder get artist =>
      _$this._artist ??= new GSongData_song_artistBuilder();
  set artist(GSongData_song_artistBuilder? artist) => _$this._artist = artist;

  GSongData_songBuilder() {
    GSongData_song._initializeBuilder(this);
  }

  GSongData_songBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _duration = $v.duration;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _signedUrl = $v.signedUrl?.toBuilder();
      _album = $v.album?.toBuilder();
      _artist = $v.artist?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongData_song other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongData_song;
  }

  @override
  void update(void Function(GSongData_songBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongData_song build() {
    _$GSongData_song _$result;
    try {
      _$result = _$v ??
          new _$GSongData_song._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongData_song', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GSongData_song', 's3Key'),
              duration: BuiltValueNullFieldError.checkNotNull(
                  duration, 'GSongData_song', 'duration'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSongData_song', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GSongData_song', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              signedUrl: _signedUrl?.build(),
              album: _album?.build(),
              artist: _artist?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 'signedUrl';
        _signedUrl?.build();
        _$failedField = 'album';
        _album?.build();
        _$failedField = 'artist';
        _artist?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongData_song', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSongData_song_signedUrl extends GSongData_song_signedUrl {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String signedUrl;

  factory _$GSongData_song_signedUrl(
          [void Function(GSongData_song_signedUrlBuilder)? updates]) =>
      (new GSongData_song_signedUrlBuilder()..update(updates)).build();

  _$GSongData_song_signedUrl._(
      {required this.G__typename, required this.id, required this.signedUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongData_song_signedUrl', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongData_song_signedUrl', 'id');
    BuiltValueNullFieldError.checkNotNull(
        signedUrl, 'GSongData_song_signedUrl', 'signedUrl');
  }

  @override
  GSongData_song_signedUrl rebuild(
          void Function(GSongData_song_signedUrlBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongData_song_signedUrlBuilder toBuilder() =>
      new GSongData_song_signedUrlBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongData_song_signedUrl &&
        G__typename == other.G__typename &&
        id == other.id &&
        signedUrl == other.signedUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), id.hashCode), signedUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongData_song_signedUrl')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('signedUrl', signedUrl))
        .toString();
  }
}

class GSongData_song_signedUrlBuilder
    implements
        Builder<GSongData_song_signedUrl, GSongData_song_signedUrlBuilder> {
  _$GSongData_song_signedUrl? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _signedUrl;
  String? get signedUrl => _$this._signedUrl;
  set signedUrl(String? signedUrl) => _$this._signedUrl = signedUrl;

  GSongData_song_signedUrlBuilder() {
    GSongData_song_signedUrl._initializeBuilder(this);
  }

  GSongData_song_signedUrlBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _signedUrl = $v.signedUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongData_song_signedUrl other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongData_song_signedUrl;
  }

  @override
  void update(void Function(GSongData_song_signedUrlBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongData_song_signedUrl build() {
    final _$result = _$v ??
        new _$GSongData_song_signedUrl._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongData_song_signedUrl', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongData_song_signedUrl', 'id'),
            signedUrl: BuiltValueNullFieldError.checkNotNull(
                signedUrl, 'GSongData_song_signedUrl', 'signedUrl'));
    replace(_$result);
    return _$result;
  }
}

class _$GSongData_song_album extends GSongData_song_album {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GSongData_song_album(
          [void Function(GSongData_song_albumBuilder)? updates]) =>
      (new GSongData_song_albumBuilder()..update(updates)).build();

  _$GSongData_song_album._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongData_song_album', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongData_song_album', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GSongData_song_album', 'name');
  }

  @override
  GSongData_song_album rebuild(
          void Function(GSongData_song_albumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongData_song_albumBuilder toBuilder() =>
      new GSongData_song_albumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongData_song_album &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongData_song_album')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GSongData_song_albumBuilder
    implements Builder<GSongData_song_album, GSongData_song_albumBuilder> {
  _$GSongData_song_album? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GSongData_song_albumBuilder() {
    GSongData_song_album._initializeBuilder(this);
  }

  GSongData_song_albumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongData_song_album other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongData_song_album;
  }

  @override
  void update(void Function(GSongData_song_albumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongData_song_album build() {
    final _$result = _$v ??
        new _$GSongData_song_album._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongData_song_album', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongData_song_album', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GSongData_song_album', 'name'));
    replace(_$result);
    return _$result;
  }
}

class _$GSongData_song_artist extends GSongData_song_artist {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GSongData_song_artist(
          [void Function(GSongData_song_artistBuilder)? updates]) =>
      (new GSongData_song_artistBuilder()..update(updates)).build();

  _$GSongData_song_artist._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongData_song_artist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongData_song_artist', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GSongData_song_artist', 'name');
  }

  @override
  GSongData_song_artist rebuild(
          void Function(GSongData_song_artistBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongData_song_artistBuilder toBuilder() =>
      new GSongData_song_artistBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongData_song_artist &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongData_song_artist')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GSongData_song_artistBuilder
    implements Builder<GSongData_song_artist, GSongData_song_artistBuilder> {
  _$GSongData_song_artist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GSongData_song_artistBuilder() {
    GSongData_song_artist._initializeBuilder(this);
  }

  GSongData_song_artistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongData_song_artist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongData_song_artist;
  }

  @override
  void update(void Function(GSongData_song_artistBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongData_song_artist build() {
    final _$result = _$v ??
        new _$GSongData_song_artist._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongData_song_artist', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongData_song_artist', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GSongData_song_artist', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
