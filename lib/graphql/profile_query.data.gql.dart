// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/device_card_fragment.data.gql.dart' as _i3;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i2;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'profile_query.data.gql.g.dart';

abstract class GProfileData
    implements Built<GProfileData, GProfileDataBuilder> {
  GProfileData._();

  factory GProfileData([Function(GProfileDataBuilder b) updates]) =
      _$GProfileData;

  static void _initializeBuilder(GProfileDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GProfileData_profile get profile;
  static Serializer<GProfileData> get serializer => _$gProfileDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GProfileData.serializer, this)
          as Map<String, dynamic>);
  static GProfileData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GProfileData.serializer, json);
}

abstract class GProfileData_profile
    implements Built<GProfileData_profile, GProfileData_profileBuilder> {
  GProfileData_profile._();

  factory GProfileData_profile(
          [Function(GProfileData_profileBuilder b) updates]) =
      _$GProfileData_profile;

  static void _initializeBuilder(GProfileData_profileBuilder b) =>
      b..G__typename = 'User';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  GProfileData_profile_s3Credential? get s3Credential;
  BuiltList<GProfileData_profile_devices> get devices;
  static Serializer<GProfileData_profile> get serializer =>
      _$gProfileDataProfileSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GProfileData_profile.serializer, this)
          as Map<String, dynamic>);
  static GProfileData_profile? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GProfileData_profile.serializer, json);
}

abstract class GProfileData_profile_s3Credential
    implements
        Built<GProfileData_profile_s3Credential,
            GProfileData_profile_s3CredentialBuilder> {
  GProfileData_profile_s3Credential._();

  factory GProfileData_profile_s3Credential(
          [Function(GProfileData_profile_s3CredentialBuilder b) updates]) =
      _$GProfileData_profile_s3Credential;

  static void _initializeBuilder(GProfileData_profile_s3CredentialBuilder b) =>
      b..G__typename = 'S3Credential';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String? get keyID;
  String? get keySecret;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  static Serializer<GProfileData_profile_s3Credential> get serializer =>
      _$gProfileDataProfileS3CredentialSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GProfileData_profile_s3Credential.serializer, this)
      as Map<String, dynamic>);
  static GProfileData_profile_s3Credential? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GProfileData_profile_s3Credential.serializer, json);
}

abstract class GProfileData_profile_devices
    implements
        Built<GProfileData_profile_devices,
            GProfileData_profile_devicesBuilder>,
        _i3.GDeviceItem {
  GProfileData_profile_devices._();

  factory GProfileData_profile_devices(
          [Function(GProfileData_profile_devicesBuilder b) updates]) =
      _$GProfileData_profile_devices;

  static void _initializeBuilder(GProfileData_profile_devicesBuilder b) =>
      b..G__typename = 'Device';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get lastIP;
  String get name;
  String get os;
  String get type;
  String get userAgent;
  _i2.GTime get lastConnection;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  static Serializer<GProfileData_profile_devices> get serializer =>
      _$gProfileDataProfileDevicesSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GProfileData_profile_devices.serializer, this) as Map<String, dynamic>);
  static GProfileData_profile_devices? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GProfileData_profile_devices.serializer, json);
}
