// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/device_card_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/device_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/device_card_fragment.var.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;

part 'device_card_fragment.req.gql.g.dart';

abstract class GDeviceItemReq
    implements
        Built<GDeviceItemReq, GDeviceItemReqBuilder>,
        _i1.FragmentRequest<_i2.GDeviceItemData, _i3.GDeviceItemVars> {
  GDeviceItemReq._();

  factory GDeviceItemReq([Function(GDeviceItemReqBuilder b) updates]) =
      _$GDeviceItemReq;

  static void _initializeBuilder(GDeviceItemReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'DeviceItem';
  _i3.GDeviceItemVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GDeviceItemData? parseData(Map<String, dynamic> json) =>
      _i2.GDeviceItemData.fromJson(json);
  static Serializer<GDeviceItemReq> get serializer =>
      _$gDeviceItemReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GDeviceItemReq.serializer, this)
          as Map<String, dynamic>);
  static GDeviceItemReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GDeviceItemReq.serializer, json);
}
