// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_card_fragment.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GDeviceItemData> _$gDeviceItemDataSerializer =
    new _$GDeviceItemDataSerializer();

class _$GDeviceItemDataSerializer
    implements StructuredSerializer<GDeviceItemData> {
  @override
  final Iterable<Type> types = const [GDeviceItemData, _$GDeviceItemData];
  @override
  final String wireName = 'GDeviceItemData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GDeviceItemData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'lastIP',
      serializers.serialize(object.lastIP,
          specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'os',
      serializers.serialize(object.os, specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'userAgent',
      serializers.serialize(object.userAgent,
          specifiedType: const FullType(String)),
      'lastConnection',
      serializers.serialize(object.lastConnection,
          specifiedType: const FullType(_i1.GTime)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i1.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i1.GTime)),
    ];

    return result;
  }

  @override
  GDeviceItemData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GDeviceItemDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastIP':
          result.lastIP = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'os':
          result.os = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userAgent':
          result.userAgent = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastConnection':
          result.lastConnection.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i1.GTime))! as _i1.GTime);
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i1.GTime))! as _i1.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i1.GTime))! as _i1.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GDeviceItemData extends GDeviceItemData {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String lastIP;
  @override
  final String name;
  @override
  final String os;
  @override
  final String type;
  @override
  final String userAgent;
  @override
  final _i1.GTime lastConnection;
  @override
  final _i1.GTime createdAt;
  @override
  final _i1.GTime updatedAt;

  factory _$GDeviceItemData([void Function(GDeviceItemDataBuilder)? updates]) =>
      (new GDeviceItemDataBuilder()..update(updates)).build();

  _$GDeviceItemData._(
      {required this.G__typename,
      required this.id,
      required this.lastIP,
      required this.name,
      required this.os,
      required this.type,
      required this.userAgent,
      required this.lastConnection,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GDeviceItemData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GDeviceItemData', 'id');
    BuiltValueNullFieldError.checkNotNull(lastIP, 'GDeviceItemData', 'lastIP');
    BuiltValueNullFieldError.checkNotNull(name, 'GDeviceItemData', 'name');
    BuiltValueNullFieldError.checkNotNull(os, 'GDeviceItemData', 'os');
    BuiltValueNullFieldError.checkNotNull(type, 'GDeviceItemData', 'type');
    BuiltValueNullFieldError.checkNotNull(
        userAgent, 'GDeviceItemData', 'userAgent');
    BuiltValueNullFieldError.checkNotNull(
        lastConnection, 'GDeviceItemData', 'lastConnection');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GDeviceItemData', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GDeviceItemData', 'updatedAt');
  }

  @override
  GDeviceItemData rebuild(void Function(GDeviceItemDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GDeviceItemDataBuilder toBuilder() =>
      new GDeviceItemDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GDeviceItemData &&
        G__typename == other.G__typename &&
        id == other.id &&
        lastIP == other.lastIP &&
        name == other.name &&
        os == other.os &&
        type == other.type &&
        userAgent == other.userAgent &&
        lastConnection == other.lastConnection &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, G__typename.hashCode),
                                        id.hashCode),
                                    lastIP.hashCode),
                                name.hashCode),
                            os.hashCode),
                        type.hashCode),
                    userAgent.hashCode),
                lastConnection.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GDeviceItemData')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('lastIP', lastIP)
          ..add('name', name)
          ..add('os', os)
          ..add('type', type)
          ..add('userAgent', userAgent)
          ..add('lastConnection', lastConnection)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GDeviceItemDataBuilder
    implements Builder<GDeviceItemData, GDeviceItemDataBuilder> {
  _$GDeviceItemData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _lastIP;
  String? get lastIP => _$this._lastIP;
  set lastIP(String? lastIP) => _$this._lastIP = lastIP;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _os;
  String? get os => _$this._os;
  set os(String? os) => _$this._os = os;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  String? _userAgent;
  String? get userAgent => _$this._userAgent;
  set userAgent(String? userAgent) => _$this._userAgent = userAgent;

  _i1.GTimeBuilder? _lastConnection;
  _i1.GTimeBuilder get lastConnection =>
      _$this._lastConnection ??= new _i1.GTimeBuilder();
  set lastConnection(_i1.GTimeBuilder? lastConnection) =>
      _$this._lastConnection = lastConnection;

  _i1.GTimeBuilder? _createdAt;
  _i1.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i1.GTimeBuilder();
  set createdAt(_i1.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i1.GTimeBuilder? _updatedAt;
  _i1.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i1.GTimeBuilder();
  set updatedAt(_i1.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GDeviceItemDataBuilder() {
    GDeviceItemData._initializeBuilder(this);
  }

  GDeviceItemDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _lastIP = $v.lastIP;
      _name = $v.name;
      _os = $v.os;
      _type = $v.type;
      _userAgent = $v.userAgent;
      _lastConnection = $v.lastConnection.toBuilder();
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GDeviceItemData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GDeviceItemData;
  }

  @override
  void update(void Function(GDeviceItemDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GDeviceItemData build() {
    _$GDeviceItemData _$result;
    try {
      _$result = _$v ??
          new _$GDeviceItemData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GDeviceItemData', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GDeviceItemData', 'id'),
              lastIP: BuiltValueNullFieldError.checkNotNull(
                  lastIP, 'GDeviceItemData', 'lastIP'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GDeviceItemData', 'name'),
              os: BuiltValueNullFieldError.checkNotNull(
                  os, 'GDeviceItemData', 'os'),
              type: BuiltValueNullFieldError.checkNotNull(
                  type, 'GDeviceItemData', 'type'),
              userAgent: BuiltValueNullFieldError.checkNotNull(
                  userAgent, 'GDeviceItemData', 'userAgent'),
              lastConnection: lastConnection.build(),
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'lastConnection';
        lastConnection.build();
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GDeviceItemData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
