// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/search_query.ast.gql.dart' as _i5;
import 'package:eclektik/graphql/search_query.data.gql.dart' as _i2;
import 'package:eclektik/graphql/search_query.var.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;

part 'search_query.req.gql.g.dart';

abstract class GSearchReq
    implements
        Built<GSearchReq, GSearchReqBuilder>,
        _i1.OperationRequest<_i2.GSearchData, _i3.GSearchVars> {
  GSearchReq._();

  factory GSearchReq([Function(GSearchReqBuilder b) updates]) = _$GSearchReq;

  static void _initializeBuilder(GSearchReqBuilder b) => b
    ..operation = _i4.Operation(document: _i5.document, operationName: 'Search')
    ..executeOnListen = true;
  _i3.GSearchVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GSearchData? Function(_i2.GSearchData?, _i2.GSearchData?)?
      get updateResult;
  _i2.GSearchData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GSearchData? parseData(Map<String, dynamic> json) =>
      _i2.GSearchData.fromJson(json);
  static Serializer<GSearchReq> get serializer => _$gSearchReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GSearchReq.serializer, this)
          as Map<String, dynamic>);
  static GSearchReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GSearchReq.serializer, json);
}
