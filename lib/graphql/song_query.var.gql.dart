// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'song_query.var.gql.g.dart';

abstract class GSongVars implements Built<GSongVars, GSongVarsBuilder> {
  GSongVars._();

  factory GSongVars([Function(GSongVarsBuilder b) updates]) = _$GSongVars;

  String get id;
  static Serializer<GSongVars> get serializer => _$gSongVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongVars.serializer, this)
          as Map<String, dynamic>);
  static GSongVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongVars.serializer, json);
}
