// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'randomplaylist_query.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GRandomPlaylistVars> _$gRandomPlaylistVarsSerializer =
    new _$GRandomPlaylistVarsSerializer();

class _$GRandomPlaylistVarsSerializer
    implements StructuredSerializer<GRandomPlaylistVars> {
  @override
  final Iterable<Type> types = const [
    GRandomPlaylistVars,
    _$GRandomPlaylistVars
  ];
  @override
  final String wireName = 'GRandomPlaylistVars';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GRandomPlaylistVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.limit;
    if (value != null) {
      result
        ..add('limit')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GRandomPlaylistVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GRandomPlaylistVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'limit':
          result.limit = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$GRandomPlaylistVars extends GRandomPlaylistVars {
  @override
  final int? limit;

  factory _$GRandomPlaylistVars(
          [void Function(GRandomPlaylistVarsBuilder)? updates]) =>
      (new GRandomPlaylistVarsBuilder()..update(updates)).build();

  _$GRandomPlaylistVars._({this.limit}) : super._();

  @override
  GRandomPlaylistVars rebuild(
          void Function(GRandomPlaylistVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GRandomPlaylistVarsBuilder toBuilder() =>
      new GRandomPlaylistVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GRandomPlaylistVars && limit == other.limit;
  }

  @override
  int get hashCode {
    return $jf($jc(0, limit.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GRandomPlaylistVars')
          ..add('limit', limit))
        .toString();
  }
}

class GRandomPlaylistVarsBuilder
    implements Builder<GRandomPlaylistVars, GRandomPlaylistVarsBuilder> {
  _$GRandomPlaylistVars? _$v;

  int? _limit;
  int? get limit => _$this._limit;
  set limit(int? limit) => _$this._limit = limit;

  GRandomPlaylistVarsBuilder();

  GRandomPlaylistVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _limit = $v.limit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GRandomPlaylistVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GRandomPlaylistVars;
  }

  @override
  void update(void Function(GRandomPlaylistVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GRandomPlaylistVars build() {
    final _$result = _$v ?? new _$GRandomPlaylistVars._(limit: limit);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
