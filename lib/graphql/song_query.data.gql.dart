// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart' as _i2;

part 'song_query.data.gql.g.dart';

abstract class GSongData implements Built<GSongData, GSongDataBuilder> {
  GSongData._();

  factory GSongData([Function(GSongDataBuilder b) updates]) = _$GSongData;

  static void _initializeBuilder(GSongDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GSongData_song get song;
  static Serializer<GSongData> get serializer => _$gSongDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongData.serializer, this)
          as Map<String, dynamic>);
  static GSongData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongData.serializer, json);
}

abstract class GSongData_song
    implements Built<GSongData_song, GSongData_songBuilder>, _i2.GSongCard {
  GSongData_song._();

  factory GSongData_song([Function(GSongData_songBuilder b) updates]) =
      _$GSongData_song;

  static void _initializeBuilder(GSongData_songBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  String get duration;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  GSongData_song_signedUrl? get signedUrl;
  GSongData_song_album? get album;
  GSongData_song_artist? get artist;
  static Serializer<GSongData_song> get serializer => _$gSongDataSongSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongData_song.serializer, this)
          as Map<String, dynamic>);
  static GSongData_song? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongData_song.serializer, json);
}

abstract class GSongData_song_signedUrl
    implements
        Built<GSongData_song_signedUrl, GSongData_song_signedUrlBuilder>,
        _i2.GSongCard_signedUrl {
  GSongData_song_signedUrl._();

  factory GSongData_song_signedUrl(
          [Function(GSongData_song_signedUrlBuilder b) updates]) =
      _$GSongData_song_signedUrl;

  static void _initializeBuilder(GSongData_song_signedUrlBuilder b) =>
      b..G__typename = 'S3SignedUrl';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get signedUrl;
  static Serializer<GSongData_song_signedUrl> get serializer =>
      _$gSongDataSongSignedUrlSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongData_song_signedUrl.serializer, this)
          as Map<String, dynamic>);
  static GSongData_song_signedUrl? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSongData_song_signedUrl.serializer, json);
}

abstract class GSongData_song_album
    implements
        Built<GSongData_song_album, GSongData_song_albumBuilder>,
        _i2.GSongCard_album {
  GSongData_song_album._();

  factory GSongData_song_album(
          [Function(GSongData_song_albumBuilder b) updates]) =
      _$GSongData_song_album;

  static void _initializeBuilder(GSongData_song_albumBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GSongData_song_album> get serializer =>
      _$gSongDataSongAlbumSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongData_song_album.serializer, this)
          as Map<String, dynamic>);
  static GSongData_song_album? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongData_song_album.serializer, json);
}

abstract class GSongData_song_artist
    implements
        Built<GSongData_song_artist, GSongData_song_artistBuilder>,
        _i2.GSongCard_artist {
  GSongData_song_artist._();

  factory GSongData_song_artist(
          [Function(GSongData_song_artistBuilder b) updates]) =
      _$GSongData_song_artist;

  static void _initializeBuilder(GSongData_song_artistBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GSongData_song_artist> get serializer =>
      _$gSongDataSongArtistSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongData_song_artist.serializer, this)
          as Map<String, dynamic>);
  static GSongData_song_artist? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongData_song_artist.serializer, json);
}
