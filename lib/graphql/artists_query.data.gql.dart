// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart' as _i4;
import 'package:eclektik/graphql/artist_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart' as _i5;

part 'artists_query.data.gql.g.dart';

abstract class GArtistsData
    implements Built<GArtistsData, GArtistsDataBuilder> {
  GArtistsData._();

  factory GArtistsData([Function(GArtistsDataBuilder b) updates]) =
      _$GArtistsData;

  static void _initializeBuilder(GArtistsDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  BuiltList<GArtistsData_artists> get artists;
  static Serializer<GArtistsData> get serializer => _$gArtistsDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistsData.serializer, this)
          as Map<String, dynamic>);
  static GArtistsData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistsData.serializer, json);
}

abstract class GArtistsData_artists
    implements
        Built<GArtistsData_artists, GArtistsData_artistsBuilder>,
        _i2.GArtistCard {
  GArtistsData_artists._();

  factory GArtistsData_artists(
          [Function(GArtistsData_artistsBuilder b) updates]) =
      _$GArtistsData_artists;

  static void _initializeBuilder(GArtistsData_artistsBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  BuiltList<GArtistsData_artists_albums> get albums;
  BuiltList<GArtistsData_artists_songs> get songs;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GArtistsData_artists> get serializer =>
      _$gArtistsDataArtistsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistsData_artists.serializer, this)
          as Map<String, dynamic>);
  static GArtistsData_artists? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistsData_artists.serializer, json);
}

abstract class GArtistsData_artists_albums
    implements
        Built<GArtistsData_artists_albums, GArtistsData_artists_albumsBuilder>,
        _i2.GArtistCard_albums,
        _i4.GAlbumItem {
  GArtistsData_artists_albums._();

  factory GArtistsData_artists_albums(
          [Function(GArtistsData_artists_albumsBuilder b) updates]) =
      _$GArtistsData_artists_albums;

  static void _initializeBuilder(GArtistsData_artists_albumsBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GArtistsData_artists_albums> get serializer =>
      _$gArtistsDataArtistsAlbumsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GArtistsData_artists_albums.serializer, this) as Map<String, dynamic>);
  static GArtistsData_artists_albums? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GArtistsData_artists_albums.serializer, json);
}

abstract class GArtistsData_artists_songs
    implements
        Built<GArtistsData_artists_songs, GArtistsData_artists_songsBuilder>,
        _i2.GArtistCard_songs,
        _i5.GSongItem {
  GArtistsData_artists_songs._();

  factory GArtistsData_artists_songs(
          [Function(GArtistsData_artists_songsBuilder b) updates]) =
      _$GArtistsData_artists_songs;

  static void _initializeBuilder(GArtistsData_artists_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GArtistsData_artists_songs> get serializer =>
      _$gArtistsDataArtistsSongsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GArtistsData_artists_songs.serializer, this) as Map<String, dynamic>);
  static GArtistsData_artists_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GArtistsData_artists_songs.serializer, json);
}
