// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song_card_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongCardVars> _$gSongCardVarsSerializer =
    new _$GSongCardVarsSerializer();

class _$GSongCardVarsSerializer implements StructuredSerializer<GSongCardVars> {
  @override
  final Iterable<Type> types = const [GSongCardVars, _$GSongCardVars];
  @override
  final String wireName = 'GSongCardVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongCardVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GSongCardVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GSongCardVarsBuilder().build();
  }
}

class _$GSongCardVars extends GSongCardVars {
  factory _$GSongCardVars([void Function(GSongCardVarsBuilder)? updates]) =>
      (new GSongCardVarsBuilder()..update(updates)).build();

  _$GSongCardVars._() : super._();

  @override
  GSongCardVars rebuild(void Function(GSongCardVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongCardVarsBuilder toBuilder() => new GSongCardVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongCardVars;
  }

  @override
  int get hashCode {
    return 649672305;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GSongCardVars').toString();
  }
}

class GSongCardVarsBuilder
    implements Builder<GSongCardVars, GSongCardVarsBuilder> {
  _$GSongCardVars? _$v;

  GSongCardVarsBuilder();

  @override
  void replace(GSongCardVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongCardVars;
  }

  @override
  void update(void Function(GSongCardVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongCardVars build() {
    final _$result = _$v ?? new _$GSongCardVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
