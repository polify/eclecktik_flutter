// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i1;
import 'package:eclektik/graphql/serializers.gql.dart' as _i2;

part 'album_item_fragment.data.gql.g.dart';

abstract class GAlbumItem {
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GAlbumItemData
    implements Built<GAlbumItemData, GAlbumItemDataBuilder>, GAlbumItem {
  GAlbumItemData._();

  factory GAlbumItemData([Function(GAlbumItemDataBuilder b) updates]) =
      _$GAlbumItemData;

  static void _initializeBuilder(GAlbumItemDataBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  static Serializer<GAlbumItemData> get serializer =>
      _$gAlbumItemDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GAlbumItemData.serializer, this)
          as Map<String, dynamic>);
  static GAlbumItemData? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GAlbumItemData.serializer, json);
}
