// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song_card_fragment.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongCardData> _$gSongCardDataSerializer =
    new _$GSongCardDataSerializer();
Serializer<GSongCardData_signedUrl> _$gSongCardDataSignedUrlSerializer =
    new _$GSongCardData_signedUrlSerializer();
Serializer<GSongCardData_album> _$gSongCardDataAlbumSerializer =
    new _$GSongCardData_albumSerializer();
Serializer<GSongCardData_artist> _$gSongCardDataArtistSerializer =
    new _$GSongCardData_artistSerializer();

class _$GSongCardDataSerializer implements StructuredSerializer<GSongCardData> {
  @override
  final Iterable<Type> types = const [GSongCardData, _$GSongCardData];
  @override
  final String wireName = 'GSongCardData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongCardData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.signedUrl;
    if (value != null) {
      result
        ..add('signedUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongCardData_signedUrl)));
    }
    value = object.album;
    if (value != null) {
      result
        ..add('album')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongCardData_album)));
    }
    value = object.artist;
    if (value != null) {
      result
        ..add('artist')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSongCardData_artist)));
    }
    return result;
  }

  @override
  GSongCardData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongCardDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'signedUrl':
          result.signedUrl.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongCardData_signedUrl))!
              as GSongCardData_signedUrl);
          break;
        case 'album':
          result.album.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongCardData_album))!
              as GSongCardData_album);
          break;
        case 'artist':
          result.artist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSongCardData_artist))!
              as GSongCardData_artist);
          break;
      }
    }

    return result.build();
  }
}

class _$GSongCardData_signedUrlSerializer
    implements StructuredSerializer<GSongCardData_signedUrl> {
  @override
  final Iterable<Type> types = const [
    GSongCardData_signedUrl,
    _$GSongCardData_signedUrl
  ];
  @override
  final String wireName = 'GSongCardData_signedUrl';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongCardData_signedUrl object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'signedUrl',
      serializers.serialize(object.signedUrl,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongCardData_signedUrl deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongCardData_signedUrlBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'signedUrl':
          result.signedUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongCardData_albumSerializer
    implements StructuredSerializer<GSongCardData_album> {
  @override
  final Iterable<Type> types = const [
    GSongCardData_album,
    _$GSongCardData_album
  ];
  @override
  final String wireName = 'GSongCardData_album';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongCardData_album object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongCardData_album deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongCardData_albumBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongCardData_artistSerializer
    implements StructuredSerializer<GSongCardData_artist> {
  @override
  final Iterable<Type> types = const [
    GSongCardData_artist,
    _$GSongCardData_artist
  ];
  @override
  final String wireName = 'GSongCardData_artist';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSongCardData_artist object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongCardData_artist deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongCardData_artistBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongCardData extends GSongCardData {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final String duration;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;
  @override
  final GSongCardData_signedUrl? signedUrl;
  @override
  final GSongCardData_album? album;
  @override
  final GSongCardData_artist? artist;

  factory _$GSongCardData([void Function(GSongCardDataBuilder)? updates]) =>
      (new GSongCardDataBuilder()..update(updates)).build();

  _$GSongCardData._(
      {required this.G__typename,
      required this.s3Key,
      required this.duration,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt,
      this.signedUrl,
      this.album,
      this.artist})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongCardData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(s3Key, 'GSongCardData', 's3Key');
    BuiltValueNullFieldError.checkNotNull(
        duration, 'GSongCardData', 'duration');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongCardData', 'id');
    BuiltValueNullFieldError.checkNotNull(title, 'GSongCardData', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSongCardData', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSongCardData', 'updatedAt');
  }

  @override
  GSongCardData rebuild(void Function(GSongCardDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongCardDataBuilder toBuilder() => new GSongCardDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongCardData &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        duration == other.duration &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        signedUrl == other.signedUrl &&
        album == other.album &&
        artist == other.artist;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, G__typename.hashCode),
                                            s3Key.hashCode),
                                        duration.hashCode),
                                    id.hashCode),
                                title.hashCode),
                            coverUrl.hashCode),
                        createdAt.hashCode),
                    updatedAt.hashCode),
                signedUrl.hashCode),
            album.hashCode),
        artist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongCardData')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('duration', duration)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('signedUrl', signedUrl)
          ..add('album', album)
          ..add('artist', artist))
        .toString();
  }
}

class GSongCardDataBuilder
    implements Builder<GSongCardData, GSongCardDataBuilder> {
  _$GSongCardData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _duration;
  String? get duration => _$this._duration;
  set duration(String? duration) => _$this._duration = duration;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSongCardData_signedUrlBuilder? _signedUrl;
  GSongCardData_signedUrlBuilder get signedUrl =>
      _$this._signedUrl ??= new GSongCardData_signedUrlBuilder();
  set signedUrl(GSongCardData_signedUrlBuilder? signedUrl) =>
      _$this._signedUrl = signedUrl;

  GSongCardData_albumBuilder? _album;
  GSongCardData_albumBuilder get album =>
      _$this._album ??= new GSongCardData_albumBuilder();
  set album(GSongCardData_albumBuilder? album) => _$this._album = album;

  GSongCardData_artistBuilder? _artist;
  GSongCardData_artistBuilder get artist =>
      _$this._artist ??= new GSongCardData_artistBuilder();
  set artist(GSongCardData_artistBuilder? artist) => _$this._artist = artist;

  GSongCardDataBuilder() {
    GSongCardData._initializeBuilder(this);
  }

  GSongCardDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _duration = $v.duration;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _signedUrl = $v.signedUrl?.toBuilder();
      _album = $v.album?.toBuilder();
      _artist = $v.artist?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongCardData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongCardData;
  }

  @override
  void update(void Function(GSongCardDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongCardData build() {
    _$GSongCardData _$result;
    try {
      _$result = _$v ??
          new _$GSongCardData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSongCardData', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GSongCardData', 's3Key'),
              duration: BuiltValueNullFieldError.checkNotNull(
                  duration, 'GSongCardData', 'duration'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSongCardData', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GSongCardData', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              signedUrl: _signedUrl?.build(),
              album: _album?.build(),
              artist: _artist?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 'signedUrl';
        _signedUrl?.build();
        _$failedField = 'album';
        _album?.build();
        _$failedField = 'artist';
        _artist?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSongCardData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSongCardData_signedUrl extends GSongCardData_signedUrl {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String signedUrl;

  factory _$GSongCardData_signedUrl(
          [void Function(GSongCardData_signedUrlBuilder)? updates]) =>
      (new GSongCardData_signedUrlBuilder()..update(updates)).build();

  _$GSongCardData_signedUrl._(
      {required this.G__typename, required this.id, required this.signedUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongCardData_signedUrl', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongCardData_signedUrl', 'id');
    BuiltValueNullFieldError.checkNotNull(
        signedUrl, 'GSongCardData_signedUrl', 'signedUrl');
  }

  @override
  GSongCardData_signedUrl rebuild(
          void Function(GSongCardData_signedUrlBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongCardData_signedUrlBuilder toBuilder() =>
      new GSongCardData_signedUrlBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongCardData_signedUrl &&
        G__typename == other.G__typename &&
        id == other.id &&
        signedUrl == other.signedUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), id.hashCode), signedUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongCardData_signedUrl')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('signedUrl', signedUrl))
        .toString();
  }
}

class GSongCardData_signedUrlBuilder
    implements
        Builder<GSongCardData_signedUrl, GSongCardData_signedUrlBuilder> {
  _$GSongCardData_signedUrl? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _signedUrl;
  String? get signedUrl => _$this._signedUrl;
  set signedUrl(String? signedUrl) => _$this._signedUrl = signedUrl;

  GSongCardData_signedUrlBuilder() {
    GSongCardData_signedUrl._initializeBuilder(this);
  }

  GSongCardData_signedUrlBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _signedUrl = $v.signedUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongCardData_signedUrl other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongCardData_signedUrl;
  }

  @override
  void update(void Function(GSongCardData_signedUrlBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongCardData_signedUrl build() {
    final _$result = _$v ??
        new _$GSongCardData_signedUrl._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongCardData_signedUrl', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongCardData_signedUrl', 'id'),
            signedUrl: BuiltValueNullFieldError.checkNotNull(
                signedUrl, 'GSongCardData_signedUrl', 'signedUrl'));
    replace(_$result);
    return _$result;
  }
}

class _$GSongCardData_album extends GSongCardData_album {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GSongCardData_album(
          [void Function(GSongCardData_albumBuilder)? updates]) =>
      (new GSongCardData_albumBuilder()..update(updates)).build();

  _$GSongCardData_album._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongCardData_album', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongCardData_album', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GSongCardData_album', 'name');
  }

  @override
  GSongCardData_album rebuild(
          void Function(GSongCardData_albumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongCardData_albumBuilder toBuilder() =>
      new GSongCardData_albumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongCardData_album &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongCardData_album')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GSongCardData_albumBuilder
    implements Builder<GSongCardData_album, GSongCardData_albumBuilder> {
  _$GSongCardData_album? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GSongCardData_albumBuilder() {
    GSongCardData_album._initializeBuilder(this);
  }

  GSongCardData_albumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongCardData_album other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongCardData_album;
  }

  @override
  void update(void Function(GSongCardData_albumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongCardData_album build() {
    final _$result = _$v ??
        new _$GSongCardData_album._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongCardData_album', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongCardData_album', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GSongCardData_album', 'name'));
    replace(_$result);
    return _$result;
  }
}

class _$GSongCardData_artist extends GSongCardData_artist {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GSongCardData_artist(
          [void Function(GSongCardData_artistBuilder)? updates]) =>
      (new GSongCardData_artistBuilder()..update(updates)).build();

  _$GSongCardData_artist._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSongCardData_artist', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GSongCardData_artist', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GSongCardData_artist', 'name');
  }

  @override
  GSongCardData_artist rebuild(
          void Function(GSongCardData_artistBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongCardData_artistBuilder toBuilder() =>
      new GSongCardData_artistBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongCardData_artist &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongCardData_artist')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GSongCardData_artistBuilder
    implements Builder<GSongCardData_artist, GSongCardData_artistBuilder> {
  _$GSongCardData_artist? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GSongCardData_artistBuilder() {
    GSongCardData_artist._initializeBuilder(this);
  }

  GSongCardData_artistBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongCardData_artist other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongCardData_artist;
  }

  @override
  void update(void Function(GSongCardData_artistBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongCardData_artist build() {
    final _$result = _$v ??
        new _$GSongCardData_artist._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GSongCardData_artist', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GSongCardData_artist', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GSongCardData_artist', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
