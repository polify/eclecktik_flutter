// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artist_card_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GArtistCardVars> _$gArtistCardVarsSerializer =
    new _$GArtistCardVarsSerializer();

class _$GArtistCardVarsSerializer
    implements StructuredSerializer<GArtistCardVars> {
  @override
  final Iterable<Type> types = const [GArtistCardVars, _$GArtistCardVars];
  @override
  final String wireName = 'GArtistCardVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GArtistCardVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GArtistCardVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GArtistCardVarsBuilder().build();
  }
}

class _$GArtistCardVars extends GArtistCardVars {
  factory _$GArtistCardVars([void Function(GArtistCardVarsBuilder)? updates]) =>
      (new GArtistCardVarsBuilder()..update(updates)).build();

  _$GArtistCardVars._() : super._();

  @override
  GArtistCardVars rebuild(void Function(GArtistCardVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GArtistCardVarsBuilder toBuilder() =>
      new GArtistCardVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GArtistCardVars;
  }

  @override
  int get hashCode {
    return 709701015;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GArtistCardVars').toString();
  }
}

class GArtistCardVarsBuilder
    implements Builder<GArtistCardVars, GArtistCardVarsBuilder> {
  _$GArtistCardVars? _$v;

  GArtistCardVarsBuilder();

  @override
  void replace(GArtistCardVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GArtistCardVars;
  }

  @override
  void update(void Function(GArtistCardVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GArtistCardVars build() {
    final _$result = _$v ?? new _$GArtistCardVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
