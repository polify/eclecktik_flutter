// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i1;
import 'package:eclektik/graphql/serializers.gql.dart' as _i2;

part 'song_item_fragment.data.gql.g.dart';

abstract class GSongItem {
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GSongItemData
    implements Built<GSongItemData, GSongItemDataBuilder>, GSongItem {
  GSongItemData._();

  factory GSongItemData([Function(GSongItemDataBuilder b) updates]) =
      _$GSongItemData;

  static void _initializeBuilder(GSongItemDataBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i1.GTime get createdAt;
  _i1.GTime get updatedAt;
  static Serializer<GSongItemData> get serializer => _$gSongItemDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i2.serializers.serializeWith(GSongItemData.serializer, this)
          as Map<String, dynamic>);
  static GSongItemData? fromJson(Map<String, dynamic> json) =>
      _i2.serializers.deserializeWith(GSongItemData.serializer, json);
}
