// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart' as _i2;

part 'randomplaylist_query.data.gql.g.dart';

abstract class GRandomPlaylistData
    implements Built<GRandomPlaylistData, GRandomPlaylistDataBuilder> {
  GRandomPlaylistData._();

  factory GRandomPlaylistData(
      [Function(GRandomPlaylistDataBuilder b) updates]) = _$GRandomPlaylistData;

  static void _initializeBuilder(GRandomPlaylistDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GRandomPlaylistData_randomPlaylist get randomPlaylist;
  static Serializer<GRandomPlaylistData> get serializer =>
      _$gRandomPlaylistDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GRandomPlaylistData.serializer, this)
          as Map<String, dynamic>);
  static GRandomPlaylistData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GRandomPlaylistData.serializer, json);
}

abstract class GRandomPlaylistData_randomPlaylist
    implements
        Built<GRandomPlaylistData_randomPlaylist,
            GRandomPlaylistData_randomPlaylistBuilder> {
  GRandomPlaylistData_randomPlaylist._();

  factory GRandomPlaylistData_randomPlaylist(
          [Function(GRandomPlaylistData_randomPlaylistBuilder b) updates]) =
      _$GRandomPlaylistData_randomPlaylist;

  static void _initializeBuilder(GRandomPlaylistData_randomPlaylistBuilder b) =>
      b..G__typename = 'Playlist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  BuiltList<GRandomPlaylistData_randomPlaylist_songs> get songs;
  static Serializer<GRandomPlaylistData_randomPlaylist> get serializer =>
      _$gRandomPlaylistDataRandomPlaylistSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GRandomPlaylistData_randomPlaylist.serializer, this)
      as Map<String, dynamic>);
  static GRandomPlaylistData_randomPlaylist? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GRandomPlaylistData_randomPlaylist.serializer, json);
}

abstract class GRandomPlaylistData_randomPlaylist_songs
    implements
        Built<GRandomPlaylistData_randomPlaylist_songs,
            GRandomPlaylistData_randomPlaylist_songsBuilder>,
        _i2.GSongCard {
  GRandomPlaylistData_randomPlaylist_songs._();

  factory GRandomPlaylistData_randomPlaylist_songs(
      [Function(GRandomPlaylistData_randomPlaylist_songsBuilder b)
          updates]) = _$GRandomPlaylistData_randomPlaylist_songs;

  static void _initializeBuilder(
          GRandomPlaylistData_randomPlaylist_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  String get duration;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  GRandomPlaylistData_randomPlaylist_songs_signedUrl? get signedUrl;
  GRandomPlaylistData_randomPlaylist_songs_album? get album;
  GRandomPlaylistData_randomPlaylist_songs_artist? get artist;
  static Serializer<GRandomPlaylistData_randomPlaylist_songs> get serializer =>
      _$gRandomPlaylistDataRandomPlaylistSongsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GRandomPlaylistData_randomPlaylist_songs.serializer, this)
      as Map<String, dynamic>);
  static GRandomPlaylistData_randomPlaylist_songs? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GRandomPlaylistData_randomPlaylist_songs.serializer, json);
}

abstract class GRandomPlaylistData_randomPlaylist_songs_signedUrl
    implements
        Built<GRandomPlaylistData_randomPlaylist_songs_signedUrl,
            GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder>,
        _i2.GSongCard_signedUrl {
  GRandomPlaylistData_randomPlaylist_songs_signedUrl._();

  factory GRandomPlaylistData_randomPlaylist_songs_signedUrl(
      [Function(GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder b)
          updates]) = _$GRandomPlaylistData_randomPlaylist_songs_signedUrl;

  static void _initializeBuilder(
          GRandomPlaylistData_randomPlaylist_songs_signedUrlBuilder b) =>
      b..G__typename = 'S3SignedUrl';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get signedUrl;
  static Serializer<GRandomPlaylistData_randomPlaylist_songs_signedUrl>
      get serializer =>
          _$gRandomPlaylistDataRandomPlaylistSongsSignedUrlSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GRandomPlaylistData_randomPlaylist_songs_signedUrl.serializer, this)
      as Map<String, dynamic>);
  static GRandomPlaylistData_randomPlaylist_songs_signedUrl? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GRandomPlaylistData_randomPlaylist_songs_signedUrl.serializer, json);
}

abstract class GRandomPlaylistData_randomPlaylist_songs_album
    implements
        Built<GRandomPlaylistData_randomPlaylist_songs_album,
            GRandomPlaylistData_randomPlaylist_songs_albumBuilder>,
        _i2.GSongCard_album {
  GRandomPlaylistData_randomPlaylist_songs_album._();

  factory GRandomPlaylistData_randomPlaylist_songs_album(
      [Function(GRandomPlaylistData_randomPlaylist_songs_albumBuilder b)
          updates]) = _$GRandomPlaylistData_randomPlaylist_songs_album;

  static void _initializeBuilder(
          GRandomPlaylistData_randomPlaylist_songs_albumBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GRandomPlaylistData_randomPlaylist_songs_album>
      get serializer => _$gRandomPlaylistDataRandomPlaylistSongsAlbumSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GRandomPlaylistData_randomPlaylist_songs_album.serializer, this)
      as Map<String, dynamic>);
  static GRandomPlaylistData_randomPlaylist_songs_album? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GRandomPlaylistData_randomPlaylist_songs_album.serializer, json);
}

abstract class GRandomPlaylistData_randomPlaylist_songs_artist
    implements
        Built<GRandomPlaylistData_randomPlaylist_songs_artist,
            GRandomPlaylistData_randomPlaylist_songs_artistBuilder>,
        _i2.GSongCard_artist {
  GRandomPlaylistData_randomPlaylist_songs_artist._();

  factory GRandomPlaylistData_randomPlaylist_songs_artist(
      [Function(GRandomPlaylistData_randomPlaylist_songs_artistBuilder b)
          updates]) = _$GRandomPlaylistData_randomPlaylist_songs_artist;

  static void _initializeBuilder(
          GRandomPlaylistData_randomPlaylist_songs_artistBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GRandomPlaylistData_randomPlaylist_songs_artist>
      get serializer =>
          _$gRandomPlaylistDataRandomPlaylistSongsArtistSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
          GRandomPlaylistData_randomPlaylist_songs_artist.serializer, this)
      as Map<String, dynamic>);
  static GRandomPlaylistData_randomPlaylist_songs_artist? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(
          GRandomPlaylistData_randomPlaylist_songs_artist.serializer, json);
}
