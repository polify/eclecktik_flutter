// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_card_fragment.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GAlbumCardVars> _$gAlbumCardVarsSerializer =
    new _$GAlbumCardVarsSerializer();

class _$GAlbumCardVarsSerializer
    implements StructuredSerializer<GAlbumCardVars> {
  @override
  final Iterable<Type> types = const [GAlbumCardVars, _$GAlbumCardVars];
  @override
  final String wireName = 'GAlbumCardVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GAlbumCardVars object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object?>[];
  }

  @override
  GAlbumCardVars deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new GAlbumCardVarsBuilder().build();
  }
}

class _$GAlbumCardVars extends GAlbumCardVars {
  factory _$GAlbumCardVars([void Function(GAlbumCardVarsBuilder)? updates]) =>
      (new GAlbumCardVarsBuilder()..update(updates)).build();

  _$GAlbumCardVars._() : super._();

  @override
  GAlbumCardVars rebuild(void Function(GAlbumCardVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumCardVarsBuilder toBuilder() =>
      new GAlbumCardVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumCardVars;
  }

  @override
  int get hashCode {
    return 545907306;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GAlbumCardVars').toString();
  }
}

class GAlbumCardVarsBuilder
    implements Builder<GAlbumCardVars, GAlbumCardVarsBuilder> {
  _$GAlbumCardVars? _$v;

  GAlbumCardVarsBuilder();

  @override
  void replace(GAlbumCardVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumCardVars;
  }

  @override
  void update(void Function(GAlbumCardVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumCardVars build() {
    final _$result = _$v ?? new _$GAlbumCardVars._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
