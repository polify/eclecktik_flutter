// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:eclektik/graphql/album_item_fragment.ast.gql.dart' as _i2;
import 'package:eclektik/graphql/artist_item_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/song_item_fragment.ast.gql.dart' as _i3;
import 'package:gql/ast.dart' as _i1;

const ArtistCard = _i1.FragmentDefinitionNode(
    name: _i1.NameNode(value: 'ArtistCard'),
    typeCondition: _i1.TypeConditionNode(
        on: _i1.NamedTypeNode(
            name: _i1.NameNode(value: 'Artist'), isNonNull: false)),
    directives: [],
    selectionSet: _i1.SelectionSetNode(selections: [
      _i1.FieldNode(
          name: _i1.NameNode(value: 'albums'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: _i1.SelectionSetNode(selections: [
            _i1.FragmentSpreadNode(
                name: _i1.NameNode(value: 'AlbumItem'), directives: [])
          ])),
      _i1.FieldNode(
          name: _i1.NameNode(value: 'songs'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: _i1.SelectionSetNode(selections: [
            _i1.FragmentSpreadNode(
                name: _i1.NameNode(value: 'SongItem'), directives: [])
          ])),
      _i1.FragmentSpreadNode(
          name: _i1.NameNode(value: 'ArtistItem'), directives: [])
    ]));
const document = _i1.DocumentNode(
    definitions: [ArtistCard, _i2.AlbumItem, _i3.SongItem, _i4.ArtistItem]);
