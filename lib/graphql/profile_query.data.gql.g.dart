// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GProfileData> _$gProfileDataSerializer =
    new _$GProfileDataSerializer();
Serializer<GProfileData_profile> _$gProfileDataProfileSerializer =
    new _$GProfileData_profileSerializer();
Serializer<GProfileData_profile_s3Credential>
    _$gProfileDataProfileS3CredentialSerializer =
    new _$GProfileData_profile_s3CredentialSerializer();
Serializer<GProfileData_profile_devices>
    _$gProfileDataProfileDevicesSerializer =
    new _$GProfileData_profile_devicesSerializer();

class _$GProfileDataSerializer implements StructuredSerializer<GProfileData> {
  @override
  final Iterable<Type> types = const [GProfileData, _$GProfileData];
  @override
  final String wireName = 'GProfileData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GProfileData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'profile',
      serializers.serialize(object.profile,
          specifiedType: const FullType(GProfileData_profile)),
    ];

    return result;
  }

  @override
  GProfileData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GProfileDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'profile':
          result.profile.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GProfileData_profile))!
              as GProfileData_profile);
          break;
      }
    }

    return result.build();
  }
}

class _$GProfileData_profileSerializer
    implements StructuredSerializer<GProfileData_profile> {
  @override
  final Iterable<Type> types = const [
    GProfileData_profile,
    _$GProfileData_profile
  ];
  @override
  final String wireName = 'GProfileData_profile';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GProfileData_profile object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
      'devices',
      serializers.serialize(object.devices,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GProfileData_profile_devices)])),
    ];
    Object? value;
    value = object.s3Credential;
    if (value != null) {
      result
        ..add('s3Credential')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GProfileData_profile_s3Credential)));
    }
    return result;
  }

  @override
  GProfileData_profile deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GProfileData_profileBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 's3Credential':
          result.s3Credential.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(GProfileData_profile_s3Credential))!
              as GProfileData_profile_s3Credential);
          break;
        case 'devices':
          result.devices.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GProfileData_profile_devices)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GProfileData_profile_s3CredentialSerializer
    implements StructuredSerializer<GProfileData_profile_s3Credential> {
  @override
  final Iterable<Type> types = const [
    GProfileData_profile_s3Credential,
    _$GProfileData_profile_s3Credential
  ];
  @override
  final String wireName = 'GProfileData_profile_s3Credential';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GProfileData_profile_s3Credential object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];
    Object? value;
    value = object.keyID;
    if (value != null) {
      result
        ..add('keyID')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.keySecret;
    if (value != null) {
      result
        ..add('keySecret')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GProfileData_profile_s3Credential deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GProfileData_profile_s3CredentialBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'keyID':
          result.keyID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'keySecret':
          result.keySecret = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GProfileData_profile_devicesSerializer
    implements StructuredSerializer<GProfileData_profile_devices> {
  @override
  final Iterable<Type> types = const [
    GProfileData_profile_devices,
    _$GProfileData_profile_devices
  ];
  @override
  final String wireName = 'GProfileData_profile_devices';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GProfileData_profile_devices object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'lastIP',
      serializers.serialize(object.lastIP,
          specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'os',
      serializers.serialize(object.os, specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'userAgent',
      serializers.serialize(object.userAgent,
          specifiedType: const FullType(String)),
      'lastConnection',
      serializers.serialize(object.lastConnection,
          specifiedType: const FullType(_i2.GTime)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];

    return result;
  }

  @override
  GProfileData_profile_devices deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GProfileData_profile_devicesBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastIP':
          result.lastIP = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'os':
          result.os = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userAgent':
          result.userAgent = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastConnection':
          result.lastConnection.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GProfileData extends GProfileData {
  @override
  final String G__typename;
  @override
  final GProfileData_profile profile;

  factory _$GProfileData([void Function(GProfileDataBuilder)? updates]) =>
      (new GProfileDataBuilder()..update(updates)).build();

  _$GProfileData._({required this.G__typename, required this.profile})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GProfileData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(profile, 'GProfileData', 'profile');
  }

  @override
  GProfileData rebuild(void Function(GProfileDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GProfileDataBuilder toBuilder() => new GProfileDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GProfileData &&
        G__typename == other.G__typename &&
        profile == other.profile;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), profile.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GProfileData')
          ..add('G__typename', G__typename)
          ..add('profile', profile))
        .toString();
  }
}

class GProfileDataBuilder
    implements Builder<GProfileData, GProfileDataBuilder> {
  _$GProfileData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GProfileData_profileBuilder? _profile;
  GProfileData_profileBuilder get profile =>
      _$this._profile ??= new GProfileData_profileBuilder();
  set profile(GProfileData_profileBuilder? profile) =>
      _$this._profile = profile;

  GProfileDataBuilder() {
    GProfileData._initializeBuilder(this);
  }

  GProfileDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _profile = $v.profile.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GProfileData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GProfileData;
  }

  @override
  void update(void Function(GProfileDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GProfileData build() {
    _$GProfileData _$result;
    try {
      _$result = _$v ??
          new _$GProfileData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GProfileData', 'G__typename'),
              profile: profile.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'profile';
        profile.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GProfileData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GProfileData_profile extends GProfileData_profile {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;
  @override
  final GProfileData_profile_s3Credential? s3Credential;
  @override
  final BuiltList<GProfileData_profile_devices> devices;

  factory _$GProfileData_profile(
          [void Function(GProfileData_profileBuilder)? updates]) =>
      (new GProfileData_profileBuilder()..update(updates)).build();

  _$GProfileData_profile._(
      {required this.G__typename,
      required this.id,
      required this.name,
      required this.createdAt,
      required this.updatedAt,
      this.s3Credential,
      required this.devices})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GProfileData_profile', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GProfileData_profile', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GProfileData_profile', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GProfileData_profile', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GProfileData_profile', 'updatedAt');
    BuiltValueNullFieldError.checkNotNull(
        devices, 'GProfileData_profile', 'devices');
  }

  @override
  GProfileData_profile rebuild(
          void Function(GProfileData_profileBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GProfileData_profileBuilder toBuilder() =>
      new GProfileData_profileBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GProfileData_profile &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        s3Credential == other.s3Credential &&
        devices == other.devices;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                        name.hashCode),
                    createdAt.hashCode),
                updatedAt.hashCode),
            s3Credential.hashCode),
        devices.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GProfileData_profile')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('s3Credential', s3Credential)
          ..add('devices', devices))
        .toString();
  }
}

class GProfileData_profileBuilder
    implements Builder<GProfileData_profile, GProfileData_profileBuilder> {
  _$GProfileData_profile? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GProfileData_profile_s3CredentialBuilder? _s3Credential;
  GProfileData_profile_s3CredentialBuilder get s3Credential =>
      _$this._s3Credential ??= new GProfileData_profile_s3CredentialBuilder();
  set s3Credential(GProfileData_profile_s3CredentialBuilder? s3Credential) =>
      _$this._s3Credential = s3Credential;

  ListBuilder<GProfileData_profile_devices>? _devices;
  ListBuilder<GProfileData_profile_devices> get devices =>
      _$this._devices ??= new ListBuilder<GProfileData_profile_devices>();
  set devices(ListBuilder<GProfileData_profile_devices>? devices) =>
      _$this._devices = devices;

  GProfileData_profileBuilder() {
    GProfileData_profile._initializeBuilder(this);
  }

  GProfileData_profileBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _s3Credential = $v.s3Credential?.toBuilder();
      _devices = $v.devices.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GProfileData_profile other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GProfileData_profile;
  }

  @override
  void update(void Function(GProfileData_profileBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GProfileData_profile build() {
    _$GProfileData_profile _$result;
    try {
      _$result = _$v ??
          new _$GProfileData_profile._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GProfileData_profile', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GProfileData_profile', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GProfileData_profile', 'name'),
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build(),
              s3Credential: _s3Credential?.build(),
              devices: devices.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
        _$failedField = 's3Credential';
        _s3Credential?.build();
        _$failedField = 'devices';
        devices.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GProfileData_profile', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GProfileData_profile_s3Credential
    extends GProfileData_profile_s3Credential {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String? keyID;
  @override
  final String? keySecret;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;

  factory _$GProfileData_profile_s3Credential(
          [void Function(GProfileData_profile_s3CredentialBuilder)? updates]) =>
      (new GProfileData_profile_s3CredentialBuilder()..update(updates)).build();

  _$GProfileData_profile_s3Credential._(
      {required this.G__typename,
      required this.id,
      this.keyID,
      this.keySecret,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GProfileData_profile_s3Credential', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GProfileData_profile_s3Credential', 'id');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GProfileData_profile_s3Credential', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GProfileData_profile_s3Credential', 'updatedAt');
  }

  @override
  GProfileData_profile_s3Credential rebuild(
          void Function(GProfileData_profile_s3CredentialBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GProfileData_profile_s3CredentialBuilder toBuilder() =>
      new GProfileData_profile_s3CredentialBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GProfileData_profile_s3Credential &&
        G__typename == other.G__typename &&
        id == other.id &&
        keyID == other.keyID &&
        keySecret == other.keySecret &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    keyID.hashCode),
                keySecret.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GProfileData_profile_s3Credential')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('keyID', keyID)
          ..add('keySecret', keySecret)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GProfileData_profile_s3CredentialBuilder
    implements
        Builder<GProfileData_profile_s3Credential,
            GProfileData_profile_s3CredentialBuilder> {
  _$GProfileData_profile_s3Credential? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _keyID;
  String? get keyID => _$this._keyID;
  set keyID(String? keyID) => _$this._keyID = keyID;

  String? _keySecret;
  String? get keySecret => _$this._keySecret;
  set keySecret(String? keySecret) => _$this._keySecret = keySecret;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GProfileData_profile_s3CredentialBuilder() {
    GProfileData_profile_s3Credential._initializeBuilder(this);
  }

  GProfileData_profile_s3CredentialBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _keyID = $v.keyID;
      _keySecret = $v.keySecret;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GProfileData_profile_s3Credential other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GProfileData_profile_s3Credential;
  }

  @override
  void update(
      void Function(GProfileData_profile_s3CredentialBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GProfileData_profile_s3Credential build() {
    _$GProfileData_profile_s3Credential _$result;
    try {
      _$result = _$v ??
          new _$GProfileData_profile_s3Credential._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GProfileData_profile_s3Credential', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GProfileData_profile_s3Credential', 'id'),
              keyID: keyID,
              keySecret: keySecret,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GProfileData_profile_s3Credential', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GProfileData_profile_devices extends GProfileData_profile_devices {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String lastIP;
  @override
  final String name;
  @override
  final String os;
  @override
  final String type;
  @override
  final String userAgent;
  @override
  final _i2.GTime lastConnection;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;

  factory _$GProfileData_profile_devices(
          [void Function(GProfileData_profile_devicesBuilder)? updates]) =>
      (new GProfileData_profile_devicesBuilder()..update(updates)).build();

  _$GProfileData_profile_devices._(
      {required this.G__typename,
      required this.id,
      required this.lastIP,
      required this.name,
      required this.os,
      required this.type,
      required this.userAgent,
      required this.lastConnection,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GProfileData_profile_devices', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GProfileData_profile_devices', 'id');
    BuiltValueNullFieldError.checkNotNull(
        lastIP, 'GProfileData_profile_devices', 'lastIP');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GProfileData_profile_devices', 'name');
    BuiltValueNullFieldError.checkNotNull(
        os, 'GProfileData_profile_devices', 'os');
    BuiltValueNullFieldError.checkNotNull(
        type, 'GProfileData_profile_devices', 'type');
    BuiltValueNullFieldError.checkNotNull(
        userAgent, 'GProfileData_profile_devices', 'userAgent');
    BuiltValueNullFieldError.checkNotNull(
        lastConnection, 'GProfileData_profile_devices', 'lastConnection');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GProfileData_profile_devices', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GProfileData_profile_devices', 'updatedAt');
  }

  @override
  GProfileData_profile_devices rebuild(
          void Function(GProfileData_profile_devicesBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GProfileData_profile_devicesBuilder toBuilder() =>
      new GProfileData_profile_devicesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GProfileData_profile_devices &&
        G__typename == other.G__typename &&
        id == other.id &&
        lastIP == other.lastIP &&
        name == other.name &&
        os == other.os &&
        type == other.type &&
        userAgent == other.userAgent &&
        lastConnection == other.lastConnection &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, G__typename.hashCode),
                                        id.hashCode),
                                    lastIP.hashCode),
                                name.hashCode),
                            os.hashCode),
                        type.hashCode),
                    userAgent.hashCode),
                lastConnection.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GProfileData_profile_devices')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('lastIP', lastIP)
          ..add('name', name)
          ..add('os', os)
          ..add('type', type)
          ..add('userAgent', userAgent)
          ..add('lastConnection', lastConnection)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GProfileData_profile_devicesBuilder
    implements
        Builder<GProfileData_profile_devices,
            GProfileData_profile_devicesBuilder> {
  _$GProfileData_profile_devices? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _lastIP;
  String? get lastIP => _$this._lastIP;
  set lastIP(String? lastIP) => _$this._lastIP = lastIP;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _os;
  String? get os => _$this._os;
  set os(String? os) => _$this._os = os;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  String? _userAgent;
  String? get userAgent => _$this._userAgent;
  set userAgent(String? userAgent) => _$this._userAgent = userAgent;

  _i2.GTimeBuilder? _lastConnection;
  _i2.GTimeBuilder get lastConnection =>
      _$this._lastConnection ??= new _i2.GTimeBuilder();
  set lastConnection(_i2.GTimeBuilder? lastConnection) =>
      _$this._lastConnection = lastConnection;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GProfileData_profile_devicesBuilder() {
    GProfileData_profile_devices._initializeBuilder(this);
  }

  GProfileData_profile_devicesBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _lastIP = $v.lastIP;
      _name = $v.name;
      _os = $v.os;
      _type = $v.type;
      _userAgent = $v.userAgent;
      _lastConnection = $v.lastConnection.toBuilder();
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GProfileData_profile_devices other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GProfileData_profile_devices;
  }

  @override
  void update(void Function(GProfileData_profile_devicesBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GProfileData_profile_devices build() {
    _$GProfileData_profile_devices _$result;
    try {
      _$result = _$v ??
          new _$GProfileData_profile_devices._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GProfileData_profile_devices', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GProfileData_profile_devices', 'id'),
              lastIP: BuiltValueNullFieldError.checkNotNull(
                  lastIP, 'GProfileData_profile_devices', 'lastIP'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GProfileData_profile_devices', 'name'),
              os: BuiltValueNullFieldError.checkNotNull(
                  os, 'GProfileData_profile_devices', 'os'),
              type: BuiltValueNullFieldError.checkNotNull(
                  type, 'GProfileData_profile_devices', 'type'),
              userAgent: BuiltValueNullFieldError.checkNotNull(
                  userAgent, 'GProfileData_profile_devices', 'userAgent'),
              lastConnection: lastConnection.build(),
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'lastConnection';
        lastConnection.build();
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GProfileData_profile_devices', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
