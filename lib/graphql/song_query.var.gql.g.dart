// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'song_query.var.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSongVars> _$gSongVarsSerializer = new _$GSongVarsSerializer();

class _$GSongVarsSerializer implements StructuredSerializer<GSongVars> {
  @override
  final Iterable<Type> types = const [GSongVars, _$GSongVars];
  @override
  final String wireName = 'GSongVars';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSongVars object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GSongVars deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSongVarsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GSongVars extends GSongVars {
  @override
  final String id;

  factory _$GSongVars([void Function(GSongVarsBuilder)? updates]) =>
      (new GSongVarsBuilder()..update(updates)).build();

  _$GSongVars._({required this.id}) : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'GSongVars', 'id');
  }

  @override
  GSongVars rebuild(void Function(GSongVarsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSongVarsBuilder toBuilder() => new GSongVarsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSongVars && id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(0, id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSongVars')..add('id', id)).toString();
  }
}

class GSongVarsBuilder implements Builder<GSongVars, GSongVarsBuilder> {
  _$GSongVars? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  GSongVarsBuilder();

  GSongVarsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSongVars other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSongVars;
  }

  @override
  void update(void Function(GSongVarsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSongVars build() {
    final _$result = _$v ??
        new _$GSongVars._(
            id: BuiltValueNullFieldError.checkNotNull(id, 'GSongVars', 'id'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
