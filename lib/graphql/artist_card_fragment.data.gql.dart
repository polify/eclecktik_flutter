// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart' as _i3;
import 'package:eclektik/graphql/artist_item_fragment.data.gql.dart' as _i1;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i2;
import 'package:eclektik/graphql/serializers.gql.dart' as _i5;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart' as _i4;

part 'artist_card_fragment.data.gql.g.dart';

abstract class GArtistCard implements _i1.GArtistItem {
  String get G__typename;
  BuiltList<GArtistCard_albums> get albums;
  BuiltList<GArtistCard_songs> get songs;
  String get id;
  String get name;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GArtistCard_albums implements _i3.GAlbumItem {
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GArtistCard_songs implements _i4.GSongItem {
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GArtistCardData
    implements
        Built<GArtistCardData, GArtistCardDataBuilder>,
        GArtistCard,
        _i1.GArtistItem {
  GArtistCardData._();

  factory GArtistCardData([Function(GArtistCardDataBuilder b) updates]) =
      _$GArtistCardData;

  static void _initializeBuilder(GArtistCardDataBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  BuiltList<GArtistCardData_albums> get albums;
  BuiltList<GArtistCardData_songs> get songs;
  String get id;
  String get name;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  static Serializer<GArtistCardData> get serializer =>
      _$gArtistCardDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i5.serializers.serializeWith(GArtistCardData.serializer, this)
          as Map<String, dynamic>);
  static GArtistCardData? fromJson(Map<String, dynamic> json) =>
      _i5.serializers.deserializeWith(GArtistCardData.serializer, json);
}

abstract class GArtistCardData_albums
    implements
        Built<GArtistCardData_albums, GArtistCardData_albumsBuilder>,
        GArtistCard_albums,
        _i3.GAlbumItem {
  GArtistCardData_albums._();

  factory GArtistCardData_albums(
          [Function(GArtistCardData_albumsBuilder b) updates]) =
      _$GArtistCardData_albums;

  static void _initializeBuilder(GArtistCardData_albumsBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  static Serializer<GArtistCardData_albums> get serializer =>
      _$gArtistCardDataAlbumsSerializer;
  Map<String, dynamic> toJson() =>
      (_i5.serializers.serializeWith(GArtistCardData_albums.serializer, this)
          as Map<String, dynamic>);
  static GArtistCardData_albums? fromJson(Map<String, dynamic> json) =>
      _i5.serializers.deserializeWith(GArtistCardData_albums.serializer, json);
}

abstract class GArtistCardData_songs
    implements
        Built<GArtistCardData_songs, GArtistCardData_songsBuilder>,
        GArtistCard_songs,
        _i4.GSongItem {
  GArtistCardData_songs._();

  factory GArtistCardData_songs(
          [Function(GArtistCardData_songsBuilder b) updates]) =
      _$GArtistCardData_songs;

  static void _initializeBuilder(GArtistCardData_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  static Serializer<GArtistCardData_songs> get serializer =>
      _$gArtistCardDataSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i5.serializers.serializeWith(GArtistCardData_songs.serializer, this)
          as Map<String, dynamic>);
  static GArtistCardData_songs? fromJson(Map<String, dynamic> json) =>
      _i5.serializers.deserializeWith(GArtistCardData_songs.serializer, json);
}
