// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:gql_code_builder/src/serializers/default_scalar_serializer.dart'
    as _i2;

part 'schema.schema.gql.g.dart';

abstract class GItemsInput implements Built<GItemsInput, GItemsInputBuilder> {
  GItemsInput._();

  factory GItemsInput([Function(GItemsInputBuilder b) updates]) = _$GItemsInput;

  BuiltList<String>? get ids;
  String? get collectionName;
  static Serializer<GItemsInput> get serializer => _$gItemsInputSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GItemsInput.serializer, this)
          as Map<String, dynamic>);
  static GItemsInput? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GItemsInput.serializer, json);
}

abstract class GNewUser implements Built<GNewUser, GNewUserBuilder> {
  GNewUser._();

  factory GNewUser([Function(GNewUserBuilder b) updates]) = _$GNewUser;

  String get name;
  String get password;
  static Serializer<GNewUser> get serializer => _$gNewUserSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GNewUser.serializer, this)
          as Map<String, dynamic>);
  static GNewUser? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GNewUser.serializer, json);
}

abstract class GPaginationInput
    implements Built<GPaginationInput, GPaginationInputBuilder> {
  GPaginationInput._();

  factory GPaginationInput([Function(GPaginationInputBuilder b) updates]) =
      _$GPaginationInput;

  int? get count;
  int? get page;
  static Serializer<GPaginationInput> get serializer =>
      _$gPaginationInputSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GPaginationInput.serializer, this)
          as Map<String, dynamic>);
  static GPaginationInput? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GPaginationInput.serializer, json);
}

abstract class GS3CredentialCreateInput
    implements
        Built<GS3CredentialCreateInput, GS3CredentialCreateInputBuilder> {
  GS3CredentialCreateInput._();

  factory GS3CredentialCreateInput(
          [Function(GS3CredentialCreateInputBuilder b) updates]) =
      _$GS3CredentialCreateInput;

  String get keyId;
  String get keySecret;
  String get bucketHost;
  String get bucketName;
  String? get clientRegion;
  String? get bucketRegion;
  static Serializer<GS3CredentialCreateInput> get serializer =>
      _$gS3CredentialCreateInputSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GS3CredentialCreateInput.serializer, this)
          as Map<String, dynamic>);
  static GS3CredentialCreateInput? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GS3CredentialCreateInput.serializer, json);
}

abstract class GS3CredentialUpdateInput
    implements
        Built<GS3CredentialUpdateInput, GS3CredentialUpdateInputBuilder> {
  GS3CredentialUpdateInput._();

  factory GS3CredentialUpdateInput(
          [Function(GS3CredentialUpdateInputBuilder b) updates]) =
      _$GS3CredentialUpdateInput;

  String? get keyId;
  String? get keySecret;
  String? get bucketHost;
  String? get bucketName;
  static Serializer<GS3CredentialUpdateInput> get serializer =>
      _$gS3CredentialUpdateInputSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GS3CredentialUpdateInput.serializer, this)
          as Map<String, dynamic>);
  static GS3CredentialUpdateInput? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GS3CredentialUpdateInput.serializer, json);
}

abstract class GSearchSongInput
    implements Built<GSearchSongInput, GSearchSongInputBuilder> {
  GSearchSongInput._();

  factory GSearchSongInput([Function(GSearchSongInputBuilder b) updates]) =
      _$GSearchSongInput;

  String? get artistName;
  String? get albumName;
  String? get songName;
  bool? get random;
  static Serializer<GSearchSongInput> get serializer =>
      _$gSearchSongInputSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSearchSongInput.serializer, this)
          as Map<String, dynamic>);
  static GSearchSongInput? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSearchSongInput.serializer, json);
}

abstract class GTime implements Built<GTime, GTimeBuilder> {
  GTime._();

  factory GTime([String? value]) =>
      _$GTime((b) => value != null ? (b..value = value) : b);

  String get value;
  @BuiltValueSerializer(custom: true)
  static Serializer<GTime> get serializer => _i2.DefaultScalarSerializer<GTime>(
      (Object serialized) => GTime((serialized as String?)));
}
