// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart' as _i4;
import 'package:eclektik/graphql/artist_item_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart' as _i5;

part 'search_query.data.gql.g.dart';

abstract class GSearchData implements Built<GSearchData, GSearchDataBuilder> {
  GSearchData._();

  factory GSearchData([Function(GSearchDataBuilder b) updates]) = _$GSearchData;

  static void _initializeBuilder(GSearchDataBuilder b) =>
      b..G__typename = 'Query';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  GSearchData_search get search;
  static Serializer<GSearchData> get serializer => _$gSearchDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSearchData.serializer, this)
          as Map<String, dynamic>);
  static GSearchData? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSearchData.serializer, json);
}

abstract class GSearchData_search
    implements Built<GSearchData_search, GSearchData_searchBuilder> {
  GSearchData_search._();

  factory GSearchData_search([Function(GSearchData_searchBuilder b) updates]) =
      _$GSearchData_search;

  static void _initializeBuilder(GSearchData_searchBuilder b) =>
      b..G__typename = 'SearchResults';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int? get total;
  GSearchData_search_artists? get artists;
  GSearchData_search_albums? get albums;
  GSearchData_search_songs? get songs;
  static Serializer<GSearchData_search> get serializer =>
      _$gSearchDataSearchSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSearchData_search.serializer, this)
          as Map<String, dynamic>);
  static GSearchData_search? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSearchData_search.serializer, json);
}

abstract class GSearchData_search_artists
    implements
        Built<GSearchData_search_artists, GSearchData_search_artistsBuilder> {
  GSearchData_search_artists._();

  factory GSearchData_search_artists(
          [Function(GSearchData_search_artistsBuilder b) updates]) =
      _$GSearchData_search_artists;

  static void _initializeBuilder(GSearchData_search_artistsBuilder b) =>
      b..G__typename = 'ArtistResults';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int? get total;
  BuiltList<GSearchData_search_artists_artists> get artists;
  static Serializer<GSearchData_search_artists> get serializer =>
      _$gSearchDataSearchArtistsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GSearchData_search_artists.serializer, this) as Map<String, dynamic>);
  static GSearchData_search_artists? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSearchData_search_artists.serializer, json);
}

abstract class GSearchData_search_artists_artists
    implements
        Built<GSearchData_search_artists_artists,
            GSearchData_search_artists_artistsBuilder>,
        _i2.GArtistItem {
  GSearchData_search_artists_artists._();

  factory GSearchData_search_artists_artists(
          [Function(GSearchData_search_artists_artistsBuilder b) updates]) =
      _$GSearchData_search_artists_artists;

  static void _initializeBuilder(GSearchData_search_artists_artistsBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GSearchData_search_artists_artists> get serializer =>
      _$gSearchDataSearchArtistsArtistsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GSearchData_search_artists_artists.serializer, this)
      as Map<String, dynamic>);
  static GSearchData_search_artists_artists? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSearchData_search_artists_artists.serializer, json);
}

abstract class GSearchData_search_albums
    implements
        Built<GSearchData_search_albums, GSearchData_search_albumsBuilder> {
  GSearchData_search_albums._();

  factory GSearchData_search_albums(
          [Function(GSearchData_search_albumsBuilder b) updates]) =
      _$GSearchData_search_albums;

  static void _initializeBuilder(GSearchData_search_albumsBuilder b) =>
      b..G__typename = 'AlbumResults';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int? get total;
  BuiltList<GSearchData_search_albums_albums> get albums;
  static Serializer<GSearchData_search_albums> get serializer =>
      _$gSearchDataSearchAlbumsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSearchData_search_albums.serializer, this)
          as Map<String, dynamic>);
  static GSearchData_search_albums? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSearchData_search_albums.serializer, json);
}

abstract class GSearchData_search_albums_albums
    implements
        Built<GSearchData_search_albums_albums,
            GSearchData_search_albums_albumsBuilder>,
        _i4.GAlbumItem {
  GSearchData_search_albums_albums._();

  factory GSearchData_search_albums_albums(
          [Function(GSearchData_search_albums_albumsBuilder b) updates]) =
      _$GSearchData_search_albums_albums;

  static void _initializeBuilder(GSearchData_search_albums_albumsBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GSearchData_search_albums_albums> get serializer =>
      _$gSearchDataSearchAlbumsAlbumsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers
          .serializeWith(GSearchData_search_albums_albums.serializer, this)
      as Map<String, dynamic>);
  static GSearchData_search_albums_albums? fromJson(
          Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSearchData_search_albums_albums.serializer, json);
}

abstract class GSearchData_search_songs
    implements
        Built<GSearchData_search_songs, GSearchData_search_songsBuilder> {
  GSearchData_search_songs._();

  factory GSearchData_search_songs(
          [Function(GSearchData_search_songsBuilder b) updates]) =
      _$GSearchData_search_songs;

  static void _initializeBuilder(GSearchData_search_songsBuilder b) =>
      b..G__typename = 'SongResults';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  int? get total;
  BuiltList<GSearchData_search_songs_songs> get songs;
  static Serializer<GSearchData_search_songs> get serializer =>
      _$gSearchDataSearchSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSearchData_search_songs.serializer, this)
          as Map<String, dynamic>);
  static GSearchData_search_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSearchData_search_songs.serializer, json);
}

abstract class GSearchData_search_songs_songs
    implements
        Built<GSearchData_search_songs_songs,
            GSearchData_search_songs_songsBuilder>,
        _i5.GSongItem {
  GSearchData_search_songs_songs._();

  factory GSearchData_search_songs_songs(
          [Function(GSearchData_search_songs_songsBuilder b) updates]) =
      _$GSearchData_search_songs_songs;

  static void _initializeBuilder(GSearchData_search_songs_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  String? get coverUrl;
  _i3.GTime get createdAt;
  _i3.GTime get updatedAt;
  static Serializer<GSearchData_search_songs_songs> get serializer =>
      _$gSearchDataSearchSongsSongsSerializer;
  Map<String, dynamic> toJson() => (_i1.serializers.serializeWith(
      GSearchData_search_songs_songs.serializer, this) as Map<String, dynamic>);
  static GSearchData_search_songs_songs? fromJson(Map<String, dynamic> json) =>
      _i1.serializers
          .deserializeWith(GSearchData_search_songs_songs.serializer, json);
}
