// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'artist_item_fragment.var.gql.g.dart';

abstract class GArtistItemVars
    implements Built<GArtistItemVars, GArtistItemVarsBuilder> {
  GArtistItemVars._();

  factory GArtistItemVars([Function(GArtistItemVarsBuilder b) updates]) =
      _$GArtistItemVars;

  static Serializer<GArtistItemVars> get serializer =>
      _$gArtistItemVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GArtistItemVars.serializer, this)
          as Map<String, dynamic>);
  static GArtistItemVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GArtistItemVars.serializer, json);
}
