// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart' as _i1;
import 'package:eclektik/graphql/schema.schema.gql.dart' as _i2;
import 'package:eclektik/graphql/serializers.gql.dart' as _i3;

part 'album_card_fragment.data.gql.g.dart';

abstract class GAlbumCard implements _i1.GAlbumItem {
  String get G__typename;
  String get s3Key;
  BuiltList<GAlbumCard_songs> get songs;
  BuiltList<GAlbumCard_artists> get artists;
  String get id;
  String get name;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  Map<String, dynamic> toJson();
}

abstract class GAlbumCard_songs {
  String get G__typename;
  String get id;
  String get title;
  Map<String, dynamic> toJson();
}

abstract class GAlbumCard_artists {
  String get G__typename;
  String get id;
  String get name;
  Map<String, dynamic> toJson();
}

abstract class GAlbumCardData
    implements
        Built<GAlbumCardData, GAlbumCardDataBuilder>,
        GAlbumCard,
        _i1.GAlbumItem {
  GAlbumCardData._();

  factory GAlbumCardData([Function(GAlbumCardDataBuilder b) updates]) =
      _$GAlbumCardData;

  static void _initializeBuilder(GAlbumCardDataBuilder b) =>
      b..G__typename = 'Album';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get s3Key;
  BuiltList<GAlbumCardData_songs> get songs;
  BuiltList<GAlbumCardData_artists> get artists;
  String get id;
  String get name;
  String? get coverUrl;
  _i2.GTime get createdAt;
  _i2.GTime get updatedAt;
  static Serializer<GAlbumCardData> get serializer =>
      _$gAlbumCardDataSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GAlbumCardData.serializer, this)
          as Map<String, dynamic>);
  static GAlbumCardData? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GAlbumCardData.serializer, json);
}

abstract class GAlbumCardData_songs
    implements
        Built<GAlbumCardData_songs, GAlbumCardData_songsBuilder>,
        GAlbumCard_songs {
  GAlbumCardData_songs._();

  factory GAlbumCardData_songs(
          [Function(GAlbumCardData_songsBuilder b) updates]) =
      _$GAlbumCardData_songs;

  static void _initializeBuilder(GAlbumCardData_songsBuilder b) =>
      b..G__typename = 'Song';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get title;
  static Serializer<GAlbumCardData_songs> get serializer =>
      _$gAlbumCardDataSongsSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GAlbumCardData_songs.serializer, this)
          as Map<String, dynamic>);
  static GAlbumCardData_songs? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GAlbumCardData_songs.serializer, json);
}

abstract class GAlbumCardData_artists
    implements
        Built<GAlbumCardData_artists, GAlbumCardData_artistsBuilder>,
        GAlbumCard_artists {
  GAlbumCardData_artists._();

  factory GAlbumCardData_artists(
          [Function(GAlbumCardData_artistsBuilder b) updates]) =
      _$GAlbumCardData_artists;

  static void _initializeBuilder(GAlbumCardData_artistsBuilder b) =>
      b..G__typename = 'Artist';
  @BuiltValueField(wireName: '__typename')
  String get G__typename;
  String get id;
  String get name;
  static Serializer<GAlbumCardData_artists> get serializer =>
      _$gAlbumCardDataArtistsSerializer;
  Map<String, dynamic> toJson() =>
      (_i3.serializers.serializeWith(GAlbumCardData_artists.serializer, this)
          as Map<String, dynamic>);
  static GAlbumCardData_artists? fromJson(Map<String, dynamic> json) =>
      _i3.serializers.deserializeWith(GAlbumCardData_artists.serializer, json);
}
