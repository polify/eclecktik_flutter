// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:eclektik/graphql/songs_query.ast.gql.dart' as _i5;
import 'package:eclektik/graphql/songs_query.data.gql.dart' as _i2;
import 'package:eclektik/graphql/songs_query.var.gql.dart' as _i3;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql_exec/gql_exec.dart' as _i4;

part 'songs_query.req.gql.g.dart';

abstract class GSongsReq
    implements
        Built<GSongsReq, GSongsReqBuilder>,
        _i1.OperationRequest<_i2.GSongsData, _i3.GSongsVars> {
  GSongsReq._();

  factory GSongsReq([Function(GSongsReqBuilder b) updates]) = _$GSongsReq;

  static void _initializeBuilder(GSongsReqBuilder b) => b
    ..operation = _i4.Operation(document: _i5.document, operationName: 'Songs')
    ..executeOnListen = true;
  _i3.GSongsVars get vars;
  _i4.Operation get operation;
  _i4.Request get execRequest =>
      _i4.Request(operation: operation, variables: vars.toJson());
  String? get requestId;
  @BuiltValueField(serialize: false)
  _i2.GSongsData? Function(_i2.GSongsData?, _i2.GSongsData?)? get updateResult;
  _i2.GSongsData? get optimisticResponse;
  String? get updateCacheHandlerKey;
  Map<String, dynamic>? get updateCacheHandlerContext;
  _i1.FetchPolicy? get fetchPolicy;
  bool get executeOnListen;
  @override
  _i2.GSongsData? parseData(Map<String, dynamic> json) =>
      _i2.GSongsData.fromJson(json);
  static Serializer<GSongsReq> get serializer => _$gSongsReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GSongsReq.serializer, this)
          as Map<String, dynamic>);
  static GSongsReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GSongsReq.serializer, json);
}
