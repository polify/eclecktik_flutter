// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/serializers.gql.dart' as _i1;

part 'songs_query.var.gql.g.dart';

abstract class GSongsVars implements Built<GSongsVars, GSongsVarsBuilder> {
  GSongsVars._();

  factory GSongsVars([Function(GSongsVarsBuilder b) updates]) = _$GSongsVars;

  int? get limit;
  int? get offset;
  static Serializer<GSongsVars> get serializer => _$gSongsVarsSerializer;
  Map<String, dynamic> toJson() =>
      (_i1.serializers.serializeWith(GSongsVars.serializer, this)
          as Map<String, dynamic>);
  static GSongsVars? fromJson(Map<String, dynamic> json) =>
      _i1.serializers.deserializeWith(GSongsVars.serializer, json);
}
