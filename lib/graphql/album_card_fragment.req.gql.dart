// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/album_card_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/album_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/album_card_fragment.var.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;

part 'album_card_fragment.req.gql.g.dart';

abstract class GAlbumCardReq
    implements
        Built<GAlbumCardReq, GAlbumCardReqBuilder>,
        _i1.FragmentRequest<_i2.GAlbumCardData, _i3.GAlbumCardVars> {
  GAlbumCardReq._();

  factory GAlbumCardReq([Function(GAlbumCardReqBuilder b) updates]) =
      _$GAlbumCardReq;

  static void _initializeBuilder(GAlbumCardReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'AlbumCard';
  _i3.GAlbumCardVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GAlbumCardData? parseData(Map<String, dynamic> json) =>
      _i2.GAlbumCardData.fromJson(json);
  static Serializer<GAlbumCardReq> get serializer => _$gAlbumCardReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GAlbumCardReq.serializer, this)
          as Map<String, dynamic>);
  static GAlbumCardReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GAlbumCardReq.serializer, json);
}
