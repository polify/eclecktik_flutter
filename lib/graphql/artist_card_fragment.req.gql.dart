// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:eclektik/graphql/artist_card_fragment.ast.gql.dart' as _i4;
import 'package:eclektik/graphql/artist_card_fragment.data.gql.dart' as _i2;
import 'package:eclektik/graphql/artist_card_fragment.var.gql.dart' as _i3;
import 'package:eclektik/graphql/serializers.gql.dart' as _i6;
import 'package:ferry_exec/ferry_exec.dart' as _i1;
import 'package:gql/ast.dart' as _i5;

part 'artist_card_fragment.req.gql.g.dart';

abstract class GArtistCardReq
    implements
        Built<GArtistCardReq, GArtistCardReqBuilder>,
        _i1.FragmentRequest<_i2.GArtistCardData, _i3.GArtistCardVars> {
  GArtistCardReq._();

  factory GArtistCardReq([Function(GArtistCardReqBuilder b) updates]) =
      _$GArtistCardReq;

  static void _initializeBuilder(GArtistCardReqBuilder b) => b
    ..document = _i4.document
    ..fragmentName = 'ArtistCard';
  _i3.GArtistCardVars get vars;
  _i5.DocumentNode get document;
  String? get fragmentName;
  Map<String, dynamic> get idFields;
  @override
  _i2.GArtistCardData? parseData(Map<String, dynamic> json) =>
      _i2.GArtistCardData.fromJson(json);
  static Serializer<GArtistCardReq> get serializer =>
      _$gArtistCardReqSerializer;
  Map<String, dynamic> toJson() =>
      (_i6.serializers.serializeWith(GArtistCardReq.serializer, this)
          as Map<String, dynamic>);
  static GArtistCardReq? fromJson(Map<String, dynamic> json) =>
      _i6.serializers.deserializeWith(GArtistCardReq.serializer, json);
}
