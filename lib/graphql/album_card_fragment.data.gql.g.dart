// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_card_fragment.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GAlbumCardData> _$gAlbumCardDataSerializer =
    new _$GAlbumCardDataSerializer();
Serializer<GAlbumCardData_songs> _$gAlbumCardDataSongsSerializer =
    new _$GAlbumCardData_songsSerializer();
Serializer<GAlbumCardData_artists> _$gAlbumCardDataArtistsSerializer =
    new _$GAlbumCardData_artistsSerializer();

class _$GAlbumCardDataSerializer
    implements StructuredSerializer<GAlbumCardData> {
  @override
  final Iterable<Type> types = const [GAlbumCardData, _$GAlbumCardData];
  @override
  final String wireName = 'GAlbumCardData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GAlbumCardData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      's3Key',
      serializers.serialize(object.s3Key,
          specifiedType: const FullType(String)),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GAlbumCardData_songs)])),
      'artists',
      serializers.serialize(object.artists,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GAlbumCardData_artists)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i2.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i2.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GAlbumCardData deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumCardDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's3Key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(GAlbumCardData_songs)]))!
              as BuiltList<Object?>);
          break;
        case 'artists':
          result.artists.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GAlbumCardData_artists)
              ]))! as BuiltList<Object?>);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i2.GTime))! as _i2.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumCardData_songsSerializer
    implements StructuredSerializer<GAlbumCardData_songs> {
  @override
  final Iterable<Type> types = const [
    GAlbumCardData_songs,
    _$GAlbumCardData_songs
  ];
  @override
  final String wireName = 'GAlbumCardData_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumCardData_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumCardData_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumCardData_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumCardData_artistsSerializer
    implements StructuredSerializer<GAlbumCardData_artists> {
  @override
  final Iterable<Type> types = const [
    GAlbumCardData_artists,
    _$GAlbumCardData_artists
  ];
  @override
  final String wireName = 'GAlbumCardData_artists';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GAlbumCardData_artists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  GAlbumCardData_artists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GAlbumCardData_artistsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GAlbumCardData extends GAlbumCardData {
  @override
  final String G__typename;
  @override
  final String s3Key;
  @override
  final BuiltList<GAlbumCardData_songs> songs;
  @override
  final BuiltList<GAlbumCardData_artists> artists;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i2.GTime createdAt;
  @override
  final _i2.GTime updatedAt;

  factory _$GAlbumCardData([void Function(GAlbumCardDataBuilder)? updates]) =>
      (new GAlbumCardDataBuilder()..update(updates)).build();

  _$GAlbumCardData._(
      {required this.G__typename,
      required this.s3Key,
      required this.songs,
      required this.artists,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumCardData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(s3Key, 'GAlbumCardData', 's3Key');
    BuiltValueNullFieldError.checkNotNull(songs, 'GAlbumCardData', 'songs');
    BuiltValueNullFieldError.checkNotNull(artists, 'GAlbumCardData', 'artists');
    BuiltValueNullFieldError.checkNotNull(id, 'GAlbumCardData', 'id');
    BuiltValueNullFieldError.checkNotNull(name, 'GAlbumCardData', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GAlbumCardData', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GAlbumCardData', 'updatedAt');
  }

  @override
  GAlbumCardData rebuild(void Function(GAlbumCardDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumCardDataBuilder toBuilder() =>
      new GAlbumCardDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumCardData &&
        G__typename == other.G__typename &&
        s3Key == other.s3Key &&
        songs == other.songs &&
        artists == other.artists &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, G__typename.hashCode),
                                    s3Key.hashCode),
                                songs.hashCode),
                            artists.hashCode),
                        id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumCardData')
          ..add('G__typename', G__typename)
          ..add('s3Key', s3Key)
          ..add('songs', songs)
          ..add('artists', artists)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GAlbumCardDataBuilder
    implements Builder<GAlbumCardData, GAlbumCardDataBuilder> {
  _$GAlbumCardData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  ListBuilder<GAlbumCardData_songs>? _songs;
  ListBuilder<GAlbumCardData_songs> get songs =>
      _$this._songs ??= new ListBuilder<GAlbumCardData_songs>();
  set songs(ListBuilder<GAlbumCardData_songs>? songs) => _$this._songs = songs;

  ListBuilder<GAlbumCardData_artists>? _artists;
  ListBuilder<GAlbumCardData_artists> get artists =>
      _$this._artists ??= new ListBuilder<GAlbumCardData_artists>();
  set artists(ListBuilder<GAlbumCardData_artists>? artists) =>
      _$this._artists = artists;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i2.GTimeBuilder? _createdAt;
  _i2.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i2.GTimeBuilder();
  set createdAt(_i2.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i2.GTimeBuilder? _updatedAt;
  _i2.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i2.GTimeBuilder();
  set updatedAt(_i2.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GAlbumCardDataBuilder() {
    GAlbumCardData._initializeBuilder(this);
  }

  GAlbumCardDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _s3Key = $v.s3Key;
      _songs = $v.songs.toBuilder();
      _artists = $v.artists.toBuilder();
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumCardData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumCardData;
  }

  @override
  void update(void Function(GAlbumCardDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumCardData build() {
    _$GAlbumCardData _$result;
    try {
      _$result = _$v ??
          new _$GAlbumCardData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GAlbumCardData', 'G__typename'),
              s3Key: BuiltValueNullFieldError.checkNotNull(
                  s3Key, 'GAlbumCardData', 's3Key'),
              songs: songs.build(),
              artists: artists.build(),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GAlbumCardData', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GAlbumCardData', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'songs';
        songs.build();
        _$failedField = 'artists';
        artists.build();

        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GAlbumCardData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumCardData_songs extends GAlbumCardData_songs {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;

  factory _$GAlbumCardData_songs(
          [void Function(GAlbumCardData_songsBuilder)? updates]) =>
      (new GAlbumCardData_songsBuilder()..update(updates)).build();

  _$GAlbumCardData_songs._(
      {required this.G__typename, required this.id, required this.title})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumCardData_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GAlbumCardData_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GAlbumCardData_songs', 'title');
  }

  @override
  GAlbumCardData_songs rebuild(
          void Function(GAlbumCardData_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumCardData_songsBuilder toBuilder() =>
      new GAlbumCardData_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumCardData_songs &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), title.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumCardData_songs')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title))
        .toString();
  }
}

class GAlbumCardData_songsBuilder
    implements Builder<GAlbumCardData_songs, GAlbumCardData_songsBuilder> {
  _$GAlbumCardData_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  GAlbumCardData_songsBuilder() {
    GAlbumCardData_songs._initializeBuilder(this);
  }

  GAlbumCardData_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumCardData_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumCardData_songs;
  }

  @override
  void update(void Function(GAlbumCardData_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumCardData_songs build() {
    final _$result = _$v ??
        new _$GAlbumCardData_songs._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GAlbumCardData_songs', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumCardData_songs', 'id'),
            title: BuiltValueNullFieldError.checkNotNull(
                title, 'GAlbumCardData_songs', 'title'));
    replace(_$result);
    return _$result;
  }
}

class _$GAlbumCardData_artists extends GAlbumCardData_artists {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;

  factory _$GAlbumCardData_artists(
          [void Function(GAlbumCardData_artistsBuilder)? updates]) =>
      (new GAlbumCardData_artistsBuilder()..update(updates)).build();

  _$GAlbumCardData_artists._(
      {required this.G__typename, required this.id, required this.name})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GAlbumCardData_artists', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(id, 'GAlbumCardData_artists', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GAlbumCardData_artists', 'name');
  }

  @override
  GAlbumCardData_artists rebuild(
          void Function(GAlbumCardData_artistsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GAlbumCardData_artistsBuilder toBuilder() =>
      new GAlbumCardData_artistsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GAlbumCardData_artists &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GAlbumCardData_artists')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class GAlbumCardData_artistsBuilder
    implements Builder<GAlbumCardData_artists, GAlbumCardData_artistsBuilder> {
  _$GAlbumCardData_artists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  GAlbumCardData_artistsBuilder() {
    GAlbumCardData_artists._initializeBuilder(this);
  }

  GAlbumCardData_artistsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GAlbumCardData_artists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GAlbumCardData_artists;
  }

  @override
  void update(void Function(GAlbumCardData_artistsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GAlbumCardData_artists build() {
    final _$result = _$v ??
        new _$GAlbumCardData_artists._(
            G__typename: BuiltValueNullFieldError.checkNotNull(
                G__typename, 'GAlbumCardData_artists', 'G__typename'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'GAlbumCardData_artists', 'id'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'GAlbumCardData_artists', 'name'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
