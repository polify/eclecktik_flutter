// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_query.data.gql.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GSearchData> _$gSearchDataSerializer = new _$GSearchDataSerializer();
Serializer<GSearchData_search> _$gSearchDataSearchSerializer =
    new _$GSearchData_searchSerializer();
Serializer<GSearchData_search_artists> _$gSearchDataSearchArtistsSerializer =
    new _$GSearchData_search_artistsSerializer();
Serializer<GSearchData_search_artists_artists>
    _$gSearchDataSearchArtistsArtistsSerializer =
    new _$GSearchData_search_artists_artistsSerializer();
Serializer<GSearchData_search_albums> _$gSearchDataSearchAlbumsSerializer =
    new _$GSearchData_search_albumsSerializer();
Serializer<GSearchData_search_albums_albums>
    _$gSearchDataSearchAlbumsAlbumsSerializer =
    new _$GSearchData_search_albums_albumsSerializer();
Serializer<GSearchData_search_songs> _$gSearchDataSearchSongsSerializer =
    new _$GSearchData_search_songsSerializer();
Serializer<GSearchData_search_songs_songs>
    _$gSearchDataSearchSongsSongsSerializer =
    new _$GSearchData_search_songs_songsSerializer();

class _$GSearchDataSerializer implements StructuredSerializer<GSearchData> {
  @override
  final Iterable<Type> types = const [GSearchData, _$GSearchData];
  @override
  final String wireName = 'GSearchData';

  @override
  Iterable<Object?> serialize(Serializers serializers, GSearchData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'search',
      serializers.serialize(object.search,
          specifiedType: const FullType(GSearchData_search)),
    ];

    return result;
  }

  @override
  GSearchData deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'search':
          result.search.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSearchData_search))!
              as GSearchData_search);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_searchSerializer
    implements StructuredSerializer<GSearchData_search> {
  @override
  final Iterable<Type> types = const [GSearchData_search, _$GSearchData_search];
  @override
  final String wireName = 'GSearchData_search';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.total;
    if (value != null) {
      result
        ..add('total')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.artists;
    if (value != null) {
      result
        ..add('artists')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSearchData_search_artists)));
    }
    value = object.albums;
    if (value != null) {
      result
        ..add('albums')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSearchData_search_albums)));
    }
    value = object.songs;
    if (value != null) {
      result
        ..add('songs')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(GSearchData_search_songs)));
    }
    return result;
  }

  @override
  GSearchData_search deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_searchBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'total':
          result.total = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'artists':
          result.artists.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSearchData_search_artists))!
              as GSearchData_search_artists);
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSearchData_search_albums))!
              as GSearchData_search_albums);
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
                  specifiedType: const FullType(GSearchData_search_songs))!
              as GSearchData_search_songs);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_search_artistsSerializer
    implements StructuredSerializer<GSearchData_search_artists> {
  @override
  final Iterable<Type> types = const [
    GSearchData_search_artists,
    _$GSearchData_search_artists
  ];
  @override
  final String wireName = 'GSearchData_search_artists';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search_artists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'artists',
      serializers.serialize(object.artists,
          specifiedType: const FullType(BuiltList,
              const [const FullType(GSearchData_search_artists_artists)])),
    ];
    Object? value;
    value = object.total;
    if (value != null) {
      result
        ..add('total')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GSearchData_search_artists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_search_artistsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'total':
          result.total = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'artists':
          result.artists.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GSearchData_search_artists_artists)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_search_artists_artistsSerializer
    implements StructuredSerializer<GSearchData_search_artists_artists> {
  @override
  final Iterable<Type> types = const [
    GSearchData_search_artists_artists,
    _$GSearchData_search_artists_artists
  ];
  @override
  final String wireName = 'GSearchData_search_artists_artists';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search_artists_artists object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GSearchData_search_artists_artists deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_search_artists_artistsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_search_albumsSerializer
    implements StructuredSerializer<GSearchData_search_albums> {
  @override
  final Iterable<Type> types = const [
    GSearchData_search_albums,
    _$GSearchData_search_albums
  ];
  @override
  final String wireName = 'GSearchData_search_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'albums',
      serializers.serialize(object.albums,
          specifiedType: const FullType(BuiltList,
              const [const FullType(GSearchData_search_albums_albums)])),
    ];
    Object? value;
    value = object.total;
    if (value != null) {
      result
        ..add('total')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GSearchData_search_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_search_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'total':
          result.total = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'albums':
          result.albums.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GSearchData_search_albums_albums)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_search_albums_albumsSerializer
    implements StructuredSerializer<GSearchData_search_albums_albums> {
  @override
  final Iterable<Type> types = const [
    GSearchData_search_albums_albums,
    _$GSearchData_search_albums_albums
  ];
  @override
  final String wireName = 'GSearchData_search_albums_albums';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search_albums_albums object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GSearchData_search_albums_albums deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_search_albums_albumsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_search_songsSerializer
    implements StructuredSerializer<GSearchData_search_songs> {
  @override
  final Iterable<Type> types = const [
    GSearchData_search_songs,
    _$GSearchData_search_songs
  ];
  @override
  final String wireName = 'GSearchData_search_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'songs',
      serializers.serialize(object.songs,
          specifiedType: const FullType(BuiltList,
              const [const FullType(GSearchData_search_songs_songs)])),
    ];
    Object? value;
    value = object.total;
    if (value != null) {
      result
        ..add('total')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  GSearchData_search_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_search_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'total':
          result.total = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'songs':
          result.songs.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(GSearchData_search_songs_songs)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData_search_songs_songsSerializer
    implements StructuredSerializer<GSearchData_search_songs_songs> {
  @override
  final Iterable<Type> types = const [
    GSearchData_search_songs_songs,
    _$GSearchData_search_songs_songs
  ];
  @override
  final String wireName = 'GSearchData_search_songs_songs';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, GSearchData_search_songs_songs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      '__typename',
      serializers.serialize(object.G__typename,
          specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(_i3.GTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(_i3.GTime)),
    ];
    Object? value;
    value = object.coverUrl;
    if (value != null) {
      result
        ..add('coverUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GSearchData_search_songs_songs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GSearchData_search_songs_songsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case '__typename':
          result.G__typename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'createdAt':
          result.createdAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
        case 'updatedAt':
          result.updatedAt.replace(serializers.deserialize(value,
              specifiedType: const FullType(_i3.GTime))! as _i3.GTime);
          break;
      }
    }

    return result.build();
  }
}

class _$GSearchData extends GSearchData {
  @override
  final String G__typename;
  @override
  final GSearchData_search search;

  factory _$GSearchData([void Function(GSearchDataBuilder)? updates]) =>
      (new GSearchDataBuilder()..update(updates)).build();

  _$GSearchData._({required this.G__typename, required this.search})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(search, 'GSearchData', 'search');
  }

  @override
  GSearchData rebuild(void Function(GSearchDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchDataBuilder toBuilder() => new GSearchDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData &&
        G__typename == other.G__typename &&
        search == other.search;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, G__typename.hashCode), search.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData')
          ..add('G__typename', G__typename)
          ..add('search', search))
        .toString();
  }
}

class GSearchDataBuilder implements Builder<GSearchData, GSearchDataBuilder> {
  _$GSearchData? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  GSearchData_searchBuilder? _search;
  GSearchData_searchBuilder get search =>
      _$this._search ??= new GSearchData_searchBuilder();
  set search(GSearchData_searchBuilder? search) => _$this._search = search;

  GSearchDataBuilder() {
    GSearchData._initializeBuilder(this);
  }

  GSearchDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _search = $v.search.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData;
  }

  @override
  void update(void Function(GSearchDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData build() {
    _$GSearchData _$result;
    try {
      _$result = _$v ??
          new _$GSearchData._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSearchData', 'G__typename'),
              search: search.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'search';
        search.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search extends GSearchData_search {
  @override
  final String G__typename;
  @override
  final int? total;
  @override
  final GSearchData_search_artists? artists;
  @override
  final GSearchData_search_albums? albums;
  @override
  final GSearchData_search_songs? songs;

  factory _$GSearchData_search(
          [void Function(GSearchData_searchBuilder)? updates]) =>
      (new GSearchData_searchBuilder()..update(updates)).build();

  _$GSearchData_search._(
      {required this.G__typename,
      this.total,
      this.artists,
      this.albums,
      this.songs})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search', 'G__typename');
  }

  @override
  GSearchData_search rebuild(
          void Function(GSearchData_searchBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_searchBuilder toBuilder() =>
      new GSearchData_searchBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search &&
        G__typename == other.G__typename &&
        total == other.total &&
        artists == other.artists &&
        albums == other.albums &&
        songs == other.songs;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, G__typename.hashCode), total.hashCode),
                artists.hashCode),
            albums.hashCode),
        songs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search')
          ..add('G__typename', G__typename)
          ..add('total', total)
          ..add('artists', artists)
          ..add('albums', albums)
          ..add('songs', songs))
        .toString();
  }
}

class GSearchData_searchBuilder
    implements Builder<GSearchData_search, GSearchData_searchBuilder> {
  _$GSearchData_search? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _total;
  int? get total => _$this._total;
  set total(int? total) => _$this._total = total;

  GSearchData_search_artistsBuilder? _artists;
  GSearchData_search_artistsBuilder get artists =>
      _$this._artists ??= new GSearchData_search_artistsBuilder();
  set artists(GSearchData_search_artistsBuilder? artists) =>
      _$this._artists = artists;

  GSearchData_search_albumsBuilder? _albums;
  GSearchData_search_albumsBuilder get albums =>
      _$this._albums ??= new GSearchData_search_albumsBuilder();
  set albums(GSearchData_search_albumsBuilder? albums) =>
      _$this._albums = albums;

  GSearchData_search_songsBuilder? _songs;
  GSearchData_search_songsBuilder get songs =>
      _$this._songs ??= new GSearchData_search_songsBuilder();
  set songs(GSearchData_search_songsBuilder? songs) => _$this._songs = songs;

  GSearchData_searchBuilder() {
    GSearchData_search._initializeBuilder(this);
  }

  GSearchData_searchBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _total = $v.total;
      _artists = $v.artists?.toBuilder();
      _albums = $v.albums?.toBuilder();
      _songs = $v.songs?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search;
  }

  @override
  void update(void Function(GSearchData_searchBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search build() {
    _$GSearchData_search _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSearchData_search', 'G__typename'),
              total: total,
              artists: _artists?.build(),
              albums: _albums?.build(),
              songs: _songs?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'artists';
        _artists?.build();
        _$failedField = 'albums';
        _albums?.build();
        _$failedField = 'songs';
        _songs?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search_artists extends GSearchData_search_artists {
  @override
  final String G__typename;
  @override
  final int? total;
  @override
  final BuiltList<GSearchData_search_artists_artists> artists;

  factory _$GSearchData_search_artists(
          [void Function(GSearchData_search_artistsBuilder)? updates]) =>
      (new GSearchData_search_artistsBuilder()..update(updates)).build();

  _$GSearchData_search_artists._(
      {required this.G__typename, this.total, required this.artists})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search_artists', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        artists, 'GSearchData_search_artists', 'artists');
  }

  @override
  GSearchData_search_artists rebuild(
          void Function(GSearchData_search_artistsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_search_artistsBuilder toBuilder() =>
      new GSearchData_search_artistsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search_artists &&
        G__typename == other.G__typename &&
        total == other.total &&
        artists == other.artists;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), total.hashCode), artists.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search_artists')
          ..add('G__typename', G__typename)
          ..add('total', total)
          ..add('artists', artists))
        .toString();
  }
}

class GSearchData_search_artistsBuilder
    implements
        Builder<GSearchData_search_artists, GSearchData_search_artistsBuilder> {
  _$GSearchData_search_artists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _total;
  int? get total => _$this._total;
  set total(int? total) => _$this._total = total;

  ListBuilder<GSearchData_search_artists_artists>? _artists;
  ListBuilder<GSearchData_search_artists_artists> get artists =>
      _$this._artists ??= new ListBuilder<GSearchData_search_artists_artists>();
  set artists(ListBuilder<GSearchData_search_artists_artists>? artists) =>
      _$this._artists = artists;

  GSearchData_search_artistsBuilder() {
    GSearchData_search_artists._initializeBuilder(this);
  }

  GSearchData_search_artistsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _total = $v.total;
      _artists = $v.artists.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search_artists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search_artists;
  }

  @override
  void update(void Function(GSearchData_search_artistsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search_artists build() {
    _$GSearchData_search_artists _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search_artists._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSearchData_search_artists', 'G__typename'),
              total: total,
              artists: artists.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'artists';
        artists.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search_artists', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search_artists_artists
    extends GSearchData_search_artists_artists {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GSearchData_search_artists_artists(
          [void Function(GSearchData_search_artists_artistsBuilder)?
              updates]) =>
      (new GSearchData_search_artists_artistsBuilder()..update(updates))
          .build();

  _$GSearchData_search_artists_artists._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search_artists_artists', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GSearchData_search_artists_artists', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GSearchData_search_artists_artists', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSearchData_search_artists_artists', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSearchData_search_artists_artists', 'updatedAt');
  }

  @override
  GSearchData_search_artists_artists rebuild(
          void Function(GSearchData_search_artists_artistsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_search_artists_artistsBuilder toBuilder() =>
      new GSearchData_search_artists_artistsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search_artists_artists &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search_artists_artists')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GSearchData_search_artists_artistsBuilder
    implements
        Builder<GSearchData_search_artists_artists,
            GSearchData_search_artists_artistsBuilder> {
  _$GSearchData_search_artists_artists? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSearchData_search_artists_artistsBuilder() {
    GSearchData_search_artists_artists._initializeBuilder(this);
  }

  GSearchData_search_artists_artistsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search_artists_artists other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search_artists_artists;
  }

  @override
  void update(
      void Function(GSearchData_search_artists_artistsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search_artists_artists build() {
    _$GSearchData_search_artists_artists _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search_artists_artists._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GSearchData_search_artists_artists', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSearchData_search_artists_artists', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GSearchData_search_artists_artists', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search_artists_artists', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search_albums extends GSearchData_search_albums {
  @override
  final String G__typename;
  @override
  final int? total;
  @override
  final BuiltList<GSearchData_search_albums_albums> albums;

  factory _$GSearchData_search_albums(
          [void Function(GSearchData_search_albumsBuilder)? updates]) =>
      (new GSearchData_search_albumsBuilder()..update(updates)).build();

  _$GSearchData_search_albums._(
      {required this.G__typename, this.total, required this.albums})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        albums, 'GSearchData_search_albums', 'albums');
  }

  @override
  GSearchData_search_albums rebuild(
          void Function(GSearchData_search_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_search_albumsBuilder toBuilder() =>
      new GSearchData_search_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search_albums &&
        G__typename == other.G__typename &&
        total == other.total &&
        albums == other.albums;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, G__typename.hashCode), total.hashCode), albums.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search_albums')
          ..add('G__typename', G__typename)
          ..add('total', total)
          ..add('albums', albums))
        .toString();
  }
}

class GSearchData_search_albumsBuilder
    implements
        Builder<GSearchData_search_albums, GSearchData_search_albumsBuilder> {
  _$GSearchData_search_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _total;
  int? get total => _$this._total;
  set total(int? total) => _$this._total = total;

  ListBuilder<GSearchData_search_albums_albums>? _albums;
  ListBuilder<GSearchData_search_albums_albums> get albums =>
      _$this._albums ??= new ListBuilder<GSearchData_search_albums_albums>();
  set albums(ListBuilder<GSearchData_search_albums_albums>? albums) =>
      _$this._albums = albums;

  GSearchData_search_albumsBuilder() {
    GSearchData_search_albums._initializeBuilder(this);
  }

  GSearchData_search_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _total = $v.total;
      _albums = $v.albums.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search_albums;
  }

  @override
  void update(void Function(GSearchData_search_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search_albums build() {
    _$GSearchData_search_albums _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSearchData_search_albums', 'G__typename'),
              total: total,
              albums: albums.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search_albums_albums
    extends GSearchData_search_albums_albums {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String name;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GSearchData_search_albums_albums(
          [void Function(GSearchData_search_albums_albumsBuilder)? updates]) =>
      (new GSearchData_search_albums_albumsBuilder()..update(updates)).build();

  _$GSearchData_search_albums_albums._(
      {required this.G__typename,
      required this.id,
      required this.name,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search_albums_albums', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GSearchData_search_albums_albums', 'id');
    BuiltValueNullFieldError.checkNotNull(
        name, 'GSearchData_search_albums_albums', 'name');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSearchData_search_albums_albums', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSearchData_search_albums_albums', 'updatedAt');
  }

  @override
  GSearchData_search_albums_albums rebuild(
          void Function(GSearchData_search_albums_albumsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_search_albums_albumsBuilder toBuilder() =>
      new GSearchData_search_albums_albumsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search_albums_albums &&
        G__typename == other.G__typename &&
        id == other.id &&
        name == other.name &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    name.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search_albums_albums')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('name', name)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GSearchData_search_albums_albumsBuilder
    implements
        Builder<GSearchData_search_albums_albums,
            GSearchData_search_albums_albumsBuilder> {
  _$GSearchData_search_albums_albums? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSearchData_search_albums_albumsBuilder() {
    GSearchData_search_albums_albums._initializeBuilder(this);
  }

  GSearchData_search_albums_albumsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _name = $v.name;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search_albums_albums other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search_albums_albums;
  }

  @override
  void update(void Function(GSearchData_search_albums_albumsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search_albums_albums build() {
    _$GSearchData_search_albums_albums _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search_albums_albums._(
              G__typename: BuiltValueNullFieldError.checkNotNull(G__typename,
                  'GSearchData_search_albums_albums', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSearchData_search_albums_albums', 'id'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'GSearchData_search_albums_albums', 'name'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search_albums_albums', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search_songs extends GSearchData_search_songs {
  @override
  final String G__typename;
  @override
  final int? total;
  @override
  final BuiltList<GSearchData_search_songs_songs> songs;

  factory _$GSearchData_search_songs(
          [void Function(GSearchData_search_songsBuilder)? updates]) =>
      (new GSearchData_search_songsBuilder()..update(updates)).build();

  _$GSearchData_search_songs._(
      {required this.G__typename, this.total, required this.songs})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        songs, 'GSearchData_search_songs', 'songs');
  }

  @override
  GSearchData_search_songs rebuild(
          void Function(GSearchData_search_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_search_songsBuilder toBuilder() =>
      new GSearchData_search_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search_songs &&
        G__typename == other.G__typename &&
        total == other.total &&
        songs == other.songs;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, G__typename.hashCode), total.hashCode), songs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search_songs')
          ..add('G__typename', G__typename)
          ..add('total', total)
          ..add('songs', songs))
        .toString();
  }
}

class GSearchData_search_songsBuilder
    implements
        Builder<GSearchData_search_songs, GSearchData_search_songsBuilder> {
  _$GSearchData_search_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  int? _total;
  int? get total => _$this._total;
  set total(int? total) => _$this._total = total;

  ListBuilder<GSearchData_search_songs_songs>? _songs;
  ListBuilder<GSearchData_search_songs_songs> get songs =>
      _$this._songs ??= new ListBuilder<GSearchData_search_songs_songs>();
  set songs(ListBuilder<GSearchData_search_songs_songs>? songs) =>
      _$this._songs = songs;

  GSearchData_search_songsBuilder() {
    GSearchData_search_songs._initializeBuilder(this);
  }

  GSearchData_search_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _total = $v.total;
      _songs = $v.songs.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search_songs;
  }

  @override
  void update(void Function(GSearchData_search_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search_songs build() {
    _$GSearchData_search_songs _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSearchData_search_songs', 'G__typename'),
              total: total,
              songs: songs.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'songs';
        songs.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$GSearchData_search_songs_songs extends GSearchData_search_songs_songs {
  @override
  final String G__typename;
  @override
  final String id;
  @override
  final String title;
  @override
  final String? coverUrl;
  @override
  final _i3.GTime createdAt;
  @override
  final _i3.GTime updatedAt;

  factory _$GSearchData_search_songs_songs(
          [void Function(GSearchData_search_songs_songsBuilder)? updates]) =>
      (new GSearchData_search_songs_songsBuilder()..update(updates)).build();

  _$GSearchData_search_songs_songs._(
      {required this.G__typename,
      required this.id,
      required this.title,
      this.coverUrl,
      required this.createdAt,
      required this.updatedAt})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        G__typename, 'GSearchData_search_songs_songs', 'G__typename');
    BuiltValueNullFieldError.checkNotNull(
        id, 'GSearchData_search_songs_songs', 'id');
    BuiltValueNullFieldError.checkNotNull(
        title, 'GSearchData_search_songs_songs', 'title');
    BuiltValueNullFieldError.checkNotNull(
        createdAt, 'GSearchData_search_songs_songs', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(
        updatedAt, 'GSearchData_search_songs_songs', 'updatedAt');
  }

  @override
  GSearchData_search_songs_songs rebuild(
          void Function(GSearchData_search_songs_songsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GSearchData_search_songs_songsBuilder toBuilder() =>
      new GSearchData_search_songs_songsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GSearchData_search_songs_songs &&
        G__typename == other.G__typename &&
        id == other.id &&
        title == other.title &&
        coverUrl == other.coverUrl &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, G__typename.hashCode), id.hashCode),
                    title.hashCode),
                coverUrl.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GSearchData_search_songs_songs')
          ..add('G__typename', G__typename)
          ..add('id', id)
          ..add('title', title)
          ..add('coverUrl', coverUrl)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class GSearchData_search_songs_songsBuilder
    implements
        Builder<GSearchData_search_songs_songs,
            GSearchData_search_songs_songsBuilder> {
  _$GSearchData_search_songs_songs? _$v;

  String? _G__typename;
  String? get G__typename => _$this._G__typename;
  set G__typename(String? G__typename) => _$this._G__typename = G__typename;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _coverUrl;
  String? get coverUrl => _$this._coverUrl;
  set coverUrl(String? coverUrl) => _$this._coverUrl = coverUrl;

  _i3.GTimeBuilder? _createdAt;
  _i3.GTimeBuilder get createdAt =>
      _$this._createdAt ??= new _i3.GTimeBuilder();
  set createdAt(_i3.GTimeBuilder? createdAt) => _$this._createdAt = createdAt;

  _i3.GTimeBuilder? _updatedAt;
  _i3.GTimeBuilder get updatedAt =>
      _$this._updatedAt ??= new _i3.GTimeBuilder();
  set updatedAt(_i3.GTimeBuilder? updatedAt) => _$this._updatedAt = updatedAt;

  GSearchData_search_songs_songsBuilder() {
    GSearchData_search_songs_songs._initializeBuilder(this);
  }

  GSearchData_search_songs_songsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _G__typename = $v.G__typename;
      _id = $v.id;
      _title = $v.title;
      _coverUrl = $v.coverUrl;
      _createdAt = $v.createdAt.toBuilder();
      _updatedAt = $v.updatedAt.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GSearchData_search_songs_songs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GSearchData_search_songs_songs;
  }

  @override
  void update(void Function(GSearchData_search_songs_songsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GSearchData_search_songs_songs build() {
    _$GSearchData_search_songs_songs _$result;
    try {
      _$result = _$v ??
          new _$GSearchData_search_songs_songs._(
              G__typename: BuiltValueNullFieldError.checkNotNull(
                  G__typename, 'GSearchData_search_songs_songs', 'G__typename'),
              id: BuiltValueNullFieldError.checkNotNull(
                  id, 'GSearchData_search_songs_songs', 'id'),
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'GSearchData_search_songs_songs', 'title'),
              coverUrl: coverUrl,
              createdAt: createdAt.build(),
              updatedAt: updatedAt.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdAt';
        createdAt.build();
        _$failedField = 'updatedAt';
        updatedAt.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GSearchData_search_songs_songs', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
