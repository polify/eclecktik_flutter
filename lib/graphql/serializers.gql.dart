import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart' show StandardJsonPlugin;
import 'package:eclektik/graphql/album_card_fragment.data.gql.dart'
    show GAlbumCardData, GAlbumCardData_artists, GAlbumCardData_songs;
import 'package:eclektik/graphql/album_card_fragment.req.gql.dart'
    show GAlbumCardReq;
import 'package:eclektik/graphql/album_card_fragment.var.gql.dart'
    show GAlbumCardVars;
import 'package:eclektik/graphql/album_item_fragment.data.gql.dart'
    show GAlbumItemData;
import 'package:eclektik/graphql/album_item_fragment.req.gql.dart'
    show GAlbumItemReq;
import 'package:eclektik/graphql/album_item_fragment.var.gql.dart'
    show GAlbumItemVars;
import 'package:eclektik/graphql/album_query.data.gql.dart'
    show
        GAlbumData,
        GAlbumData_album,
        GAlbumData_album_artists,
        GAlbumData_album_songs,
        GAlbumData_album_songs_album,
        GAlbumData_album_songs_artist,
        GAlbumData_album_songs_signedUrl;
import 'package:eclektik/graphql/album_query.req.gql.dart' show GAlbumReq;
import 'package:eclektik/graphql/album_query.var.gql.dart' show GAlbumVars;
import 'package:eclektik/graphql/albums_query.data.gql.dart'
    show
        GAlbumsData,
        GAlbumsData_albums,
        GAlbumsData_albums_albums,
        GAlbumsData_albums_albums_artists,
        GAlbumsData_albums_albums_songs;
import 'package:eclektik/graphql/albums_query.req.gql.dart' show GAlbumsReq;
import 'package:eclektik/graphql/albums_query.var.gql.dart' show GAlbumsVars;
import 'package:eclektik/graphql/artist_card_fragment.data.gql.dart'
    show GArtistCardData, GArtistCardData_albums, GArtistCardData_songs;
import 'package:eclektik/graphql/artist_card_fragment.req.gql.dart'
    show GArtistCardReq;
import 'package:eclektik/graphql/artist_card_fragment.var.gql.dart'
    show GArtistCardVars;
import 'package:eclektik/graphql/artist_item_fragment.data.gql.dart'
    show GArtistItemData;
import 'package:eclektik/graphql/artist_item_fragment.req.gql.dart'
    show GArtistItemReq;
import 'package:eclektik/graphql/artist_item_fragment.var.gql.dart'
    show GArtistItemVars;
import 'package:eclektik/graphql/artist_query.data.gql.dart'
    show
        GArtistData,
        GArtistData_artist,
        GArtistData_artist_albums,
        GArtistData_artist_songs;
import 'package:eclektik/graphql/artist_query.req.gql.dart' show GArtistReq;
import 'package:eclektik/graphql/artist_query.var.gql.dart' show GArtistVars;
import 'package:eclektik/graphql/artists_query.data.gql.dart'
    show
        GArtistsData,
        GArtistsData_artists,
        GArtistsData_artists_albums,
        GArtistsData_artists_songs;
import 'package:eclektik/graphql/artists_query.req.gql.dart' show GArtistsReq;
import 'package:eclektik/graphql/artists_query.var.gql.dart' show GArtistsVars;
import 'package:eclektik/graphql/device_card_fragment.data.gql.dart'
    show GDeviceItemData;
import 'package:eclektik/graphql/device_card_fragment.req.gql.dart'
    show GDeviceItemReq;
import 'package:eclektik/graphql/device_card_fragment.var.gql.dart'
    show GDeviceItemVars;
import 'package:eclektik/graphql/login_query.data.gql.dart'
    show GLoginData, GLoginData_login, GLoginData_login_user;
import 'package:eclektik/graphql/login_query.req.gql.dart' show GLoginReq;
import 'package:eclektik/graphql/login_query.var.gql.dart' show GLoginVars;
import 'package:eclektik/graphql/profile_query.data.gql.dart'
    show
        GProfileData,
        GProfileData_profile,
        GProfileData_profile_devices,
        GProfileData_profile_s3Credential;
import 'package:eclektik/graphql/profile_query.req.gql.dart' show GProfileReq;
import 'package:eclektik/graphql/profile_query.var.gql.dart' show GProfileVars;
import 'package:eclektik/graphql/randomplaylist_query.data.gql.dart'
    show
        GRandomPlaylistData,
        GRandomPlaylistData_randomPlaylist,
        GRandomPlaylistData_randomPlaylist_songs,
        GRandomPlaylistData_randomPlaylist_songs_album,
        GRandomPlaylistData_randomPlaylist_songs_artist,
        GRandomPlaylistData_randomPlaylist_songs_signedUrl;
import 'package:eclektik/graphql/randomplaylist_query.req.gql.dart'
    show GRandomPlaylistReq;
import 'package:eclektik/graphql/randomplaylist_query.var.gql.dart'
    show GRandomPlaylistVars;
import 'package:eclektik/graphql/schema.schema.gql.dart'
    show
        GItemsInput,
        GNewUser,
        GPaginationInput,
        GS3CredentialCreateInput,
        GS3CredentialUpdateInput,
        GSearchSongInput,
        GTime;
import 'package:eclektik/graphql/search_query.data.gql.dart'
    show
        GSearchData,
        GSearchData_search,
        GSearchData_search_albums,
        GSearchData_search_albums_albums,
        GSearchData_search_artists,
        GSearchData_search_artists_artists,
        GSearchData_search_songs,
        GSearchData_search_songs_songs;
import 'package:eclektik/graphql/search_query.req.gql.dart' show GSearchReq;
import 'package:eclektik/graphql/search_query.var.gql.dart' show GSearchVars;
import 'package:eclektik/graphql/song_card_fragment.data.gql.dart'
    show
        GSongCardData,
        GSongCardData_album,
        GSongCardData_artist,
        GSongCardData_signedUrl;
import 'package:eclektik/graphql/song_card_fragment.req.gql.dart'
    show GSongCardReq;
import 'package:eclektik/graphql/song_card_fragment.var.gql.dart'
    show GSongCardVars;
import 'package:eclektik/graphql/song_item_fragment.data.gql.dart'
    show GSongItemData;
import 'package:eclektik/graphql/song_item_fragment.req.gql.dart'
    show GSongItemReq;
import 'package:eclektik/graphql/song_item_fragment.var.gql.dart'
    show GSongItemVars;
import 'package:eclektik/graphql/song_query.data.gql.dart'
    show
        GSongData,
        GSongData_song,
        GSongData_song_album,
        GSongData_song_artist,
        GSongData_song_signedUrl;
import 'package:eclektik/graphql/song_query.req.gql.dart' show GSongReq;
import 'package:eclektik/graphql/song_query.var.gql.dart' show GSongVars;
import 'package:eclektik/graphql/songs_query.data.gql.dart'
    show
        GSongsData,
        GSongsData_songs,
        GSongsData_songs_songs,
        GSongsData_songs_songs_album,
        GSongsData_songs_songs_artist,
        GSongsData_songs_songs_signedUrl;
import 'package:eclektik/graphql/songs_query.req.gql.dart' show GSongsReq;
import 'package:eclektik/graphql/songs_query.var.gql.dart' show GSongsVars;
import 'package:gql_code_builder/src/serializers/operation_serializer.dart'
    show OperationSerializer;

part 'serializers.gql.g.dart';

final SerializersBuilder _serializersBuilder = _$serializers.toBuilder()
  ..add(OperationSerializer())
  ..addPlugin(StandardJsonPlugin());
@SerializersFor([
  GAlbumCardData,
  GAlbumCardData_artists,
  GAlbumCardData_songs,
  GAlbumCardReq,
  GAlbumCardVars,
  GAlbumData,
  GAlbumData_album,
  GAlbumData_album_artists,
  GAlbumData_album_songs,
  GAlbumData_album_songs_album,
  GAlbumData_album_songs_artist,
  GAlbumData_album_songs_signedUrl,
  GAlbumItemData,
  GAlbumItemReq,
  GAlbumItemVars,
  GAlbumReq,
  GAlbumVars,
  GAlbumsData,
  GAlbumsData_albums,
  GAlbumsData_albums_albums,
  GAlbumsData_albums_albums_artists,
  GAlbumsData_albums_albums_songs,
  GAlbumsReq,
  GAlbumsVars,
  GArtistCardData,
  GArtistCardData_albums,
  GArtistCardData_songs,
  GArtistCardReq,
  GArtistCardVars,
  GArtistData,
  GArtistData_artist,
  GArtistData_artist_albums,
  GArtistData_artist_songs,
  GArtistItemData,
  GArtistItemReq,
  GArtistItemVars,
  GArtistReq,
  GArtistVars,
  GArtistsData,
  GArtistsData_artists,
  GArtistsData_artists_albums,
  GArtistsData_artists_songs,
  GArtistsReq,
  GArtistsVars,
  GDeviceItemData,
  GDeviceItemReq,
  GDeviceItemVars,
  GItemsInput,
  GLoginData,
  GLoginData_login,
  GLoginData_login_user,
  GLoginReq,
  GLoginVars,
  GNewUser,
  GPaginationInput,
  GProfileData,
  GProfileData_profile,
  GProfileData_profile_devices,
  GProfileData_profile_s3Credential,
  GProfileReq,
  GProfileVars,
  GRandomPlaylistData,
  GRandomPlaylistData_randomPlaylist,
  GRandomPlaylistData_randomPlaylist_songs,
  GRandomPlaylistData_randomPlaylist_songs_album,
  GRandomPlaylistData_randomPlaylist_songs_artist,
  GRandomPlaylistData_randomPlaylist_songs_signedUrl,
  GRandomPlaylistReq,
  GRandomPlaylistVars,
  GS3CredentialCreateInput,
  GS3CredentialUpdateInput,
  GSearchData,
  GSearchData_search,
  GSearchData_search_albums,
  GSearchData_search_albums_albums,
  GSearchData_search_artists,
  GSearchData_search_artists_artists,
  GSearchData_search_songs,
  GSearchData_search_songs_songs,
  GSearchReq,
  GSearchSongInput,
  GSearchVars,
  GSongCardData,
  GSongCardData_album,
  GSongCardData_artist,
  GSongCardData_signedUrl,
  GSongCardReq,
  GSongCardVars,
  GSongData,
  GSongData_song,
  GSongData_song_album,
  GSongData_song_artist,
  GSongData_song_signedUrl,
  GSongItemData,
  GSongItemReq,
  GSongItemVars,
  GSongReq,
  GSongVars,
  GSongsData,
  GSongsData_songs,
  GSongsData_songs_songs,
  GSongsData_songs_songs_album,
  GSongsData_songs_songs_artist,
  GSongsData_songs_songs_signedUrl,
  GSongsReq,
  GSongsVars,
  GTime
])
final Serializers serializers = _serializersBuilder.build();
