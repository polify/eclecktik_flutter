import 'package:flutter/material.dart';

const defaultNoCover = "https://i.giphy.com/media/5ehBR5qtLEXBe/giphy.webp";
const MaterialColor palette = MaterialColor(0xffffa500, {
  50: Color(0xffffa500),
  100: Color(0xffb38b00),
  200: Color(0xfffff1bf),
  300: Color(0xffffe380),
  400: Color(0xffffffff),
  500: Color(0xff454d66),
  600: Color(0xffb38b00),
  700: Color(0xfffff1bf),
  800: Color(0xffffe380),
  900: Color(0xffff9400),
});

const kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: 'OpenSans',
);

const kLabelStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyle = BoxDecoration(
  color: const Color(0xFF6CA8F1),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: const [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

const kScaffoldBackgroundStyle = BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      Color(0xFF73AEF5),
      Color(0xFF61A4F1),
      Color(0xFF478DE0),
      Color(0xFF398AE5),
    ],
    stops: [0.2, 0.4, 0.7, 0.9],
  ),
);

const kScaffoldBackgroundStyleDark = BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      Color(0xFF362222),
      Color(0xFF171010),
      Color(0xFF423F3E),
      Color(0xFF2B2B2B),
    ],
    stops: [0.2, 0.4, 0.7, 0.9],
  ),
);
