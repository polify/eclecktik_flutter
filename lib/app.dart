import 'package:eclektik/screens/album_screen.dart';
import 'package:eclektik/screens/artist_screen.dart';
import 'package:eclektik/screens/settings_screen.dart';
import 'package:eclektik/screens/shell_screen.dart';
import 'package:eclektik/screens/login_screen.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  final bool isLogged;
  const MyApp({Key? key, required this.isLogged}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Eclektik',
      color: Colors.black,
      darkTheme: ThemeData(primaryColor: Colors.black),
      themeMode: ThemeMode.dark,
      home: isLogged
          ? Builder(builder: (ctx) => const ShellScreen())
          : const LoginScreen(),
      onGenerateRoute: (settings) {
        // print(settings);
        switch (settings.name) {
          case 'login':
            return MaterialPageRoute(builder: (_) => const LoginScreen());
          case 'home':
            return MaterialPageRoute(builder: (_) => const ShellScreen());
          case 'settings':
            return MaterialPageRoute(builder: (_) => SettingsScreen());
          case 'album':
            return MaterialPageRoute(
                builder: (_) => AlbumScreen(
                      album: (settings.arguments as Map)["album"],
                    ));
          case 'artist':
            return MaterialPageRoute(
                builder: (_) => ArtistScreen(
                      artist: (settings.arguments as Map)["artist"],
                    ));
          case 'song':
            return MaterialPageRoute(
                builder: (_) =>
                    Center(child: Text((settings.arguments as Map)["id"]!)));
          default:
            return MaterialPageRoute(
                builder: (_) => const Scaffold(
                      body: Center(
                        child: Text("not found..."),
                      ),
                    ));
        }
      },
    );
  }
}
