import 'dart:convert';
// import 'dart:io';

import 'dart:typed_data';

import 'package:ferry_hive_store/ferry_hive_store.dart';

///
///{
///  "UserName": "",
///  "UserID": "",
///  "exp": 1623764469,
///  "iss": "AuthService"
///}
///
class Jwt {
  String encoded;
  String userId;
  String userName;
  DateTime expireAt;
  bool get isexpired => DateTime.now().isAfter(expireAt);
  static const String boxName = 'eclektik';
  Jwt(
      {required this.encoded,
      required this.expireAt,
      required this.userId,
      required this.userName});
  factory Jwt.fromToken(String base) {
    final String text = base.split('.')[1];
    final Uint8List decoded = base64.decode(base64.normalize(text));
    final String val = utf8.decode(decoded);
    final Map<String, dynamic> jason = json.decode(val) as Map<String, dynamic>;
    final String username = jason['UserName'] as String;
    final String userid = jason['UserID'] as String;

    final DateTime exp =
        DateTime.fromMillisecondsSinceEpoch((jason['exp'] as int) * 1000);
    return Jwt(
        encoded: base, expireAt: exp, userId: userid, userName: username);
  }

  static Future<Jwt?> fromStorage(HiveStore? storage) async {
    Jwt? token;
    if (storage != null) {
      var data = storage.get("auth");
      if (data != null) {
        String? accessToken = data["token"];
        if (accessToken != null) {
          return Jwt.fromToken(accessToken);
        }
      }
    }
    return token;
  }

  Future<void> logout(HiveStore? storage) async {
    if (storage != null) storage.clear();
  }
}
